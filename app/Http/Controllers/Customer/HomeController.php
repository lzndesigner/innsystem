<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\Invoice;
use Auth;
use DB;

class HomeController extends Controller
{
    /**
     * Armazena uma nova instancia do model Customer
     *
     * @var \App\Customer
     */
    private $customers;

    protected $redirectTo = '/minha-conta';

    public function __construct()
    {
        $this->middleware('auth:customer')->only(['index', 'editarDados', 'editarDadosUpdate', 'myServices', 'myInvoices', 'viewInvoice']);
        $this->customers = app(Customer::class);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $infoCustomer = Auth::user();
        if($infoCustomer->cep == null){
            return redirect('/minha-conta/meus-dados');
        }else{
            return view('frontend.pages.customers.dashboard', compact('infoCustomer'));
        }
    }

    public function editarDados()
    {
        $customer_id = Auth::id();
        $result = Customer::where('id', $customer_id)->first();
        return view('frontend.pages.customers.dados', compact('result'));
    }

    public function editarDadosUpdate(Request $request, $id)
    {
        $result = $request->all();
        $model = Customer::find($id);

        $rules = [
            'name'          => "required",
            'document'          => "required",
            'email'         => "required|unique:customers,email,$id,id",
            'password'      => 'nullable|min:6',
            'company'       => "required",
            // 'cep'           => "required",
            // 'address'       => "required",
            // 'number'        => "required",
            // 'city'          => "required",
            // 'state'         => "required",
            'phone'         => "required"
        ];

        $messages = [
            'name.required' => 'nome é obrigatório',
            'document.required' => 'cpf/cnpj é obrigatório',
            'email.required' => 'e-mail é obrigatório',
            'company.required' => 'empresa é obrigatório',
            'cep.required' => 'CEP é obrigatório',
            'address.required' => 'endereço é obrigatório',
            'number.required' => 'número é obrigatório',
            'city.required' => 'cidade é obrigatório',
            'state.required' => 'estado é obrigatório',
            'phone.required' => 'telefone é obrigatório'
        ];

        $validator = Validator::make($result, $rules, $messages);

        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 422);
        }

        $model->name = $result['name'];
        $model->document = $result['document'];
        $model->company = $result['company'];
        if ($result['password']) {
            $password = bcrypt($result['password']);
            $model->password = $password;
        }
        $model->cep = $result['cep'];
        $model->address = $result['address'];
        $model->number = $result['number'];
        $model->complement = $result['complement'];
        $model->city = $result['city'];
        $model->state = $result['state'];
        $model->phone = $result['phone'];

        try {
            $model->save();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json('Cliente alterado com sucesso', 200);
    }

    public function myServices()
    {
        $customer_id = Auth::id();

        $result = Customer::find($customer_id);

        $myServices = DB::table('customer_services as a')
            ->select('a.id as id', 'a.customer_id', 'a.service_id', 'a.dominio', 'a.date_start', 'a.date_end', 'a.price', 'a.period', 'a.status', 'b.name as nameService')
            ->join('services as b', 'a.service_id', 'b.id')
            ->where('a.customer_id', $customer_id)
            // ->paginate(10);
            ->get();

        return view('frontend.pages.customers.servicos', compact('result', 'myServices'));
    }

    public function myInvoices()
    {
        $customer_id = Auth::id();

        $result = Invoice::find($customer_id);

        $myInvoices = DB::table('invoices as a')
            ->select('a.id as id', 'a.customer_id', 'a.description', 'a.price', 'a.payment_method', 'a.date_invoice', 'a.date_end', 'a.date_payment', 'a.status')
            ->where('a.customer_id', $customer_id)
            ->get();

        return view('frontend.pages.customers.faturas', compact('result', 'myInvoices'));
    }

    public function viewInvoice($id)
    {
        $customer_id = Auth::id();        
        $result = Invoice::where('customer_id', $customer_id)->find($id);

        if ($result) {
            $getCustomer = DB::table('customers')->where('id', $result->customer_id)->first();

            return view('frontend.viewInvoice', compact('result', 'getCustomer'));
        } else {
            return redirect('/');
        }
    }
}
