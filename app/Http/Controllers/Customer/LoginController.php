<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\Models\Customer;
use DB;
use Carbon\Carbon;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/minha-conta';



    public function __construct()
    {
        $this->middleware('guest:customer')->except('logout');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        return view('frontend.pages.customers.auth.login');
    }


    public function loginCustomer(Request $request)
    {
        // Validate the form data
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:5'
        ]);
        // Attempt to log the user in
        if (Auth::guard('customer')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            // if successful, then redirect to their intended location
            return redirect()->route('customer.dashboard');
        } else {
            session()->flash('messages.error', ['Dados inválido, verifique e tente novamente.']);
            return redirect()->back()->withInput($request->only('email', 'password'));
        }
    }

    public function loginGoogle(){
        return Socialite::driver('google')->redirect();
    }

    public function loginGoogleCallback(){
        $user = Socialite::driver('google')->user();
        $password_new = '123456';
        // dd($user);

        $model = new Customer;
        $model->name = $user->name;
        $model->company = 'Teste';
        $model->document = '123456789';
        $model->email = $user->email;
        if ($password_new) {
            $password = bcrypt($password_new);
            $model->password = $password;
        }
        $model->status = 'ativo';
        $model->cep = '14077220';
        $model->address = 'Av Antonio';
        $model->number = '526';
        $model->complement = 'casa 1';
        $model->city = 'ribeirao preto';
        $model->state = 'SP';
        $model->phone = '16992747526';
        $model->payment_method = 'pix';
        $model->balance_account = 0.00;
        $model->created_at = Carbon::now();
        $model->updated_at = Carbon::now();

        try {
            $model->save();
            Auth::guard('customer')->attempt(['email' => $user->email, 'password' => $password_new]);
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return redirect('/minha-conta');

    }



    public function logout()
    {
        Auth::guard('customer')->logout();
        return redirect()->route('customer.auth.login');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('frontend.pages.customers.auth.register');
    }



    public function store(Request $request)
    {
        $result = $request->all();
        
        $rules = [
            'name'          => "required",
            'document'          => "required",
            'email'         => "required|unique:customers",
            'password'      => 'required|min:6|confirmed',
            'company'       => "required",
            // 'cep'           => "required",
            // 'address'       => "required",
            // 'number'        => "required",
            // 'city'          => "required",
            // 'state'         => "required",
            'phone'         => "required"
        ];
        
        $messages = [
            'name.required' => 'nome é obrigatório',
            'document.required' => 'cpf/cnpj é obrigatório',
            'email.required' => 'e-mail é obrigatório',
            'password.required' => 'senha é obrigatório',
            'password.min' => 'senha precisa ter 6 caracteres',
            'company.required' => 'empresa é obrigatório',
            'cep.required' => 'CEP é obrigatório',
            'address.required' => 'endereço é obrigatório',
            'number.required' => 'número é obrigatório',
            'city.required' => 'cidade é obrigatório',
            'state.required' => 'estado é obrigatório',
            'phone.required' => 'telefone é obrigatório'
        ];
        
        $validator = Validator::make($result, $rules, $messages);
        
        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 422);
        }
        
        $model = new Customer;
        $model->name = $result['name'];
        $model->company = $result['company'];
        $model->document = $result['document'];
        $model->email = $result['email'];
        if ($result['password']) {
            $password = bcrypt($result['password']);
            $model->password = $password;
        }
        $model->status = 'ativo';
        $model->cep = $result['cep'];
        $model->address = $result['address'];
        $model->number = $result['number'];
        $model->complement = $result['complement'];
        $model->city = $result['city'];
        $model->state = $result['state'];
        $model->phone = $result['phone'];
        $model->payment_method = 'pix';
        $model->balance_account = 0.00;
        $model->created_at = Carbon::now();
        $model->updated_at = Carbon::now();

        try {
            $model->save();
            Auth::guard('customer')->attempt(['email' => $request->email, 'password' => $request->password]);
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json('Parabéns, seu cadastro foi realizado com sucesso!', 200);

    }
}
