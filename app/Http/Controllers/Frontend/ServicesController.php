<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Invoice;
use DB;
use Auth;
use Carbon\Carbon;
use App\Models\Customer;
use App\Models\Service;
use App\Models\CustomerServices;

class ServicesController extends Controller
{
  // Page - sites
  public function sites()
  {
    return view('frontend.pages.servicos.criacaosites');
  }

  // Page - Design Grafico
  public function designgrafico()
  {
    return view('frontend.pages.servicos.designgrafico');
  }

  // Page - Design Games
  public function designgames()
  {
    return view('frontend.pages.servicos.design-games');
  }

  // Page - Ilustração
  public function ilustracao()
  {
    return view('frontend.pages.servicos.ilustracao');
  }

  // Page - Loja Virtual
  public function lojavirtual()
  {
    return view('frontend.pages.servicos.lojavirtual');
  }
  // Page - Loja Virtual - Modelos
  public function lojavirtual_modelos()
  {
    return view('frontend.pages.servicos.lojavirtual_modelos');
  }

  // Loja Virtual - Pedido
  public function lojavirtualOrder($id, $period)
  {
    $service = new Service;
    $getService = $service::find($id);
    $getCustomer = false;
    if (Auth::guard('customer')->check()) {
      $getCustomer = Auth::guard('customer')->user();
    }

    return view('frontend.pages.servicos.lojavirtual_order', compact('getService', 'period', 'getCustomer'));
  }

  // Loja Virtual - Pedido Store
  public function lojavirtualStore(Request $request)
  {
    $result = $request->all();


    if ($result['customer_id'] == null) {

      $model_customer = new Customer;

      $model_customer->name = $result['name'];
      $model_customer->document = $result['document'];
      $model_customer->company = $result['company'];
      $model_customer->email = $result['email'];
      if ($result['password']) {
        $password = bcrypt($result['password']);
        $model_customer->password = $password;
      }
      $model_customer->status = 'ativo';
      $model_customer->phone = $result['phone'];
      $model_customer->payment_method = $result['payment_method'];
      $model_customer->balance_account = 0.00;
      $model_customer->created_at = Carbon::now();
      $model_customer->updated_at = Carbon::now();
      try {
        $model_customer->save();
        $customer_id = $model_customer->id;
        Auth::guard('customer')->attempt(['email' => $result['email'], 'password' => $result['password']]);
      } catch (\Exception $e) {
        \Log::error($e->getMessage());
        return response()->json($e->getMessage(), 500);
      }
    } else {
      $customer_id = $result['customer_id'];
    }

    try {
      $model_service = new CustomerServices;

      $model_service->customer_id = $customer_id;
      $model_service->service_id = $result['service_id'];
      $model_service->dominio = $result['dominio'];
      $model_service->status = 'ativo';
      $model_service->date_start = Carbon::now()->format('Y-m-d');
      $model_service->date_end = Carbon::now()->format('Y-m-d');
      $model_service->price = $result['price_total'];
      $model_service->period = $result['period'];
      $model_service->created_at = Carbon::now();
      $model_service->updated_at = Carbon::now();

      // dd($model_service);

      $msg_success = '';
      try {
        $model_service->save();

        $getService = DB::table('services')->where('id', $result['service_id'])->first();

        DB::table('customer_activities')->insert([
          'customer_id' => $customer_id,
          'description' => 'Novo serviço: ' . $getService->name . '',
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now(),
        ]);

        
        
        // Nesse momento preciso disparar o Evento de Gerar Fatura
        $msg_success = ' e fatura gerada.';
        $customer_service_id = $model_service->id;
        $getCustomer = DB::table('customers')->where('id', $customer_id)->first();
        $period = null;
        if ($result['period'] == 'unico') {
          $description = $getService->name . ' - ' . $result['dominio'] . ' - pagamento unico';
        } else {
          if ($result['period'] == 'mensal') {
            $period = 1;
          } else if ($result['period'] == 'trimestral') {
            $period = 3;
          } else if ($result['period'] == 'anual') {
            $period = 12;
          }
          $description = $getService->name . ' - ' . $result['dominio'] . ' (de: ' . Carbon::parse(Carbon::now())->format('d/m/Y') . ' até ' . Carbon::parse(Carbon::now())->addMonth($period)->format('d/m/Y') . ') - R$ ' . number_format($result['price_total'], 2);
        }
        
        $array_descriptions = array(
          $description
        );
        $array_services = array(
          $customer_service_id
        );
        
        $invoice = new Invoice;
        $invoice->customer_id = $customer_id;
        $invoice->customer_service_id = json_encode($array_services);
        $invoice->description = $array_descriptions;
        $invoice->price = $result['price_total'];
        $invoice->status = 'nao_pago';
        $invoice->payment_method = $result['payment_method'];
        $invoice->date_invoice = Carbon::now();
        $invoice->date_end = Carbon::now()->addDays(7);
        $invoice->date_payment = null;
        $invoice->created_at = Carbon::now();
        $invoice->updated_at = Carbon::now();
        $invoice->save();
        
        DB::table('customer_activities')->insert([
          'customer_id' => $customer_id,
          'description' => 'Fatura #' . $invoice->id . ' gerada',
          'created_at' => Carbon::now()->addMinutes(1),
          'updated_at' => Carbon::now()->addMinutes(1),
        ]);

        $data_fatura = Carbon::now()->format('d/m/Y');
        
        $details = [
          'title' => 'Nova fatura gerada',
          'customer' => $getCustomer->name,
          'company' => $getCustomer->company ? $getCustomer->company : '',
          'data_fatura' => $data_fatura,
          'data_vencimento' => Carbon::now()->addDays(7)->format('d/m/Y'),
          'price' => number_format($result['price_total'], 2),
          'payment_method' => $result['payment_method'],
          'description' => [
            $description
          ],
          'invoice_id' => $invoice->id,
          'url_base' => url('/')
        ];
        
        \Mail::to('contato@innsystem.com.br')->send(new \App\Mail\NewOrder($details));
        \Mail::to($getCustomer->email)->send(new \App\Mail\NewInvoice($details));
      } catch (\Exception $e) {
        \Log::error($e->getMessage());
        return response()->json($e->getMessage(), 500);
      }
    } catch (\Exception $e) {
      \Log::error($e->getMessage());
      return response()->json($e->getMessage(), 500);
    }
    
    return response()->json('Perfeito! Seu pedido foi registrado com sucesso. Na tela seguinte, você receberá as instruções para pagamento.', 200);
  }

  // Loja Virtual - Sucesso
  public function lojavirtualSuccess()
  {
    return view('frontend.pages.servicos.lojavirtual_success');
  }
}
