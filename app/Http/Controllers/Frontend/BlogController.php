<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BlogCategory;
use App\Models\BlogPost;
use DB;
use Carbon\Carbon;

class BlogController extends Controller
{

    public function index()
    {
        $getCategories = DB::table('blog_categories')
        ->select('id', 'name', 'icon', 'status', 'slug', 'description')
        ->orderBy('id', 'desc')
        ->where('status', 'ativo')
        ->get();

        return view('frontend.pages.helpdesk.index', compact('getCategories'));
    }

    public function showCategory($slugCategory)
    {
        $infoCategory = DB::table('blog_categories as a')
        ->select('a.id', 'a.name', 'a.slug as slugCategory')
        ->where('a.slug', $slugCategory)
        ->first();

        $results = DB::table('blog_posts as a')
            ->select('a.title', 'a.description', 'a.slug as slugPost', 'a.category_id', 'a.image', 'a.views', 'a.created_at', 'b.id as idCategory', 'b.name as nameCategory', 'b.slug as slugCategoria')
            ->join('blog_categories as b', 'b.id', '=', 'a.category_id')
            ->where('b.slug', $slugCategory)
            ->get();

        return view('frontend.pages.helpdesk.categories', compact('results', 'infoCategory'));
    }

    public function showPost($slugCategory, $slugPost)
    {
        $result = DB::table('blog_posts as a')
            ->select('a.title', 'a.description', 'a.slug as slugPost', 'a.category_id', 'a.image', 'a.views', 'a.updated_at', 'b.id as idCategory', 'b.name as nameCategory', 'b.slug as slugCategoria')
            ->join('blog_categories as b', 'b.id', '=', 'a.category_id')
            ->where('a.slug', $slugPost)
            ->first();
            
            DB::table('blog_posts as a')
            ->where('a.slug', $slugPost)
            ->update(['a.views' => DB::raw('a.views + 1')]);

        return view('frontend.pages.helpdesk.posts', compact('result'));
    }
}
