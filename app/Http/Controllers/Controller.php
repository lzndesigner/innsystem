<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use View;
use DB;
use App\Models\BlogCategory;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $blogcategories;
    protected $blogposts;

    public function __construct()
    {
        $this->blogcategories = DB::table('blog_categories as a')
                                    ->where('a.status', 'ativo')
                                    ->get();
        $this->blogposts = DB::table('blog_posts as b')
                                    ->where('b.status', 'ativo')
                                    ->get();

        View::share('blogcategories', $this->blogcategories);
        View::share('blogposts', $this->blogposts);
    }
}
