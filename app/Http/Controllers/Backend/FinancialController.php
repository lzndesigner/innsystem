<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AccountBank;
use App\Models\Expense;
use App\Models\Release;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Validator;

class FinancialController extends Controller
{
    protected $request;
    protected $fields;
    protected $datarequest;

    public function __construct(Request $request)
    {
        $this->request              =  $request;

        $this->datarequest = [
            'titulo'               =>  'Financeiro',
            'diretorio'            =>  'backend.financial',
            'url_action'               =>  'admin/financial'
        ];
    }

    // Dashboard
    public function index()
    {
        $getAccountBanks = AccountBank::where('type', 'private')->get();
        $getAccountInvestiments = AccountBank::where('type', 'investiment')->get();

        $getExpenses = Expense::get();

        $getReleases = Release::whereYear('date', \Carbon\Carbon::now()->format('Y'))->orderBy('date', 'DESC')->get();

        $groupedReleases = $getReleases->groupBy(function ($item) {
            return Carbon::parse($item->date)->format('m'); // Agrupa por mês
        });

        $totalsByMonth = $groupedReleases->mapWithKeys(function ($items, $month) {
            $totalEntradas = $items->where('type', 1)->sum('amount');
            $totalSaidas = $items->where('type', 2)->sum('amount');

            return [$month => [
                'total_entradas' => $totalEntradas,
                'total_saidas' => $totalSaidas
            ]];
        });

        return view($this->datarequest['diretorio'] . '.index', compact('getAccountBanks', 'getAccountInvestiments', 'getExpenses', 'getReleases', 'groupedReleases', 'totalsByMonth'))->with($this->datarequest);
    }

    // AccountBank
    public function accountBankCreate()
    {
        return view($this->datarequest['diretorio'] . '.form_account_bank');
    }

    public function accountBankStore()
    {
        $model = new AccountBank();
        $result = $this->request->all();

        $rules = [
            'name'     => "required",
            'amount' => 'required'
        ];

        $messages = [
            'name.required' => 'nome é obrigatório',
            'amount.required' => 'preço é obrigatório'
        ];

        $validator = Validator::make($result, $rules, $messages);

        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 422);
        }

        $model->name = $result['name'];
        $model->amount = $result['amount'];
        $model->type = $result['type'];
        $model->created_at = Carbon::now();
        $model->updated_at = Carbon::now();

        try {
            $model->save();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json('Conta Bancária cadastrada com sucesso!', 200);
    }

    public function accountBankEdit($id)
    {
        $result = AccountBank::where('id', $id)->first();

        return view($this->datarequest['diretorio'] . '.form_account_bank', compact('result'))->with($this->datarequest);
    }

    public function accountBankUpdate($id)
    {
        $model = AccountBank::find($id);

        $result = $this->request->all();

        $rules = [
            'name'     => "required",
            'amount' => 'required'
        ];

        $messages = [
            'name.required' => 'nome é obrigatório',
            'amount.required' => 'preço é obrigatório'
        ];

        $validator = Validator::make($result, $rules, $messages);

        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 422);
        }

        $model->name = $result['name'];
        $model->amount = $result['amount'];
        $model->type = $result['type'];
        $model->created_at = Carbon::now();
        $model->updated_at = Carbon::now();

        try {
            $model->save();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json('Conta Bancária editada com sucesso!', 200);
    }

    // Expenses
    public function expensesCreate()
    {
        return view($this->datarequest['diretorio'] . '.form_expenses');
    }

    public function expensesStore()
    {
        $model = new Expense();
        $result = $this->request->all();

        $rules = [
            'name'     => "required",
            'amount' => 'required',
            'next_due' => 'required'
        ];

        $messages = [
            'name.required' => 'nome é obrigatório',
            'amount.required' => 'preço é obrigatório',
            'next_due.required' => 'próximo vencimento é obrigatório'
        ];

        $validator = Validator::make($result, $rules, $messages);

        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 422);
        }

        $model->name = $result['name'];
        $model->amount = $result['amount'];
        $model->next_due = \Carbon\Carbon::parse($result['next_due'])->format('Y-m-d');
        $model->date_due = \Carbon\Carbon::parse($result['next_due'])->format('d');
        $model->created_at = Carbon::now();
        $model->updated_at = Carbon::now();

        try {
            $model->save();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json('Despesa cadastrada com sucesso!', 200);
    }

    public function expensesEdit($id)
    {
        $result = Expense::where('id', $id)->first();

        return view($this->datarequest['diretorio'] . '.form_expenses', compact('result'))->with($this->datarequest);
    }

    public function expensesUpdate($id)
    {
        $model = Expense::find($id);

        $result = $this->request->all();

        $rules = [
            'name'     => "required",
            'amount' => 'required',
            'next_due' => 'required',
        ];

        $messages = [
            'name.required' => 'nome é obrigatório',
            'amount.required' => 'preço é obrigatório',
            'next_due.required' => 'próximo vencimento é obrigatório',
        ];

        $validator = Validator::make($result, $rules, $messages);

        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 422);
        }

        $model->name = $result['name'];
        $model->amount = $result['amount'];
        $model->next_due = \Carbon\Carbon::parse($result['next_due'])->format('Y-m-d');
        $model->date_due = \Carbon\Carbon::parse($result['next_due'])->format('d');
        $model->created_at = Carbon::now();
        $model->updated_at = Carbon::now();

        try {
            $model->save();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json('Despesa editada com sucesso!', 200);
    }

    public function expensesDelete($id)
    {
        $model = Expense::find($id);

        try {
            $model->delete();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json('Despesa deletada com sucesso!', 200);
    }

    // Releases    
    public function releasesLoad(Request $request)
    {
        $result = $request->all();

        if (!$result['year']) {
            $year = \Carbon\Carbon::now()->format('Y');
        } else {
            $year = \Carbon\Carbon::parse($result['year'] . '-01-01')->format('Y');
        }

        $getReleases = Release::whereYear('date', $year)->orderBy('date', 'DESC')->get();

        $groupedReleases = $getReleases->groupBy(function ($item) {
            return Carbon::parse($item->date)->format('m'); // Agrupa por mês
        });

        $totalsByMonth = $groupedReleases->mapWithKeys(function ($items, $month) {
            $totalEntradas = $items->where('type', 1)->sum('amount');
            $totalSaidas = $items->where('type', 2)->sum('amount');

            return [$month => [
                'total_entradas' => $totalEntradas,
                'total_saidas' => $totalSaidas
            ]];
        });

        return view($this->datarequest['diretorio'] . '.list_releases', compact('year', 'getReleases', 'groupedReleases', 'totalsByMonth'));
    }

    public function releasesCreate()
    {
        $account_banks = AccountBank::get();

        return view($this->datarequest['diretorio'] . '.form_releases', compact('account_banks'));
    }

    public function releasesStore()
    {
        $model = new Release();
        $result = $this->request->all();

        $rules = [
            'account_id'     => "required",
            'name'     => "required",
            'amount' => 'required',
            'date' => 'required'
        ];

        $messages = [
            'account_id.required' => 'conta bancária é obrigatório',
            'name.required' => 'nome é obrigatório',
            'amount.required' => 'preço é obrigatório',
            'date.required' => 'data é obrigatório'
        ];

        $validator = Validator::make($result, $rules, $messages);

        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 422);
        }

        $model->type = $result['type'];
        $model->account_id = $result['account_id'];
        $model->name = $result['name'];
        $model->amount = $result['amount'];
        $model->date = \Carbon\Carbon::parse($result['date'])->format('Y-m-d');
        $model->created_at = Carbon::now();
        $model->updated_at = Carbon::now();

        try {
            $model->save();

            $getAccountBank = AccountBank::find($model->account_id);
            if ($getAccountBank) {
                if ($model->type == 1) {
                    $getAccountBank->amount += $model->amount;
                } else {
                    $getAccountBank->amount -= $model->amount;
                }
                $getAccountBank->save();
            }
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json('Lançamento cadastrado com sucesso!', 200);
    }

    public function releasesEdit($id)
    {
        $account_banks = AccountBank::get();
        $result = Release::where('id', $id)->first();

        return view($this->datarequest['diretorio'] . '.form_releases', compact('result', 'account_banks'))->with($this->datarequest);
    }

    public function releasesUpdate($id)
    {
        $model = Release::find($id);

        $result = $this->request->all();

        $rules = [
            'account_id'     => "required",
            'name'     => "required",
            'amount' => 'required',
            'date' => 'required'
        ];

        $messages = [
            'account_id.required' => 'conta bancária é obrigatório',
            'name.required' => 'nome é obrigatório',
            'amount.required' => 'preço é obrigatório',
            'date.required' => 'data é obrigatório'
        ];

        $validator = Validator::make($result, $rules, $messages);

        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 422);
        }

        $model->type = $result['type'];
        $model->account_id = $result['account_id'];
        $model->name = $result['name'];
        $model->amount = $result['amount'];
        $model->date = \Carbon\Carbon::parse($result['date'])->format('Y-m-d');
        $model->created_at = Carbon::now();
        $model->updated_at = Carbon::now();

        try {
            $model->save();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json('Lançamento editado com sucesso!', 200);
    }
}
