<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Release extends Model
{
    public function accountBank()
    {
        return $this->belongsTo(AccountBank::class, 'account_id');
    }
}
