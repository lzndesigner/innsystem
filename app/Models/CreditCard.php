<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CreditCard extends Model
{
    public function creditCardShoppings()
    {
        return $this->hasMany(CreditCardShopping::class, 'card_id');
    }
}
