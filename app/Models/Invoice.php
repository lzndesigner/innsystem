<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id',
        // 'description',
        'price',
        'status',
        'payment_method',
        'date_invoice',
        'date_end',
        'date_payment'
    ];

    protected $casts = [
        'description' => 'array'
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function accountBank()
    {
        return $this->belongsTo(AccountBank::class, 'payment_method');
    }
}
