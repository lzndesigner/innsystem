<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CreditCardShopping extends Model
{
    public function creditCard()
    {
        return $this->belongsTo(CreditCard::class, 'card_id');
    }
}
