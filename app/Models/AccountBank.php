<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccountBank extends Model
{
    public function releases()
    {
        return $this->hasMany(Release::class, 'account_id');
    }
}
