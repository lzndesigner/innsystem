<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BankAccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('account_banks')->insert([
            ['name' => 'Bradesco', 'amount' => 0, 'type' => 'private'],
            ['name' => 'Caixa', 'amount' => 0, 'type' => 'private'],
            ['name' => 'NuBank', 'amount' => 0, 'type' => 'private'],
            ['name' => 'Em mãos', 'amount' => 0, 'type' => 'private'],
        ]);
    }
}
