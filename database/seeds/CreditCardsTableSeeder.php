<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CreditCardsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('credit_cards')->insert([
            ['name' => 'CC Bradesco', 'date_due' => 10, 'limit' => 3500, 'invoice_next_due' => '2023-01-10', 'invoice_amount' => 800],
            ['name' => 'CC NuBank', 'date_due' => 17, 'limit' => 1000, 'invoice_next_due' => '2023-01-17', 'invoice_amount' => 300],
        ]);
    }
}
