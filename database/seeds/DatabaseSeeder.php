<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(CustomerSeeder::class);
        $this->call(ServiceSeeder::class);
        // $this->call(BankAccountsTableSeeder::class);        
        // $this->call(ExpensesTableSeeder::class);        
        // $this->call(CreditCardsTableSeeder::class);        
        // $this->call(CreditCardShoppingsTableSeeder::class);        
        // $this->call(ReleasesTableSeeder::class);        
    }
}
