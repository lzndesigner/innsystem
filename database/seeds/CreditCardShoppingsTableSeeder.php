<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CreditCardShoppingsTableSeeder extends Seeder
{
    public function run()
    {
        // Exemplo: inserir registros fictícios
        DB::table('credit_card_shoppings')->insert([
            ['card_id' => 1, 'name' => 'Compra do Cacto', 'amount' => 50, 'installments' => 1, 'date_buy' => now()],
        ]);
    }
}
