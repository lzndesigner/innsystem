<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ExpensesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('expenses')->insert([
            ['name' => 'Energia', 'amount' => 150, 'date_due' => 26, 'next_due' => '2023-01-26'],
            ['name' => 'Agua', 'amount' => 50, 'date_due' => 20, 'next_due' => '2023-01-20'],
            ['name' => 'Internet', 'amount' => 90, 'date_due' => 10, 'next_due' => '2023-01-10'],
            ['name' => 'Internet Móvel', 'amount' => 60, 'date_due' => 15, 'next_due' => '2023-01-15'],
            ['name' => 'Servidor Hostinger', 'amount' => 130, 'date_due' => 10, 'next_due' => '2023-01-10'],
        ]);
    }
}
