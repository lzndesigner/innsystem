<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ReleasesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('releases')->insert([
            ['type' => 1, 'account_id' => 2, 'name' => 'Recebimento do Serviço Murilo', 'amount' => 300.00, 'date' => '2023-12-20'],
        ]);
    }
}
