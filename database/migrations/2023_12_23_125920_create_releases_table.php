<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReleasesTable extends Migration
{
    public function up()
    {
        Schema::create('releases', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('type');
            $table->unsignedBigInteger('account_id');
            $table->string('name');
            $table->decimal('amount', 8, 2);
            $table->date('date');
            $table->timestamps();

            $table->foreign('account_id')->references('id')->on('account_banks')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('releases');
    }
}
