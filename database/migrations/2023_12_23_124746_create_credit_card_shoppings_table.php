<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCreditCardShoppingsTable extends Migration
{
    public function up()
    {
        Schema::create('credit_card_shoppings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('card_id');
            $table->string('name');
            $table->decimal('amount', 8, 2);
            $table->integer('installments');
            $table->date('date_buy');
            $table->timestamps();

            $table->foreign('card_id')->references('id')->on('credit_cards')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('credit_card_shoppings');
    }
}
