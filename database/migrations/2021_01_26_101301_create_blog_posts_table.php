<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_posts', function (Blueprint $table) {
            $table->id('id');
            $table->string('title');
            $table->string('category_id');
            $table->longText('description')->nullable();
            $table->string('image')->nullable();
            $table->string('views');
            $table->string('slug', 100)->unique(); 
            $table->index('slug'); // otimizar consulta
            $table->enum('status', ['ativo', 'desabilitado']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_posts');
    }
}
