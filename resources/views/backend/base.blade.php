<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
  <base href="{{Request::url()}}" />
  <title>Painel de Controle - @yield('title')</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="shortcut icon" href="{{ asset('galerias/favicon.ico?2') }}" />
  <!-- Include Fontawesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

  <link rel="stylesheet" href="{{ asset('general/css/bootstrap.css?2') }}" type="text/css" />
  <link rel="stylesheet" href="{{ asset('backend/css/template.css?2') }}" type="text/css" />
  <link rel="stylesheet" href="{{ asset('general/plugins/datepicker/css/bootstrap-datepicker.css?1') }}" type="text/css" />
  <link rel="stylesheet" href="{{ asset('/general/plugins/sweetalert/sweetalert2.min.css') }}">
  @yield('cssPage')
</head>

<body>
  <div class="boxed">
    <header>
      <div class="">
        <div class="d-flex">
          <div class="mx-3">
            <div id="logo">
              <a href="javascript:;">
                <img src="{{ asset('galerias/logo_azul_branco.webp') }}" alt="Painel de Controle" class="img-fluid" style="width:140px;">
              </a>
            </div><!-- logo -->
          </div><!-- cols -->
          <div class="flex-fill">
            <div class="header-toolbar float-right">
              <ul>
                <li><a href="{{ route('backend.settings') }}" data-toggle="tooltip" data-placement="bottom" title="Configurações"><i class="fa fa-cog"></i></a></li>
                <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" data-toggle="tooltip" data-placement="bottom" title="Deslogar-se"><i class="fa fa-sign-out"></i></a></li>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
                </form>
              </ul>
            </div>
          </div>
        </div><!-- d-flex -->
      </div><!-- container -->
    </header>
    <nav>
      <div class="">
        <div class="row">
          <div class="col-sm-12 col-md-12">
            @include('backend.includes.menuprincipal')
          </div>
        </div>
      </div><!-- container -->
    </nav>

    @yield('content')

    @yield('modalPage')

    <!-- modal Account Bank -->
    <div class="modal fade" id="modalAccountBank" tabindex="-1" role="dialog" aria-labelledby="modalAccountBankLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <form action="" class="form-horizontal" id="form-request-account-bank">
            <div class="modal-header">
              <h5 class="modal-title" id="modalAccountBankLabel">Conta Bancária</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body" id="content-account-bank">
            </div><!-- modal-body -->
            <div class="modal-footer">
              <button type="submit" class="btn btn-success" id="btn-salvar-account-bank"><i class="fa fa-check"></i> Salvar</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>
            </div>
          </form>
        </div>
      </div>
    </div><!-- modal Account Bank -->

    <!-- modalExpenses -->
    <div class="modal fade" id="modalExpenses" tabindex="-1" role="dialog" aria-labelledby="modalExpensesLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <form action="" class="form-horizontal" id="form-request-expenses">
            <div class="modal-header">
              <h5 class="modal-title" id="modalExpensesLabel">Despesas</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body" id="content-expenses">
            </div><!-- modal-body -->
            <div class="modal-footer">
              <button type="submit" class="btn btn-success" id="btn-salvar-expenses"><i class="fa fa-check"></i> Salvar</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>
            </div>
          </form>
        </div>
      </div>
    </div><!-- modalExpenses -->

    <!-- modalReleases -->
    <div class="modal fade" id="modalReleases" tabindex="-1" role="dialog" aria-labelledby="modalReleasesLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <form action="" class="form-horizontal" id="form-request-releases">
            <div class="modal-header">
              <h5 class="modal-title" id="modalReleasesLabel">Lançamentos</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body" id="content-releases">
            </div><!-- modal-body -->
            <div class="modal-footer">
              <button type="submit" class="btn btn-success" id="btn-salvar-releases"><i class="fa fa-check"></i> Salvar</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>
            </div>
          </form>
        </div>
      </div>
    </div><!-- modalReleases -->

    <footer>
      <p>Todos os Direitos Reservados</p>
    </footer>

    <div id="buttons_actions">
      <!-- Default dropup button -->
      <div class="btn-group dropup">
        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
          <i class="fa fa-plus"></i>
        </button>
        <div class="dropdown-menu">
          <!-- Dropdown menu links -->
          <li><a href="#" class="dropdown-item new-account-bank"><i class="fa fa-plus"></i> Conta Bancária</a></li>
          <li><a href="#" class="dropdown-item new-expenses"><i class="fa fa-plus"></i> Despesas</a></li>
          <li><a href="#" class="dropdown-item new-releases"><i class="fa fa-plus"></i> Lançamento</a></li>
        </div>
      </div>
    </div><!-- buttons_actions -->
  </div><!-- boxed -->

  <script type="text/javascript" src="{{ asset('general/js/jquery-3.5.1.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('general/js/bootstrap.bundle.js') }}"></script>
  <script src="https://unpkg.com/@popperjs/core@2/dist/umd/popper.js"></script>
  <script src="{{ asset('/general/plugins/sweetalert/sweetalert2.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('general/js/jquery.maskedinput.js?1') }}"></script>
  <script type="text/javascript" src="{{ asset('general/plugins/datepicker/js/bootstrap-datepicker.js') }}"></script>
  <script type="text/javascript" src="{{ asset('general/plugins/datepicker/locales/bootstrap-datepicker.pt-BR.min.js') }}"></script>
  @yield('jsPage')
  <script type="text/javascript" src="{{ asset('backend/js/functions.js?2') }}"></script>

  <!-- Account Bank -->
  <script>
    // Open Modal - Create
    $(document).on("click", ".new-account-bank", function() {
      $("#content-account-bank").html('');
      $("#modalAccountBank").modal('show');
      var url = `{{ route('backend.financial.account_bank.create') }}`;
      $.get(url,
        $(this)
        .addClass('modal-scrollfix')
        .find('#content-account-bank')
        .html('Carregando...'),
        function(data) {
          $("#content-account-bank").html(data);
          $("#btn-salvar-account-bank").attr('data-type', 'create');
          formatedPrice();
        });
    });

    // Open Modal - Edit
    $(document).on("click", ".edit-account-bank", function(e) {
      e.preventDefault();

      let id = $(this).data('id');

      $("#content-account-bank").html('');
      $("#modalAccountBank").modal('show');
      var url = `{{ url('/admin/financeiro/conta-bancaria/edit/${id}') }}`;
      $.get(url,
        $(this)
        .addClass('modal-scrollfix')
        .find('#content-account-bank')
        .html('Carregando...'),
        function(data) {
          $("#content-account-bank").html(data);
          $("#btn-salvar-account-bank").attr('data-type', 'edit').attr('data-id', id);
          formatedPrice();
        });
    });

    // Button Save Forms - Create and Edit
    $(document).on('click', '#btn-salvar-account-bank', function(e) {
      e.preventDefault();
      //disable the submit button
      $("form #btn-salvar-account-bank").attr("disabled", true);
      $('form #btn-salvar-account-bank').append('<i class="fa fa-spinner fa-spin ml-3"></i>');
      setTimeout(function() {
        $('form #btn-salvar-account-bank').prop("disabled", false);
        $('form #btn-salvar-account-bank').find('.fa-spinner').addClass('d-none');
      }, 5000);

      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': "{{ csrf_token() }}"
        }
      });

      let id = $(this).data('id');
      var type = $(this).data('type');

      if (type == 'edit') {
        if (id) {
          var url = `{{ url('/admin/financeiro/conta-bancaria/update/${id}') }}`;
          var method = 'POST';
        }
      } else {
        var url = "{{ url('/admin/financeiro/conta-bancaria/store') }}";
        var method = 'POST';
      }

      var data = $('#form-request-account-bank').serialize();

      $.ajax({
        url: url,
        data: data,
        method: method,
        success: function(data) {
          Swal.fire({
            text: data,
            icon: 'success',
            showClass: {
              popup: 'animate_animated animate_backInUp'
            },
            onClose: () => {
              // Loading page listagem
              location.reload();
            }
          });
        },
        error: function(xhr) {
          if (xhr.status === 422) {
            Swal.fire({
              text: 'Validação: ' + xhr.responseJSON,
              icon: 'warning',
              showClass: {
                popup: 'animate_animated animate_wobble'
              }
            });
          } else {
            Swal.fire({
              text: 'Erro interno, informe ao suporte: ' + xhr.responseJSON,
              icon: 'error',
              showClass: {
                popup: 'animate_animated animate_wobble'
              }
            });
          }
        }
      });
    });
  </script>

  <!-- Expenses -->
  <script>
    // Open Modal - Create
    $(document).on("click", ".new-expenses", function() {
      $("#content-expenses").html('');
      $("#modalExpenses").modal('show');
      var url = `{{ route('backend.financial.expenses.create') }}`;
      $.get(url,
        $(this)
        .addClass('modal-scrollfix')
        .find('#content-expenses')
        .html('Carregando...'),
        function(data) {
          $("#content-expenses").html(data);
          $("#btn-salvar-expenses").attr('data-type', 'create');
          formatedPrice();
        });
    });

    // Open Modal - Edit
    $(document).on("click", ".edit-expenses", function(e) {
      e.preventDefault();

      let id = $(this).data('id');

      $("#content-expenses").html('');
      $("#modalExpenses").modal('show');
      var url = `{{ url('/admin/financeiro/despesas/edit/${id}') }}`;
      $.get(url,
        $(this)
        .addClass('modal-scrollfix')
        .find('#content-expenses')
        .html('Carregando...'),
        function(data) {
          $("#content-expenses").html(data);
          $("#btn-salvar-expenses").attr('data-type', 'edit').attr('data-id', id);
          formatedPrice();
        });
    });

    // Button Save Forms - Create and Edit
    $(document).on('click', '#btn-salvar-expenses', function(e) {
      e.preventDefault();
      //disable the submit button
      $("form #btn-salvar-expenses").attr("disabled", true);
      $('form #btn-salvar-expenses').append('<i class="fa fa-spinner fa-spin ml-3"></i>');
      setTimeout(function() {
        $('form #btn-salvar-expenses').prop("disabled", false);
        $('form #btn-salvar-expenses').find('.fa-spinner').addClass('d-none');
      }, 5000);

      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': "{{ csrf_token() }}"
        }
      });

      let id = $(this).data('id');
      var type = $(this).data('type');

      if (type == 'edit') {
        if (id) {
          var url = `{{ url('/admin/financeiro/despesas/update/${id}') }}`;
          var method = 'POST';
        }
      } else {
        var url = "{{ url('/admin/financeiro/despesas/store') }}";
        var method = 'POST';
      }

      var data = $('#form-request-expenses').serialize();

      $.ajax({
        url: url,
        data: data,
        method: method,
        success: function(data) {
          Swal.fire({
            text: data,
            icon: 'success',
            showClass: {
              popup: 'animate_animated animate_backInUp'
            },
            onClose: () => {
              // Loading page listagem
              location.reload();
            }
          });
        },
        error: function(xhr) {
          if (xhr.status === 422) {
            Swal.fire({
              text: 'Validação: ' + xhr.responseJSON,
              icon: 'warning',
              showClass: {
                popup: 'animate_animated animate_wobble'
              }
            });
          } else {
            Swal.fire({
              text: 'Erro interno, informe ao suporte: ' + xhr.responseJSON,
              icon: 'error',
              showClass: {
                popup: 'animate_animated animate_wobble'
              }
            });
          }
        }
      });
    });

    $(document).on('click', '.remove-expenses', function(e) {
      e.preventDefault();
      
      Swal.fire({
        title: 'Deseja remover este registro?',
        text: "Você não poderá reverter isso!",
        icon: 'question',
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sim, deletar!'
      }).then((result) => {
        if (result.value) {

          $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': "{{ csrf_token() }}"
            }
          });
          
          let id = $(this).data('id');
          var url = `{{ url('/admin/financeiro/despesas/delete/${id}') }}`;
          var method = 'POST';

          $.ajax({
            url: url,
            method: method,
            success: function(data) {
              Swal.fire({
                text: data,
                icon: 'success',
                showClass: {
                  popup: 'animate_animated animate_backInUp'
                },
                onClose: () => {
                  // Loading page listagem
                  location.reload();
                }
              });
            },
            error: function(xhr) {
              if (xhr.status === 422) {
                Swal.fire({
                  text: 'Validação: ' + xhr.responseJSON,
                  icon: 'warning',
                  showClass: {
                    popup: 'animate_animated animate_wobble'
                  }
                });
              } else {
                Swal.fire({
                  text: 'Erro interno, informe ao suporte: ' + xhr.responseJSON,
                  icon: 'error',
                  showClass: {
                    popup: 'animate_animated animate_wobble'
                  }
                });
              }
            }
          });


        }
      });

    });
  </script>

  <!-- Releases -->
  <script>
    // Open Modal - Create
    $(document).on("click", ".new-releases", function() {
      $("#content-releases").html('');
      $("#modalReleases").modal('show');
      var url = `{{ route('backend.financial.releases.create') }}`;
      $.get(url,
        $(this)
        .addClass('modal-scrollfix')
        .find('#content-releases')
        .html('Carregando...'),
        function(data) {
          $("#content-releases").html(data);
          $("#btn-salvar-releases").attr('data-type', 'create');
          formatedPrice();
        });
    });

    // Open Modal - Edit
    $(document).on("click", ".edit-releases", function(e) {
      e.preventDefault();

      let id = $(this).data('id');

      $("#content-releases").html('');
      $("#modalReleases").modal('show');
      var url = `{{ url('/admin/financeiro/lancamentos/edit/${id}') }}`;
      $.get(url,
        $(this)
        .addClass('modal-scrollfix')
        .find('#content-releases')
        .html('Carregando...'),
        function(data) {
          $("#content-releases").html(data);
          $("#btn-salvar-releases").attr('data-type', 'edit').attr('data-id', id);
          formatedPrice();
        });
    });

    // Button Save Forms - Create and Edit
    $(document).on('click', '#btn-salvar-releases', function(e) {
      e.preventDefault();
      //disable the submit button
      $("form #btn-salvar-releases").attr("disabled", true);
      $('form #btn-salvar-releases').append('<i class="fa fa-spinner fa-spin ml-3"></i>');
      setTimeout(function() {
        $('form #btn-salvar-releases').prop("disabled", false);
        $('form #btn-salvar-releases').find('.fa-spinner').addClass('d-none');
      }, 5000);

      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': "{{ csrf_token() }}"
        }
      });

      let id = $(this).data('id');
      var type = $(this).data('type');

      if (type == 'edit') {
        if (id) {
          var url = `{{ url('/admin/financeiro/lancamentos/update/${id}') }}`;
          var method = 'POST';
        }
      } else {
        var url = "{{ url('/admin/financeiro/lancamentos/store') }}";
        var method = 'POST';
      }

      var data = $('#form-request-releases').serialize();

      $.ajax({
        url: url,
        data: data,
        method: method,
        success: function(data) {
          Swal.fire({
            text: data,
            icon: 'success',
            showClass: {
              popup: 'animate_animated animate_backInUp'
            },
            onClose: () => {
              // Loading page listagem
              location.reload();
            }
          });
        },
        error: function(xhr) {
          if (xhr.status === 422) {
            Swal.fire({
              text: 'Validação: ' + xhr.responseJSON,
              icon: 'warning',
              showClass: {
                popup: 'animate_animated animate_wobble'
              }
            });
          } else {
            Swal.fire({
              text: 'Erro interno, informe ao suporte: ' + xhr.responseJSON,
              icon: 'error',
              showClass: {
                popup: 'animate_animated animate_wobble'
              }
            });
          }
        }
      });
    });
  </script>
</body>

</html>