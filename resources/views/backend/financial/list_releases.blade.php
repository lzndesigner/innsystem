@php
$nameMonth = [
1 => 'Janeiro',
2 => 'Fevereiro',
3 => 'Março',
4 => 'Abril',
5 => 'Maio',
6 => 'Junho',
7 => 'Julho',
8 => 'Agosto',
9 => 'Setembro',
10 => 'Outubro',
11 => 'Novembro',
12 => 'Dezembro'
];
@endphp

@if(isset($getReleases) && count($getReleases) > 0)
<p class="text-muted fs-7 mb-2">Resultados de <b>{{$year}}</b></p>
@foreach ($groupedReleases as $month => $releases)
<div class="accordion" id="accordionExample2">
    <div class="card mb-3">
        <div class="card-header p-1" id="heading{{$month}}">
            <h2 class="mb-0 d-flex flex-wrap align-items-center justify-content-between">
                <button class="btn btn-block text-black text-left" type="button" data-toggle="collapse" data-target="#collapse{{$month}}" aria-expanded="true" aria-controls="collapse{{$month}}">
                    <b>{{$nameMonth[intval($month)]}}:</b> <span class="text-muted mb-2"><small>Entrada: R$ {{number_format($totalsByMonth[$month]['total_entradas'], 2, ',', '.')}} - Saída: R$ {{number_format($totalsByMonth[$month]['total_saidas'], 2, ',', '.')}}</small></span>
                </button>
            </h2>
        </div><!-- card-header -->
        <div id="collapse{{$month}}" class="collapse" aria-labelledby="heading{{$month}}" data-parent="#accordionExample2">
            <div class="card-body">
                <ul class="list-unstyled">
                    @foreach($releases as $release)
                    <li class="mb-2 d-flex flex-wrap align-items-center justify-content-between border-bottom pb-1">
                        <div class="">
                            @if($release->type == 1)
                            <span class="badge badge-success" style="font-size:8px;"><i class="fa fa-plus"></i></span>
                            @else
                            <span class="badge badge-danger" style="font-size:8px;"><i class="fa fa-minus"></i></span>
                            @endif
                            <small class="px-1">{{\Carbon\Carbon::parse($release->date)->format('d/m/Y')}}</small>
                            
                            {{$release->name}} 
                        </div>
                        <div>
                            <span class="badge">R$ {{ number_format($release->amount, 2, ',', '.')}}</span>
                            <a href="#" class="edit-releases fs-7" data-id="{{ $release->id }}"><i class="fa fa-edit"></i></a>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>
@endforeach
@else
<div class="alert alert-info">Desculpe, mas não encontramos nenhum resultados.</div>
@endif