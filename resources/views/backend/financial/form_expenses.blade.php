<div class="form-row align-items-center">
    <div class="col-xs-12 col-md-6">
        <div class="form-group">
            <label for="name" class="col-form-label">Nome (<span class="text-danger">*</span>):</label>
            <input type="text" id="name" name="name" class="form-control" placeholder="Nome" value="{{isset($result->name) ? $result->name : ''}}">
        </div><!-- form-group -->
    </div><!-- col -->

    <div class="col-xs-3 col-md-3">
        <div class="form-group">
            <label for="amount" class="col-form-label">Saldo Atual (<span class="text-danger">*</span>):</label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">R$</span>
                </div>
                <input type="text" id="amount" name="amount" class="form-control formatedPrice" placeholder="100,00" value="{{isset($result->amount) ? $result->amount : '0'}}">
            </div>
        </div><!-- form-group -->
    </div><!-- col -->
    <div class="col-xs-3 col-md-3">
        <div class="form-group">
            <label for="next_due" class="col-form-label">Próximo Vencimento (<span class="text-danger">*</span>):</label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">R$</span>
                </div>
                <input type="date" id="next_due" name="next_due" class="form-control" placeholder="100,00" value="{{isset($result->next_due) ? \Carbon\Carbon::parse($result->next_due)->format('Y-m-d') : \Carbon\Carbon::now()->format('Y-m-d')}}">
            </div>
        </div><!-- form-group -->
    </div><!-- col -->

</div><!-- form-row -->