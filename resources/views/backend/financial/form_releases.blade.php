<div class="form-row align-items-center">
    <div class="col-xs-12 col-md-3">
        <div class="form-group">
            <label for="type">Tipo:</label>
            <select name="type" id="type" class="form-control">
                @if(isset($result->type))
                <option value="1" {{ $result->type == "1" ? 'selected' : ''}}>Entrada</option>
                <option value="2" {{ $result->type == "2" ? 'selected' : ''}}>Saída</option>
                @else
                <option value="1" selected>Entrada</option>
                <option value="2">Saída</option>
                @endif
            </select>
        </div>
    </div><!-- col -->

    <div class="col-xs-12 col-md-3">
        <div class="form-group">
            <label for="account_id">Conta Bancária:</label>
            <select name="account_id" id="account_id" class="form-control">
                <option value=""></option>
                @foreach ($account_banks as $account_bank)
                <option value="{{ $account_bank->id }}" {{ (isset($result) ? ($account_bank->id == $result->account_id ? 'selected' : '') : '') }}>{{ $account_bank->name }}</option>
                @endforeach
            </select>
        </div>
    </div><!-- col -->
</div>
<div class="form-row align-items-center">
    <div class="col-xs-12 col-md-6">
        <div class="form-group">
            <label for="name" class="col-form-label">Nome (<span class="text-danger">*</span>):</label>
            <input type="text" id="name" name="name" class="form-control" placeholder="Nome" value="{{isset($result->name) ? $result->name : ''}}">
        </div><!-- form-group -->
    </div><!-- col -->

    <div class="col-xs-3 col-md-3">
        <div class="form-group">
            <label for="amount" class="col-form-label">Saldo Atual (<span class="text-danger">*</span>):</label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">R$</span>
                </div>
                <input type="text" id="amount" name="amount" class="form-control formatedPrice" placeholder="100,00" value="{{isset($result->amount) ? $result->amount : '0'}}">
            </div>
        </div><!-- form-group -->
    </div><!-- col -->
    <div class="col-xs-3 col-md-3">
        <div class="form-group">
            <label for="date" class="col-form-label">Data (<span class="text-danger">*</span>):</label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">R$</span>
                </div>
                <input type="date" id="date" name="date" class="form-control" placeholder="100,00" value="{{isset($result->date) ? \Carbon\Carbon::parse($result->date)->format('Y-m-d') : \Carbon\Carbon::now()->format('Y-m-d')}}">
            </div>
        </div><!-- form-group -->
    </div><!-- col -->

</div><!-- form-row -->