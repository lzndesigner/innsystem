<div class="form-row align-items-center">
    <div class="col-xs-12 col-md-6">
        <div class="form-group">
            <label for="name" class="col-form-label">Nome (<span class="text-danger">*</span>):</label>
            <input type="text" id="name" name="name" class="form-control" placeholder="Nome" value="{{isset($result->name) ? $result->name : ''}}">
        </div><!-- form-group -->
    </div><!-- col -->

    <div class="col-xs-3 col-md-3">
        <div class="form-group">
            <label for="amount" class="col-form-label">Saldo Atual (<span class="text-danger">*</span>):</label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">R$</span>
                </div>
                <input type="text" id="amount" name="amount" class="form-control formatedPrice" placeholder="100,00" value="{{isset($result->amount) ? $result->amount : '0'}}">
            </div>
        </div><!-- form-group -->
    </div><!-- col -->

    <div class="col-xs-12 col-md-3">
        <div class="form-group">
            <label for="type">Tipo:</label>
            <select name="type" id="type" class="form-control">
                @if(isset($result->type))
                <option value="private" {{ $result->type == "private" ? 'selected' : ''}}>Pessoal</option>
                <option value="investiment" {{ $result->type == "investiment" ? 'selected' : ''}}>Investimento</option>
                @else
                <option value="private" selected>Pessoal</option>
                <option value="investiment">Investimento</option>
                @endif
            </select>
        </div>
    </div><!-- col -->
</div><!-- form-row -->