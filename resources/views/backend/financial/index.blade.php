@extends('backend.base')
@section('title', $titulo)

@section('content')
<section>
  <div class="">
    <div class="row">
      <div class="col-xs-12 col-md-3 mb-4">
        <div class="accordion" id="accordionExample">
          <div class="card mb-3">
            <div class="card-header p-1" id="headingOne">
              <h2 class="mb-0 d-flex flex-wrap align-items-center justify-content-between">
                <button class="btn btn-block text-black text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                  Contas Bancárias
                </button>
              </h2>
            </div><!-- card-header -->
            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
              <div class="card-body">
                @php
                $acocuntAmountTotal = 0;
                @endphp
                @if(isset($getAccountBanks) && count($getAccountBanks) > 0)
                <ul class="list-unstyled">
                  @foreach($getAccountBanks as $account_bank)
                  <li class="pb-2">{{$account_bank->name}}: R$ {{ number_format($account_bank->amount, 2, ',', '.') }} <a href="#" class="edit-account-bank" data-id="{{ $account_bank->id }}"><i class="fa fa-edit"></i></a></li>
                  @php
                  $acocuntAmountTotal += $account_bank->amount;
                  @endphp
                  @endforeach
                  <li class="fs-7 text-muted"><b>Total: R$ {{number_format($acocuntAmountTotal, 2, ',', '.')}}</b></li>
                </ul>
                @endif
                <a href="#" class="btn btn-sm btn-success new-account-bank"><i class="fa fa-plus"></i> Nova Conta</a>
              </div>
            </div><!-- collapseOne -->
          </div><!-- card -->


          @if(isset($getAccountInvestiments) && count($getAccountInvestiments) > 0)
          <div class="card mb-3">
            <div class="card-header p-1" id="headingOne">
              <h2 class="mb-0">
                <button class="btn btn-block text-black btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                  Saldo de Investimentos
                </button>
              </h2>
            </div><!-- card-header -->
            <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordionExample">
              <div class="card-body">
                <ul class="list-unstyled">
                  @foreach($getAccountInvestiments as $account_investiment)
                  <li class="pb-2">{{$account_investiment->name}}: R$ {{ number_format($account_investiment->amount, 2, ',', '.') }} <a href="#" class="edit-account-bank" data-id="{{ $account_bank->id }}"><i class="fa fa-edit"></i></a></li>
                  @endforeach
                </ul>
                <a href="#" class="btn btn-sm btn-success new-account-bank"><i class="fa fa-plus"></i> Nova Conta</a>
              </div>
            </div><!-- collapseTwo -->
          </div><!-- card -->
          @endif

          @if(isset($getExpenses) && count($getExpenses) > 0)
          <div class="card mb-3">
            <div class="card-header p-1" id="headingThree">
              <h2 class="mb-0 d-flex flex-wrap align-items-center justify-content-between">
                <button class="btn btn-block text-black text-left" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                  Contas a Pagar
                </button>
              </h2>
            </div><!-- card-header -->
            <div id="collapseThree" class="collapse show" aria-labelledby="headingThree" data-parent="#accordionExample">
              <div class="card-body">
                @if(isset($getExpenses) && count($getExpenses) > 0)
                <ul class="list-unstyled">
                  @foreach($getExpenses as $expense)
                  <li class="pb-3 border-bottom mb-3">
                    <div class="d-flex flex-wrap justify-content-between">
                      <b>{{$expense->name}}</b>
                      <div>
                        <a href="#" class="edit-expenses" data-id="{{ $expense->id }}"><i class="fa fa-edit"></i></a>
                        <a href="#" class="remove-expenses text-danger" data-id="{{ $expense->id }}"><i class="fa fa-times"></i></a>
                      </div>
                    </div>
                    <small class="text-muted">R$ {{ number_format($expense->amount, 2, ',', '.') }}</small>
                    @if(\Carbon\Carbon::parse($expense->next_due) <= \Carbon\Carbon::now()) <span class="badge fs-8 badge-danger">{{ \Carbon\Carbon::parse($expense->next_due)->format('d/m/Y') }}</span>
                      @elseif(\Carbon\Carbon::parse($expense->next_due) == \Carbon\Carbon::now())
                      <span class="badge fs-8 badge-warning">{{ \Carbon\Carbon::parse($expense->next_due)->format('d/m/Y') }}</span>
                      @else
                      <span class="badge fs-8 badge-success">{{ \Carbon\Carbon::parse($expense->next_due)->format('d/m/Y') }}</span>
                      @endif

                  </li>
                  @endforeach
                </ul>
                @endif
                <a href="#" class="btn btn-sm btn-success new-expenses"><i class="fa fa-plus"></i> Nova Despesa</a>
              </div>
            </div><!-- collapseThree -->
          </div><!-- card -->
          @endif
        </div><!-- #accordionExample -->
      </div>

      <div class="col-xs-12 col-md-7 mb-4">
        <div class="card">
          <div class="card-body">
            <div class="d-flex flex-wrap justify-content-between align-items-center">
              <h5 class="card-title mb-0">Entrada e Saída</h5>

              <select name="releasesMonth" id="releasesMonth" class="form-control" style="max-width:150px;">
                @for ($ano = 2024; $ano >= 2015; $ano--)
                <option value="{{ $ano }}">{{ $ano }}</option>
                @endfor
              </select>
            </div>

            <hr>

            <div id="list_releases"></div>
          </div>
        </div>
      </div>

      <div class="col-xs-12 col-md-3 d-none">
        <div class="card mb-4">
          <div class="card-body">
            <h5 class="card-title mb-0">C.Crédito Bradesco</h5>
            <p class="text-muted"><small>Limite: R$ 3.500 - (10/01/2024 = R$ 700,00)</small></p>

            <p><b>Fatura mês Dezembro (10/12/2023)</b></p>
            <ul class="list-unstyled">
              <li>Compra Cacto: R$ 50,00 (23/12/2023)</li>
            </ul>

            <p><b>Fatura mês Janeiro (10/01/2024)</b></p>
            <ul class="list-unstyled">
              <li>Compra Cacto: R$ 50,00 (23/12/2023)</li>
            </ul>
          </div>
        </div>

        <div class="card">
          <div class="card-body">
            <h5 class="card-title mb-0">C.Crédito Bradesco</h5>
            <p class="text-muted"><small>Limite: R$ 3.500 - (10/01/2024 = R$ 700,00)</small></p>

            <p><b>Fatura mês Dezembro (10/12/2023)</b></p>
            <ul class="list-unstyled">
              <li>Compra Cacto: R$ 50,00 (23/12/2023)</li>
            </ul>

            <p><b>Fatura mês Janeiro (10/01/2024)</b></p>
            <ul class="list-unstyled">
              <li>Compra Cacto: R$ 50,00 (23/12/2023)</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>
@endsection

@section('cssPage')
@endsection

@section('jsPage')
<script>
  function enableLoading(target) {
    disableLoading(target);
    if ($(target + ' > .loading').length == 0) {
      $(target).append(`<div class="loading active">
                <div class="container"><div class="d-flex justify-content-center">
                    <div class="col-sm-8">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="margin:auto;display:block;" width="50px" height="50px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
                        <circle cx="50" cy="50" fill="none" stroke="#41bfec" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138">
                        <animateTransform attributeName="transform" type="rotate" repeatCount="indefinite" dur="1s" values="0 50 50;360 50 50" keyTimes="0;1"></animateTransform>
                        </circle>
                    </svg></div></div></div>
          </div>`);
    }
  }

  function disableLoading(target) {
    $(target + ' > .loading').remove();
  }

  function loadReleases(year) {
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': "{{ csrf_token() }}"
      }
    });

    $.ajax({
      type: 'GET',
      url: '/admin/financeiro/lancamentos/load?year=' + year,
      beforeSend: function() {
        enableLoading('#list_releases');
      }
    }).done(function(data) {
      $("#list_releases").html(data);
      disableLoading('#list_releases');
    });
  }

  $(document).ready(function() {
    loadReleases(new Date().getFullYear());
  });

  $('#releasesMonth').on('change', function() {
    var year_selected = $(this).val();
    loadReleases(year_selected);
  });
</script>
@endsection