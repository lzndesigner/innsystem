@extends('backend.base')
@section('title', isset($result->title) ? $result->title : 'Postagem não Encontrada'))

@section('content')
<section>
  <div class="">
    <div class="row">
      <div class="col-xs-12 col-md-12">
        <div class="card">
          <div class="card-body">
            @if(isset($result))
            <h5 class="card-title">@yield('title') - <small>{{ $result->nameCategory }}</small></h5>
            <hr>
            {{$result->description}}
            <hr>
            @else
            <h5 class="card-title">@yield('title')</h5>
            Desculpe, mas não encontramos a publicação solicitada.
            @endif
            <a href="{{ url('/admin/blog/posts') }}" class="btn btn-sm btn-primary">Voltar</a>
          </div><!-- card-body -->
        </div><!-- card -->
      </div><!-- cols -->
    </div><!-- row -->
  </div><!--  -->
</section>
@endsection