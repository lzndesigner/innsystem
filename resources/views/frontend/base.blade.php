<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <base href="{{ Request::url() }}" />
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>InnSystem | @yield('title')</title>
    <meta name="description" content="Somos um empresa especializada no desenvolvimento de sistemas web, são mais de 10 anos unindo inovação em usabilidade com as principais tecnologias e ferramentas do mercado.">
    <meta name="keywords" content="innsys, innsystem, inovação, de, sistemas, inovação de sistemas, inn, innovation, innovative, system, sys, loja, virtual, vendas, online, loja virtual, crie sua loja, criar loja, criação de site, e-commerce, responsive, personalizado, correios, pagseguro, paypal, loja personalizada, loja pronta, loja completa, loja sem limite, mu, muonline, webzen, mu online, games" />
    <meta name="author" content="Leonardo Augusto">
    <meta name="robots" content="index, follow" />
    <meta name="googlebot" content="index, follow" />
    <meta name="language" content="pt-br" />
    <meta name="revisit-after" content="3 days">
    <meta name="rating" content="general" />
    <!-- metas rede social: facebook -->
    <meta property="og:locale" content="pt_BR" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://innsystem.com.br/" />
    <meta property="og:site_name" content="InnSystem" />
    <meta property="og:image" content="{{ asset('https://innsystem.com.br/galerias/facebook_2.png') }}" />
    <meta property="og:image:type" content="image/png" />
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="800" />
    <meta property="og:description" content="Somos um empresa especializada no desenvolvimento de sistemas web, são mais de 10 anos unindo inovação em usabilidade com as principais tecnologias e ferramentas do mercado." />
    @yield('headPage')

    <!-- Include Favicon -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;800;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Play:wght@400;700&display=swap" rel="stylesheet">
    <link rel="shortcut icon" href="{{ asset('galerias/favicon.ico?2') }}" />
    <!-- Include Fontawesome -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"> -->
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('frontend/css/bootstrap.min.css?1') }}" rel="stylesheet">
    <!-- main css file -->
    <link href="{{ asset('frontend/css/main.css?9') }}" rel="stylesheet">
    <!-- custom css file -->
    <link href="{{ asset('frontend/css/custom.css?9') }}" rel="stylesheet">
    <link href="{{ asset('general/css/complements.css?7') }}" rel="stylesheet">
    @yield('cssPage')
    <link href="{{ asset('frontend/prettyphoto/css/prettyPhoto.css?1') }}" rel="stylesheet">
</head>

<body>
    @include('frontend.includes.loadingpage')

    @include('frontend.includes.header')

    @yield('breadcrumb')

    @yield('content')

    @include('frontend.includes.footer')

    
    <!-- jquery -->
    <script src="{{ asset('frontend/js/jquery.min.js') }}"></script>
    <script src="{{ asset('frontend/js/popper.min.js') }}"></script>
    <!-- bootstrap JavaScript -->
    <script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('general/js/jquery.maskedinput.js?1') }}"></script>
    <!-- template JavaScript -->
    <script src="{{ asset('frontend/js/template-scripts.js?3') }}"></script>
    <!-- flickity JavaScript -->
    <script src="{{ asset('frontend/js/flickity.pkgd.min.js') }}"></script>
    <!-- carousel JavaScript -->
    <script src="{{ asset('frontend/owlcarousel/owl.carousel.min.js') }}"></script>
    <!-- parallax JavaScript -->
    <script src="{{ asset('frontend/js/parallax.min.js') }}"></script>
    <!-- mailchamp JavaScript -->
    <script src="{{ asset('frontend/js/mailchamp.js') }}"></script>
    <!-- bootstrap offcanvas -->
    <script src="{{ asset('frontend/js/bootstrap.offcanvas.min.js') }}"></script>
    <!-- touchSwipe JavaScript -->
    <script src="{{ asset('frontend/js/jquery.touchSwipe.min.js') }}"></script>
    <!-- seconde style additionel JavaScript -->
    <script src="{{ asset('frontend/js/particles-code.js') }}"></script>
    <script src="{{ asset('frontend/js/particles.js') }}"></script>
    <script src="{{ asset('frontend/js/smoothscroll.js') }}"></script>
    <script src="{{ asset('frontend/prettyphoto/js/jquery.prettyPhoto.js') }}"></script>

    @yield('jsPage')

    <script src="https://kit.fontawesome.com/04571ab3d2.js" crossorigin="anonymous"></script>
    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-KKMSQ5ZTDZ"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-KKMSQ5ZTDZ');
    </script>

    <!-- <!--Start of Tawk.to Script--
    <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/6071cac5f7ce182709390d11/1f2u805e2';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
    <!--End of Tawk.to Script-- -->

</body>

</html>