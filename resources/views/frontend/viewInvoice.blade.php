@extends('frontend.base')
@section('title', 'Fatura #' . $result->id)

@section('content')
    <section class="p-5">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h2 class="card-title">@yield('title')
                                @if ($result->status == 'pago')
                                    <span class="badge badge-success">Pago</span>
                                @elseif($result->status == 'nao_pago')
                                    <span class="badge badge-danger">Em aberto</span>
                                @else
                                    <span class="badge badge-dark">Cancelado</span>
                                @endif
                            </h2>

                            <ul>
                                <li>Data da Fatura: {{ \Carbon\Carbon::parse($result->date_invoice)->format('d/m/Y') }}
                                </li>
                                <li>Forma de Pagamento: {{ $result->payment_method }}</li>
                            </ul>
                            <hr>
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <h4>Descrição da Fatura</h4>
                                    <ul>
                                        @foreach ($result->description as $description)
                                            <li>{{ $description }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <p><b>Pagar a:</b></p>
                                    <ul>
                                        <li>InnSystem Inovação em Sistemas</li>
                                        <li>contato@innsystem.com.br</li>
                                        <li>CNPJ: 21.635.247/0001-54</li>
                                    </ul>
                                </div><!-- cols -->
                                <div class="col-xs-12 col-md-6">
                                    <p><b>Faturado para:</b></p>
                                    <ul>
                                        <li>{{ $getCustomer->name }} ({{ $getCustomer->company }})</li>
                                        <li>{{ $getCustomer->address }}, N° {{ $getCustomer->number }}</li>
                                        <li>{{ $getCustomer->city }} - {{ $getCustomer->state }}</li>
                                        <li>{{ $getCustomer->phone }}</li>
                                    </ul>
                                </div><!-- cols -->
                            </div>

                            @if ($result->status == 'nao_pago')
                                <hr>

                                <h4>Formas de Pagamentos</h4>
                                <ul class="nav nav-tabs nav-payments" id="myTab" role="tablist">
                                    <li class="nav-item" role="presentation">
                                        <a class="nav-link active" id="nubank" data-toggle="tab" href="#tab_nubank"
                                            role="tab" aria-controls="tab_nubank" aria-selected="true">
                                            <img src="{{ asset('/galerias/payment/nubank.png') }}"
                                                alt="NuBank - Boleto"></a>
                                    </li>
                                </ul>
                                <div class="tab-content tab-payments p-4" id="myTabContent">
                                    <div class="tab-pane fade show active" id="tab_nubank" role="tabpanel"
                                        aria-labelledby="nubank">
                                        <!-- conteudo -->
                                        <h4>Instrução de Pagamento - NuBank</h4>
                                        <p><b>Pagamento por boleto</b>: solicite o um boleto através do e-mail <a
                                                href="mailto:contato@innsystem.com.br"><b>contato@innsystem.com.br</b></a> ou no WhatsApp <a href="https://wa.me/5516992747526" target="_Blank"><b>(16) 9.9274-7526</b></a></p>
                                        <div class="row mt-5">
                                            <div class="col-xs-12 col-md-6">
                                                <p><b>Pagamento por transferência</b>: acesse sua conta através do
                                                    Aplicativo, clique na opção "Transferir". <br> Depois clique na opção
                                                    "Transferir para um novo contato".</p>
                                                <blockquote>
                                                    <p><b>Transferir ou Depósito para:</b></p>
                                                    <ul>
                                                        <li>Nome: Leonardo Augusto Costa dos Santos</li>
                                                        <li>CPF: 415.524.658-96</li>
                                                        <li>Banco: Nu Pagamentos S.A. (260)</li>
                                                        <li>Agência: 0001</li>
                                                        <li>Conta: 46287812-3</li>
                                                    </ul>
                                                </blockquote>
                                                <p>A confirmação de pagamento ocorre entre 1~2 dias úteis.</p>
                                            </div>
                                            <div class="col-xs-12 col-md-6">
                                                <p><b>Pagamento por QR Code</b>: Através do seu Aplicativo do Banco, aponte
                                                    a camera para o QRCode e finalize com o pagamento do valor da sua Fatura
                                                </p>
                                                <img src="{{ asset('/galerias/payment/qrcode_nubank.png') }}"
                                                    class="img-fluid" alt="Instrução NuBank" />
                                            </div>
                                        </div>

                                        <hr>

                                        <h4>Instrução de Pagamento - Depósito ou Transferência</h4>
                                        <p>Você pode realizar a transferência ou depósito direto em conta. <br>
                                            Confirmar enviando o comprovante para o e-mail <a
                                                href="mailto:contato@innsystem.com.br"><b>contato@innsystem.com.br</b></a>
                                            ou no WhatsApp <a href="https://wa.me/5516992747526"
                                                target="_Blank"><b>(16) 9.9274-7526</b></a></p>
                                        <div class="row row-payments mt-2">
                                            <div class="col-xs-12 col-md-6">
                                                <img src="{{ asset('/galerias/payment/bradesco.png') }}"
                                                    class="img-fluid" alt="Bradesco">
                                                <blockquote>
                                                    <p><b>Banco do Bradesco</b></p>
                                                    <ul>
                                                        <li>Agência: 2307-8</li>
                                                        <li>Conta: 0500192-7</li>
                                                        <li>Conta Corrente</li>
                                                        <li>Leonardo Augusto Costa dos Santos</li>
                                                    </ul>
                                                </blockquote>
                                            </div>
                                            <div class="col-xs-12 col-md-6">
                                                <img src="{{ asset('/galerias/payment/caixa.png') }}" class="img-fluid"
                                                    alt="Caixa">
                                                <blockquote>
                                                    <p><b>Banco da Caixa</b></p>
                                                    <ul>
                                                        <li>Agência: 2947</li>
                                                        <li>Operação: 013</li>
                                                        <li>Conta: 00016127-2</li>
                                                        <li>Conta Poupança</li>
                                                        <li>Leonardo Augusto Costa dos Santos</li>
                                                    </ul>
                                                </blockquote>
                                            </div>
                                        </div>
                                        <p>A confirmação de pagamento ocorre dentro de 2~3 dias úteis.</p>
                                        <!-- conteudo -->
                                    </div><!-- tab-panel -->
                                </div>

                            @endif
                        </div><!-- card-body -->
                    </div><!-- card -->
                </div><!-- cols -->
            </div><!-- row -->
        </div><!--  -->
    </section>
@endsection

@section('cssPage')
@endsection

@section('jsPage')
@endsection
