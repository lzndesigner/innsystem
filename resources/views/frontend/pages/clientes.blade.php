@extends('frontend.base')
@section('title', 'Conheça nossos clientes')

@section('content')

<section class="padding-100-0 position-relative">
    <div class="container">

        <div class="row justify-content-center mr-tp-0 ">
            <div class="col-md-8 text-center">
                <h2 class="side-text-right-title">Nossos Clientes</h2>
            </div>
        </div>

        <div class="row justify-content-center mr-tp-0 ">
            <div class="col-6 col-md-2">
                <a href="https://bcvistorias.com.br/" target="_Blank">
                    <img src="{{ asset('galerias/clientes/bcvistorias.png') }}" alt="BC Vistorias Imobiliarias" class="img-fluid" />
                </a>
            </div><!-- item -->
            <div class="col-6 col-md-2">
                <a href="https://plentychem.com.br" target="_Blank">
                    <img src="{{ asset('galerias/clientes/plentychem.png') }}" alt="Plentychem" class="img-fluid" />
                </a>
            </div><!-- item -->
            <div class="col-6 col-md-2">
                <a href="https://ecocolorbrasil.com.br" target="_Blank">
                    <img src="{{ asset('galerias/clientes/ecocolor.webp') }}" alt="Eco Color" class="img-fluid" />
                </a>
            </div><!-- item -->
            <div class="col-6 col-md-2">
                <a href="https://sunworld.com.br" target="_Blank">
                    <img src="{{ asset('galerias/clientes/sunworld.webp') }}" alt="Sun World" class="img-fluid" />
                </a>
            </div><!-- item -->
            <div class="col-6 col-md-2">
                <a href="https://dmultydistribuidora.com.br" target="_Blank">
                    <img src="{{ asset('galerias/clientes/dmulty.webp') }}" alt="DMulty Distribuidora" class="img-fluid" />
                </a>
            </div><!-- item -->
            <div class="col-6 col-md-2">
                <a href="https://decorarhomeservice.com.br/" target="_Blank">
                    <img src="{{ asset('galerias/clientes/decorar.png') }}" alt="Decorar Home Service" class="img-fluid" />
                </a>
            </div><!-- item -->
            <div class="col-6 col-md-2">
                <a href="https://kiwimidia.com" target="_Blank">
                    <img src="{{ asset('galerias/clientes/kiwimidia.webp') }}" alt="Kiwimidia" class="img-fluid" />
                </a>
            </div><!-- item -->
            <div class="col-6 col-md-2">
                <a href="https://store.inglesiw.com.br" target="_Blank">
                    <img src="{{ asset('galerias/clientes/ingles_iw.webp') }}" alt="Inglês iW Store" class="img-fluid" />
                </a>
            </div><!-- item -->
            <div class="col-6 col-md-2">
                <a href="https://fitadelaco.com.br" target="_Blank">
                    <img src="{{ asset('galerias/clientes/fita_de_laco.webp') }}" alt="Fita de Laço" class="img-fluid" />
                </a>
            </div><!-- item -->
        </div>
    </div>
</section>

@endsection


@section('jsPage')
@endsection
@section('cssPage')
@endsection