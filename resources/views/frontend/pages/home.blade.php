@extends('frontend.base')
@section('title', 'Criação de Site, Loja Virtual e Design Gráfico')

@section('main_home')
<main class="container mb-auto">
  <section>
    <div class="container">
      <div class="row justify-content-start futures-version-2">

        <div class="col-md-4">
          <div class="futures-version-2-box">
            <i class="fa fa-cart-arrow-down"></i>
            <h5>Loja Virtual <small>(E-commerce)</small></h5>
            <p>Ofereça seus produtos e serviços para o mundo, através de uma loja totalmente exclusiva, feita sob medida, com segurança, suporte e de fácil administração.</p>
            <a class="btn btn-sm btn-primary btn-icon-green" href="{{ url('/loja-virtual') }}">conheça mais</a>
          </div>
        </div>

        <div class="col-md-4">
          <div class="futures-version-2-box">
            <i class="fa fa-magic"></i>
            <h5>Design Gráfico</h5>
            <p>A identidade visual aumenta significativamente a visibilidade e qualidade dos serviços ou produtos oferecidos aos consumidores.</p>
            <a class="btn btn-sm btn-primary btn-icon-purple" href="{{ url('/design-grafico') }}">conheça mais</a>
          </div>
        </div>

        <div class="col-md-4">
          <div class="futures-version-2-box">
            <i class="fa fa-desktop"></i>
            <h5>Criação de Site</h5>
            <p>Um site bem elaborado, criativo traz uma experiência incrível para os clientes, utilizando ferramentas e funcionalidades que aumentam a visibilidade do seu negócio.</p>
            <a class="btn btn-sm btn-primary btn-icon-blue" href="{{ url('/criacao-sites') }}">conheça mais</a>
          </div>
        </div>

      </div>
    </div>
  </section>
</main>
<div class="mt-auto"></div>
@endsection

@section('content')

<section class="padding-70-0 position-relative">
  <div class="container">
    <div class="row justify-content-between">
      <div class="col-md-7 side-text-right-container d-flex mx-auto flex-column">
        <div class="mb-auto"></div>
        <h2 class="side-text-right-title f-size25">E-commerce<br> <small>Loja Virtual Exclusiva</small></h2>
        <p class="side-text-right-text f-size16">
          Tenha a sua disposição os principais recursos para o bom funcionamento de uma Loja Virtual, como a simulação de frete com os Correios e Transportadoras, principais formas de pagamentos como o Mercado Pago, PagSeguro, PayPal e outras empresas.
        </p>
        <p class="side-text-right-text f-size16">
          Integre sua loja com os maiores Marketplaces, Mercado Livre, B2W e outras. Aumente a visibilidade e vendas administrando tudo através de um único lugar.
        </p>
        <p class="side-text-right-text f-size16">
          O gerenciamento é feito através de um painel de controle simples e de fácil uso, oferecemos também um treinamento para você começar sua loja, explicando detalhadamente cada passo a ser realizado e exemplos prático.
        </p>
        <p>
          <a class="plan-dedicated-order-button" href="https://wa.me/5516992747526" target="_Blank">Entre em Contato</a>
        </p>
        <div class="mt-auto"></div>
      </div>
      <div class="col-md-5 pad-tp-60 about-us-img-section">
        <img src="{{ asset('galerias/pages/loja-virtual.webp') }}" alt="Loja Virtual">
      </div>
    </div>
  </div>
</section>



<section class="section-wth-amwaj">
  <div class="bg_overlay_section-amwaj">
    <img src="{{ asset('frontend/img/bg/b_bg_02.jpg') }}" alt="img-bg">
  </div>

  <div class="container">
    <div class="row justify-content-between mr-tp-50">
      <div class="col-md-7 side-text-right-container">
        <h2 class="side-text-right-title">Criação de Site<br> <small>gerenciável & responsivo</small></h2>
        <p class="side-text-right-text f-size-16">
          Desenvolvemos sites para as mais diversas áreas, Advogados, Arquitetos, Dentistas, Fotógrafos, Gráficas, Hotéis, Imobiliárias, Médicos, Pousadas, Restaurantes e outros segmentos, utilizando de tecnologias que permitem evoluções constantes e a implementação de novos recursos.
        </p>
        <p class="side-text-right-text f-size-16">
          Você poderá gerenciar seu negócio através de um painel de controle que criamos exclusivamente para que você tenha autonomia em gerenciar todo conteúdo do site, de forma rápida e prática. Conheça as métricas de acessos ao seu site utilizando a integração com o Google Analytics.
        </p>
        <a class="plan-dedicated-order-button" href="https://wa.me/5516992747526" target="_Blank">Entre em Contato</a>
      </div>
      <div class="col-md-5 pad-tp-30 side-text-right-container side-text-plan-hosting text-left">
        <h3 class="side-text-right-title f-size-20">Constante evolução</h3>
        <ul class="web-hosting-options">
          <li><i class="fas fa-database"></i> Painel Administrativo</li>
          <li><i class="fas fa-magic"></i> Visual Personalizado</li>
          <li><i class="fas fa-window-restore"></i> Layout Responsivo</li>
          <li><i class="fas fa-signal"></i> Otimização de Sites</li>
          <li><i class="far fa-clock"></i> Performance & Suporte</li>
        </ul>
        <img src="{{ asset('galerias/pages/criacao-site.webp') }}" class="img-fluid col-8 mt-4" alt="Criação de Sites">
      </div>
    </div>
  </div>
</section>

@endsection


@section('jsPage')
@endsection
@section('cssPage')
@endsection