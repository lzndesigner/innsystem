<section class="padding-70-0 d-none">
    <div class="container">
        <div class="row justify-content-center first-pricing-table-container">
            <div class="col-md-5">
                <p class="side-text-right-text f-size25 text-center"><b>Vamos começar sua Loja Virtual?</b></p>

                <div id="monthly-yearly-chenge" class="mr-tp-20 custom-change text-center">
                    <a class="active monthly-price f-size12"> <span class="change-box-text">Plano Mensal</span> <span class="change-box"></span></a>
                    <a class="yearli-price f-size12"> <span class="change-box-text">Plano Anual (-15% OFF)</span></a>
                </div>
            </div>
        </div>
        <div class="row justify-content-center first-pricing-table-container mr-tp-10">
            <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                <div class="first-pricing-table">
                    <div class="content">
                        <i class="fas fa-cart-plus first-pricing-table-icon"></i>
                        <h5 class="first-pricing-table-title">Starting <span>para você que está <br> começando no
                                <b>mundo digital</b>!</span></h5>
                        <span style="text-align:center; margin-top:40px;" class="plan-price second-pricing-table-price monthly">
                            <i class="monthly">R$ 100 <span style="opacity: 0.5;font-size: 14px;">/mês</span></i>
                            <i class="yearly">R$ 1020 <span style="opacity: 0.5;font-size: 14px;">/ano</span> <br> <span style="opacity: 0.5;font-size: 14px;">Economize R$ 180,00</span></i>
                        </span>

                        <ul class="first-pricing-table-body">
                            <li><b>Ativação Imediata</b></li>
                            <li>Visitas, Produtos & Pedidos <br> <b>SEM LIMITE</b></li>
                            <li>Treinamento em Vídeo <small>(<a href="https://www.youtube.com/playlist?list=PLCHp7y2B24qmh4YqHbDPr7NKNvd--f7pl" target="Blank">disponível no Youtube</a>)</small></a></li>
                            <li>Tema Exclusivo e Responsivo</li>
                            <li>Depósito, Pix, PagSeguro e PayPal</li>
                            <li>Melhor Envio e Frenet</li>
                            <li>Cupons de Desconto</li>
                            <li>Relatório de Vendas</li>
                            <li>Suporte Prioritário WhatsApp</li>
                        </ul>
                        <a class="first-pricing-table-order" href="{{ url('https://wa.me/5516992747526?text=Ol%C3%A1+%F0%9F%91%8B+tenho+interesse+em+ter+o+meu+site') }}" target="_Blank"><i class="fa fa-whatsapp"></i>
                            Quero Começar já!</a>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>








<section class="padding-40-0">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
            <h1 class="side-text-right-title text-center">Todos os recursos</h1>
                <div class="table-responsive">
                    <table id="prices" class="table table-striped">
                        <thead id="theadHide">
                            <tr class="price-dsc" id="toDisapear">
                                <th class="">

                                </th>
                                <th class="checklist left">
                                    <p>
                                        recursos do plano
                                        <span class="soft-type oceanlight"><i class="fa fa-cart-plus"></i>
                                            Starting</span>
                                    </p>
                                </th>
                                <th class="checklist left">
                                    <p>
                                        recursos do plano
                                        <span class="soft-type oceandark"><i class="fa fa-rocket"></i> Rocket</span>
                                    </p>
                                </th>
                            </tr>
                        </thead>
                        <tbody id="features-body">
                             <tr class="feature">
                                <td class="feature">
                                    <span class="name">Visitas na Loja</span>
                                </td>
                                <td>
                                    <span>Ilimitadas</span>
                                </td>
                                <td>
                                    <span>Ilimitadas</span>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Produtos Cadastrados</span>
                                </td>
                                <td>
                                    <span>Ilimitadas</span>
                                </td>
                                <td>
                                    <span>Ilimitadas</span>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Pedidos Realizados</span>
                                </td>
                                <td>
                                    <span>Ilimitadas</span>
                                </td>
                                <td>
                                    <span>Ilimitadas</span>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Marketplaces (TinyERP)</span>
                                </td>
                                <td>
                                    <i class="fa fa-remove text-danger"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Atacado & Varejo</span>
                                </td>
                                <td>
                                    <i class="fa fa-remove text-danger"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Programa de Afiliados</span>
                                </td>
                                <td>
                                    <i class="fa fa-remove text-danger"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Atendimento & Suporte</span>
                                </td>
                                <td>
                                    <span>E-mail + WhatsApp</span>
                                </td>
                                <td>
                                    <span>E-mail + WhatsApp</span>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Modelos de Visuais</span>
                                </td>
                                <td>
                                    <span>Personalização Exclusiva</span>
                                </td>
                                <td>
                                    <span>Personalização Exclusiva</span>
                                </td>
                            </tr>
                            <tr class="feature active">
                                <td class="feature" colspan="3"><i class="fa fa-credit-card"></i> Formas de Pagamento
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Depósito/Transferência e PIX</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">PayPal (Cartões)</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">PagSeguro (Cartões e Boleto)</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Pagamento na Entrega/Local</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Moip (Cartões e Boleto)</span>
                                </td>
                                <td>
                                    <i class="fa fa-remove text-danger"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Ame Digital (Cartões e Boleto)</span>
                                </td>
                                <td>
                                    <i class="fa fa-remove text-danger"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>

                            <tr class="feature active">
                                <td class="feature" colspan="3"><i class="fa fa-truck"></i> Formas de Envios</td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Frete Grátis com Valor Mínimo</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Melhor Envio (Correios e Transportadoras)</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Frenet (Correios e Transportadoras)</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Retirar na Loja</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>

                            <tr class="feature active">
                                <td class="feature" colspan="3"><i class="fa fa-star"></i> Recursos Padrões</td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Funcionamento 24 horas por dia</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Instalação & Configuração</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Treinamento em Vídeo (passo-a-passo)</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Certificado de Segurança Digital (SSL)</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Hospedagem para seu Domínio</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">5 Contas de E-mail @seudominio</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Layout Responsivo e Customizavel</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Integração com o Google Analytics</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Otimização de Site S.E.O</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Backup & Manutenção</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Simulação de Frete com os Correios</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Produtos com Opções de Variações</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Produtos com Marcas e Comentários</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Imagens de Produtos com Zoom</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Pedidos com Valor Mínimo</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Cupons de Descontos para Clientes</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Informativo aos Clientes (por e-mail)</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Páginas de Informações e Institucionais</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Relatórios de Vendas, Produtos e Clientes</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Contrato de Fidelidade</span>
                                </td>
                                <td>
                                    <i class="fa fa-remove text-danger"></i>
                                </td>
                                <td>
                                    <i class="fa fa-remove text-danger"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Taxas de Comissão (%)</span>
                                </td>
                                <td>
                                    <i class="fa fa-remove text-danger"></i>
                                </td>
                                <td>
                                    <i class="fa fa-remove text-danger"></i>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- row -->
    </div><!-- container -->
</section>