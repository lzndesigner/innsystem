@extends('frontend.base')
@section('title', 'Criamos toda identidade visual para sua empresa')

@section('content')

<section class="padding-70-0">
    <div class="container">
        <div class="row justify-content-between mr-tp-50">
            <div class="col-md-7 side-text-right-container">
                <h2 class="side-text-right-title">Design Gráfico</h2>
                <p class="side-text-right-text">
                    A presença de uma empresa no <b>mundo digital</b> se tornou essencial, com as redes sociais ficou
                    mais fácil levar seus <u>produtos e serviços</u> até seus clientes. Com uma <b>identidade visual</b>
                    sólida e bem trabalhada aumenta significativamente a presença de sua empresa na internet.
                    <br>
                    <br>
                    Planejamos e criamos toda a identidade visual de sua empresa, como todo o <b>material
                        institucional</b> para o dia a dia, os mais utilizados são os cartões visita, panfletos,
                    adesivos, placas, assinatura digital para e-mails, crachás e entre outros.
                </p>
                <p class="side-text-right-text"><b>Pronto para planejar a identidade visual da sua Empresa?</b></p>
                <a class="plan-dedicated-order-button" href="https://wa.me/5516992747526">Vamos
                    conversar um pouco no WhatsApp, entre em contato</a>
            </div>
            <div class="col-md-5 side-text-right-container side-text-plan-hosting mr-tp-20">
                <h5 class="side-text-right-title f-size15">Quais os serviços que fazemos?</h5>

                <ul class="web-hosting-options">
                    <li><i class="fa fa-check"></i> Banner Digital para Web e Redes Sociais</li>
                    <li><i class="fa fa-check"></i> Cartões Visita, Presente e Vale-Cupom</li>
                    <li><i class="fa fa-check"></i> Catálogo de Produtos e Serviços</li>
                    <li><i class="fa fa-check"></i> Cardápio para Restaurantes e Pizzarias</li>
                    <li><i class="fa fa-check"></i> Flyer para Eventos</li>
                    <li><i class="fa fa-check"></i> Folhetos e Panfletos</li>
                    <li><i class="fa fa-check"></i> Identidade Visual (Logo e Manual)</li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="padding-20-0-100">
    <div class="container">
        <div class="row justify-content-center side-text-plan-hosting mr-tp-20">
            <h2 class="side-text-right-title f-size20 mr-0">Nossos Casos de Sucesso</h2>
            <div class="col-sm-12">
                <div class="grid grid-designgrafico galleryPrettyNotAnimated">

                    <div class="grid-item col-sm-6 col-md-3 col-lg-4 p-3">
                        <a href="{{ asset('galerias/portfolio/design/mimosenxovais.webp?1') }}" rel="prettyPhoto[design]">
                            <img src="{{ asset('galerias/portfolio/design/mimosenxovais.webp?1') }}" alt="Mimos Enxovais" class="img-fluid" />
                        </a>
                        <div class="description">
                            <h2>Mimos Enxovais</h2>
                            <p>Banner Digital</p>
                        </div>
                    </div><!-- grid-item col-sm-6 col-md-3 col-lg-4 p-3 -->

                    <div class="grid-item col-sm-6 col-md-3 col-lg-4 p-3">
                        <a href="{{ asset('galerias/portfolio/design/banner_startoutlet.webp?1') }}" rel="prettyPhoto[design]">
                            <img src="{{ asset('galerias/portfolio/design/banner_startoutlet.webp?1') }}" alt="Start Outlet" class="img-fluid" />
                        </a>
                        <div class="description">
                            <h2>Start Outlet</h2>
                            <p>Banner Digital</p>
                        </div>
                    </div><!-- grid-item col-sm-6 col-md-3 col-lg-4 p-3 -->

                    <div class="grid-item col-sm-6 col-md-3 col-lg-4 p-3">
                        <a href="{{ asset('galerias/portfolio/design/mu_banner_mucity.webp?1') }}" rel="prettyPhoto[design]">
                            <img src="{{ asset('galerias/portfolio/design/mu_banner_mucity.webp?1') }}" alt="Mu City" class="img-fluid" />
                        </a>
                        <div class="description">
                            <h2>Mu City</h2>
                            <p>Banner Digital</p>
                        </div>
                    </div><!-- grid-item col-sm-6 col-md-3 col-lg-4 p-3 -->

                    <div class="grid-item col-sm-6 col-md-3 col-lg-4 p-3">
                        <a href="{{ asset('galerias/portfolio/design/panfleto_tocadolanche.webp?1') }}" rel="prettyPhoto[design]">
                            <img src="{{ asset('galerias/portfolio/design/panfleto_tocadolanche.webp?1') }}" alt="Toca do Lanche" class="img-fluid" />
                        </a>
                        <div class="description">
                            <h2>Toca do Lanche</h2>
                            <p>Panfleto/Cardapio</p>
                        </div>
                    </div><!-- grid-item col-sm-6 col-md-3 col-lg-4 p-3 -->

                    <div class="grid-item col-sm-6 col-md-3 col-lg-4 p-3">
                        <a href="{{ asset('galerias/portfolio/design/tainara_almeida.webp?1') }}" rel="prettyPhoto[design]">
                            <img src="{{ asset('galerias/portfolio/design/tainara_almeida.webp?1') }}" alt="Tainara Almeida" class="img-fluid" />
                        </a>
                        <div class="description">
                            <h2>Tainara Almeida</h2>
                            <p>Logo</p>
                        </div>
                    </div><!-- grid-item col-sm-6 col-md-3 col-lg-4 p-3 -->

                    <div class="grid-item col-sm-6 col-md-3 col-lg-4 p-3">
                        <a href="{{ asset('galerias/portfolio/design/logo_laluna.webp?1') }}" rel="prettyPhoto[design]">
                            <img src="{{ asset('galerias/portfolio/design/logo_laluna.webp?1') }}" alt="Laluna" class="img-fluid" />
                        </a>
                        <div class="description">
                            <h2>Laluna</h2>
                            <p>Logo</p>
                        </div>
                    </div><!-- grid-item col-sm-6 col-md-3 col-lg-4 p-3 -->

                    <div class="grid-item col-sm-6 col-md-3 col-lg-4 p-3">
                        <a href="{{ asset('galerias/portfolio/design/final_campeonato.webp?1') }}" rel="prettyPhoto[design]">
                            <img src="{{ asset('galerias/portfolio/design/final_campeonato.webp?1') }}" alt="Final de Campeonato" class="img-fluid" />
                        </a>
                        <div class="description">
                            <h2>Final de Campeonato</h2>
                            <p>Banner Digital</p>
                        </div>
                    </div><!-- grid-item col-sm-6 col-md-3 col-lg-4 p-3 -->

                    <div class="grid-item col-sm-6 col-md-3 col-lg-4 p-3">
                        <a href="{{ asset('galerias/portfolio/design/classificacao_jogos.webp?1') }}" rel="prettyPhoto[design]">
                            <img src="{{ asset('galerias/portfolio/design/classificacao_jogos.webp?1') }}" alt="Final de Campeonato" class="img-fluid" />
                        </a>
                        <div class="description">
                            <h2>Classificação de Campeonato</h2>
                            <p>Banner Digital</p>
                        </div>
                    </div><!-- grid-item col-sm-6 col-md-3 col-lg-4 p-3 -->

                    <div class="grid-item col-sm-6 col-md-3 col-lg-4 p-3">
                        <a href="{{ asset('galerias/portfolio/design/logo_mrbolsas2.webp?1') }}" rel="prettyPhoto[design]">
                            <img src="{{ asset('galerias/portfolio/design/logo_mrbolsas2.webp?1') }}" alt="Logo MR Bolsas" class="img-fluid" />
                        </a>
                        <div class="description">
                            <h2>Logo MR Bolsas</h2>
                            <p>Logo</p>
                        </div>
                    </div><!-- grid-item col-sm-6 col-md-3 col-lg-4 p-3 -->

                    <!--
                    <div class="grid-item col-sm-6 col-md-3 col-lg-4 p-3">
                        <a href="{{ asset('galerias/portfolio/design/riomix.webp?1') }}" rel="prettyPhoto[design]">
                            <img src="{{ asset('galerias/portfolio/design/riomix.webp?1') }}" alt="Bar Rio Mix" class="img-fluid" />
                        </a>
                        <div class="description">
                            <h2>Bar Rio Mix</h2>
                            <p>Banner Digital</p>
                        </div>
                    </div><!-- grid-item col-sm-6 col-md-3 col-lg-4 p-3 -->

                </div>
            </div>
        </div>

        <div class="row justify-content-center mr-tp-50">
            <div class="col-8 text-center">
                <p class="side-text-right-text"><b>Vamos começar seu Projeto?</b></p>
                <a class="plan-dedicated-order-button btn-lg" href="https://wa.me/5516992747526">Entre em Contato</a>
            </div>
        </div>
    </div>
</section>
@endsection

@section('cssPage')
@endsection
@section('jsPage')
<script src="{{ asset('/general/js/isotope.pkgd.min.js') }}"></script>
<script>
    $(window).load(function () {
        $('.grid').isotope({
            itemSelector: '.grid-item',
            percentPosition: true,
            masonry: {
                // use outer width of grid-sizer for columnWidth
                columnWidth: 0
            }
        })
    });
</script>
@endsection