@extends('frontend.base')
@section('title', 'Design Games')

@section('content')


<section class="padding-70-0">
    <div class="container">
        <div class="row justify-content-between mr-tp-50">
            <div class="col-xs-12 col-md-12 col-lg-7 side-text-right-container">
                <h2 class="side-text-right-title">Tudo para Jogadores <br> e Servidores de Games!</h2>
                <p class="side-text-right-text">
                    Trabalhamos na criação exclusiva de tudo que os jogadores e equipes precisam para construir suas carreiras e história no mundo dos games, como, Mu Online, Call of Duty, Free Fire, e outros.
                </p>

                <div class="row justify-content-between">
                    <div class="col-xs-12 col-md col-lg-4">
                        <div class="render_sm"></div>

                    </div>
                    <div class="col-xs-12 col-md col-lg-8">
                        <div class="mt-0 mt-md-5 ml-5 ml-md-0">
                            <p><b>Principais Serviços:</b></p>
                            <ul class="web-hosting-options">
                                <li><i class="fa fa-check text-success"></i> Sites para Equipes</li>
                                <li><i class="fa fa-check text-success"></i> Design para Redes Sociais</li>
                                <li><i class="fa fa-check text-success"></i> Criação de Logo, Mascote</li>
                                <li><i class="fa fa-check text-success"></i> Design de Produtos (Camisas, Bonés, etc.)</li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-xs-12 col-md-12 col-lg-5 side-text-right-image text-center d-flex mx-auto flex-column">
                <img src="{{ asset('/galerias/pages/icons/template_mu.webp?1') }}" class="img-fluid" alt="Tudo para seu Servidor">
            </div>
        </div>
    </div><!-- container -->
</section>


<section class="padding-70-0-0">
    <div class="container">
        <div class="row justify-content-between m-0 p-0 mr-tp-50">
            <div class="col-xs-12 col-md-12 col-lg-12 side-text-right-container">
                <h1 class="side-text-right-title text-center">Construímos toda sua <br> Identidade Visual</h1>
                <div class="groups-icons fullwidth mt-5">
                    <ul class="">
                        <li class="text-center text-lg-left d-flex flex-column flex-sm-column flex-md-column flex-lg-row mb-5 mb-lg-0">
                            <span class="icon mt-2 mr-3"><img src="{{ asset('/galerias/pages/icons/logo_mascotes.webp') }}" alt="Logo & Mascotes" class="img-fluid"></span>
                            <div class="content pt-5">
                                <h2>Logo & Mascotes</h2>
                                <p class="side-text-right-text">
                                    Crie sua história com um logo exclusivo com suas preferencias, <br>
                                    seu estilo, seu personagem, suas cores e tipografia!
                                </p>
                                <p class="side-text-right-text">
                                    Durante cada etapa da criação apresentamos o andamento, <br>
                                    para a aprovação garantindo a melhor satisfação do cliente.
                                </p>
                            </div>
                        </li>

                        <li class="text-center text-lg-right d-flex justify-content-end flex-column flex-sm-column flex-md-column flex-lg-row mb-5 mb-lg-0">
                            <div class="content pt-5 order-last order-lg-first">
                                <h2>Entradas e Web Sites</h2>
                                <p class="side-text-right-text">
                                    Criamos todos os tipos de sites para os mais diversos games, <br>
                                    seja para apresentação de equipes, convocação de membros <br>
                                    e também funcionalidade para vendas de produtos, <br>
                                    calendário para campeonatos e outras funcionalidades.
                                </p>
                            </div>
                            <span class="icon mt-2 ml-3 order-first order-lg-last"><img src="{{ asset('/galerias/pages/icons/monitor_entrada.webp') }}" alt="Entradas e Web Sites" class="img-fluid"></span>
                        </li>

                        <li class="text-center text-lg-left d-flex flex-column flex-sm-column flex-md-column flex-lg-row mb-5 mb-lg-0">
                            <span class="icon mt-2 mr-3"><img src="{{ asset('/galerias/pages/icons/criativo_midiasociais.webp') }}" alt="Logo & Mascotes" class="img-fluid"></span>
                            <div class="content pt-5">
                                <h2>Criativos para Mídias Sociais</h2>
                                <p class="side-text-right-text">
                                    Se posicione nas redes sociais com criativos que impactam seu <br>
                                    público, trazendo mais audiência e integração com seus seguidores.
                                </p>
                                <p class="side-text-right-text">
                                    Utilize dos banners para divulgar horário das Lives ou campeonatos, <br>
                                    recrutamento para o clã e muito mais...
                                </p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="padding-40-0">
    <div class="container">
        <div class="row justify-content-center mr-tp-50">
            <div class="col-8 text-center">
                <h1 class="side-text-right-title text-center">É fantástico. <br> Vamos fazer tudo isso com você!</h1>
                <p class="side-text-right-text">
                    Um de nosso maiores objetivos é te ajudar a crescer. Isso significa que temos um time que está trabalhando exclusivamente para trazer a você informações relevantes, dicas e atualidades do mundo do e-commerce.
                </p>

                <p class=""><a href="https://wa.me/5516992747526" target="_Blank" class="btn btn-color-whatsapp"><i class="fa fa-whatsapp"></i> Fale com um especialista</a></p>
            </div>
        </div>
    </div>
</section>

<section class="section-wth-amwaj">
    <div class="bg_overlay_section-amwaj">
        <img src="{{ asset('/frontend/img/bg/b_bg_02.jpg') }}" alt="img-bg">
    </div>

    <div class="container-fullwidth">
        <div class="row justify-content-between mr-tp-50">

            <div class="col-md-12 side-text-right-container">
                <h1 class="side-text-right-title text-center">Conheça nossos <br> Casos de Sucesso</h1>

                <div class="grid grid-designgrafico galleryPrettyNotAnimated">
                    <div class="grid-item col-sm-6 col-md-4 col-lg-3 p-3 mb-3">
                        <a href="{{ asset('galerias/portfolio/games/banner_musvg.webp') }}" rel="prettyPhoto[design]">
                            <img src="{{ asset('galerias/portfolio/games/banner_musvg.webp') }}" alt="Mu Seven Games" class="img-fluid" />
                        </a>
                        <div class="description">
                            <h2>Mu Seven Games</h2>
                            <p>Criativo Mu Online</p>
                        </div>
                    </div><!-- grid-item col-sm-6 col-md-4 col-lg-3 p-3 mb-3 -->
                    <div class="grid-item col-sm-6 col-md-4 col-lg-3 p-3 mb-3">
                        <a href="{{ asset('galerias/portfolio/games/banner_musvg2.webp') }}" rel="prettyPhoto[design]">
                            <img src="{{ asset('galerias/portfolio/games/banner_musvg2.webp') }}" alt="Mu Seven Games" class="img-fluid" />
                        </a>
                        <div class="description">
                            <h2>Mu Seven Games</h2>
                            <p>Criativo Mu Online</p>
                        </div>
                    </div><!-- grid-item col-sm-6 col-md-4 col-lg-3 p-3 mb-3 -->

                    <div class="grid-item col-sm-6 col-md-4 col-lg-3 p-3 mb-3">
                        <a href="{{ asset('galerias/portfolio/ilustracao/leonidas_cod.webp') }}" rel="prettyPhoto[design]">
                            <img src="{{ asset('galerias/portfolio/ilustracao/leonidas_cod.webp') }}" alt="Leonidas COD Mobile" class="img-fluid" />
                        </a>
                        <div class="description">
                            <h2>Leonidas COD Mobile</h2>
                            <p>Logo e-Sport</p>
                        </div>
                    </div><!-- grid-item col-sm-6 col-md-4 col-lg-3 p-3 mb-3 -->

                    <div class="grid-item col-sm-6 col-md-4 col-lg-3 p-3 mb-3">
                        <a href="{{ asset('galerias/portfolio/ilustracao/meninos_pesca.webp') }}" rel="prettyPhoto[design]">
                            <img src="{{ asset('galerias/portfolio/ilustracao/meninos_pesca.webp') }}" alt="Meninos da Pesca" class="img-fluid" />
                        </a>
                        <div class="description">
                            <h2>Meninos da Pesca</h2>
                            <p>Logo e-Sport</p>
                        </div>
                    </div><!-- grid-item col-sm-6 col-md-4 col-lg-3 p-3 mb-3 -->

                    <div class="grid-item col-sm-6 col-md-4 col-lg-3 p-3 mb-3">
                        <a href="{{ asset('galerias/portfolio/design/mu_banner_mucity.webp') }}" rel="prettyPhoto[design]">
                            <img src="{{ asset('galerias/portfolio/design/mu_banner_mucity.webp') }}" alt="Mu City" class="img-fluid" />
                        </a>
                        <div class="description">
                            <h2>Mu City</h2>
                            <p>Criativo Mu Online</p>
                        </div>
                    </div><!-- grid-item col-sm-6 col-md-4 col-lg-3 p-3 mb-3 -->

                    <div class="grid-item col-sm-6 col-md-4 col-lg-3 p-3 mb-3">
                        <a href="{{ asset('galerias/portfolio/design/final_campeonato.webp') }}" rel="prettyPhoto[design]">
                            <img src="{{ asset('galerias/portfolio/design/final_campeonato.webp') }}" alt="Campeonato de Futebol" class="img-fluid" />
                        </a>
                        <div class="description">
                            <h2>Campeonato de Futebol</h2>
                            <p>Criativo Esportes</p>
                        </div>
                    </div><!-- grid-item col-sm-6 col-md-4 col-lg-3 p-3 mb-3 -->

                    <div class="grid-item col-sm-6 col-md-4 col-lg-3 p-3 mb-3">
                        <a href="{{ asset('galerias/portfolio/games/entradamu_muwalker.webp') }}" rel="prettyPhoto[design]">
                            <img src="{{ asset('galerias/portfolio/games/entradamu_muwalker.webp') }}" alt="Mu Walker - Natal" class="img-fluid" />
                        </a>
                        <div class="description">
                            <h2>Mu Walker - Natal</h2>
                            <p>Entrada Website</p>
                        </div>
                    </div><!-- grid-item col-sm-6 col-md-4 col-lg-3 p-3 mb-3 -->

                    <div class="grid-item col-sm-6 col-md-4 col-lg-3 p-3 mb-3">
                        <a href="{{ asset('galerias/portfolio/games/logo_muozfire.webp') }}" rel="prettyPhoto[design]">
                            <img src="{{ asset('galerias/portfolio/games/logo_muozfire.webp') }}" alt="Mu Oz" class="img-fluid" />
                        </a>
                        <div class="description">
                            <h2>Mu Oz</h2>
                            <p>Logo Mu Online</p>
                        </div>
                    </div><!-- grid-item col-sm-6 col-md-4 col-lg-3 p-3 mb-3 -->

                    <div class="grid-item col-sm-6 col-md-4 col-lg-3 p-3 mb-3">
                        <a href="{{ asset('galerias/portfolio/games/logo_warriors_2.webp') }}" rel="prettyPhoto[design]">
                            <img src="{{ asset('galerias/portfolio/games/logo_warriors_2.webp') }}" alt="Warriors Mu" class="img-fluid" />
                        </a>
                        <div class="description">
                            <h2>Warriors Mu</h2>
                            <p>Logo Mu Online</p>
                        </div>
                    </div><!-- grid-item col-sm-6 col-md-4 col-lg-3 p-3 mb-3 -->

                    <!--
                    <div class="grid-item col-sm-6 col-md-4 col-lg-3 p-3 mb-3">
                        <a href="{{ asset('galerias/portfolio/ilustracao/goku_animation.gif') }}" rel="prettyPhoto[design]">
                            <img src="{{ asset('galerias/portfolio/ilustracao/goku_animation.gif') }}" alt="Goku Desenho" class="img-fluid" />
                        </a>
                        <div class="description">
                            <h2>Goku</h2>
                            <p>Desenho e Ilustração</p>
                        </div>
                    </div><!-- grid-item col-sm-6 col-md-4 col-lg-3 p-3 mb-3 -->

                </div>
            </div>


        </div><!-- end row -->
    </div><!-- end container -->
</section>



@endsection

@section('cssPage')
@endsection
@section('jsPage')
<script src="{{ asset('/general/js/isotope.pkgd.min.js') }}"></script>
<script>
    $(window).load(function() {
        $('.grid').isotope({
            itemSelector: '.grid-item',
            percentPosition: true,
            masonry: {
                // use outer width of grid-sizer for columnWidth
                columnWidth: 0
            }
        })
    });
</script>
@endsection