@extends('frontend.base')
@section('title', 'Crie sua Loja Virtual e comece a vender')

@section('content')

<section class="padding-70-0-0">
    <div class="container">
        <div class="row justify-content-between mr-tp-50">
            <div class="col-xs-12 col-md-12 col-lg-7 side-text-right-container">
                <h2 class="side-text-right-title">Pronto para criarmos <br> sua Loja Virtual?</h2>
                <p class="side-text-right-text">
                    A venda pela internet tem aumentado nos últimos tempos, nossa proposta é uma solução completa para você vender seus produtos em um ambiente seguro e profissional.
                </p>
                <p class="side-text-right-text">
                    Nossa equipe cuida de toda parte do sistema, criação do layout da sua loja conforme deseja e você só precisa se preocupar em vender.
                </p>
                <p class="side-text-right-text">
                    <b>No nosso sistema não há:</b> Limites de acessos, pedidos ou produtos; Comissão ou taxas por pedidos;
                </p>
            </div>
            <div class="col-xs-12 col-md-12 col-lg-5 side-text-right-image text-center d-flex mx-auto flex-column">
                <img src="{{ asset('/galerias/portfolio/lojavirtual/mockup3.webp?1') }}" class="img-fluid" alt="Criação de Loja Virtual">
            </div>
        </div>
    </div><!-- container -->
</section>
<section class="padding-70-0-0">
    <div class="container-fullwidth">
        <div class="row justify-content-between m-0 p-0 mr-tp-50">
            <div class="col-xs-12 col-md-12 col-lg-12 side-text-right-container">
                <h1 class="side-text-right-title text-center">Veja como é fácil ter o <br> seu negócio na Internet!</h1>


                <div class="groups-icons mt-5">
                    <ul class="d-flex flex-column flex-sm-column flex-md-row align-items-center justify-center-content">
                        <li>
                            <span class="icon"><img src="{{ asset('/galerias/pages/icons/os-primeiros-passos.webp') }}" class="img-fluid" alt="Os primeiros passos"></span>
                            <h2>Os primeiros passos</h2>
                            <p class="side-text-right-text">Nossa equipe irá cuidar de toda parte de desenvolvimento e design exclusivamente para sua loja.</p>
                        </li>

                        <li>
                            <span class="icon"><img src="{{ asset('/galerias/pages/icons/adicione-produtos.webp') }}" class="img-fluid" alt="Adicione facilmente seus produtos"></span>
                            <h2>Adicione facilmente <br> seus produtos</h2>
                            <p class="side-text-right-text">Com nosso treinamento você vai aprender a gerenciar todo seu catálogo de produtos, pedidos, clientes e muito mais de uma maneira simples e rápida.</p>
                        </li>

                        <li>
                            <span class="icon"><img src="{{ asset('/galerias/pages/icons/pagamentos.webp') }}" class="img-fluid" alt="Pagamento de Clientes"></span>
                            <h2>Pagamento de Clientes</h2>
                            <p class="side-text-right-text">Receba diretamente em sua conta os pagamentos dos clientes, os principaís são: Pagseguro, PayPal, PIX, Depósito ou Cartões.</p>
                        </li>
                    </ul>
                    <ul class="d-flex flex-column flex-sm-column flex-md-row align-items-center justify-center-content">
                        <li>
                            <span class="icon"><img src="{{ asset('/galerias/pages/icons/entregas.webp') }}" class="img-fluid" alt="Envie por Correios ou Transportadoras"></span>
                            <h2>Envie por Correios <br> ou Transportadoras</h2>
                            <p class="side-text-right-text">O valor do frete é calculado automaticamente na hora para o cliente e incluído no pedido, você só precisará preparar e despachar o pedido.</p>
                        </li>

                        <li>
                            <span class="icon"><img src="{{ asset('/galerias/pages/icons/whatsapp.webp') }}" class="img-fluid" alt="Suporte Exclusivo pelo WhatsApp"></span>
                            <h2>Suporte Exclusivo <br> pelo WhatsApp</h2>
                            <p class="side-text-right-text">Nossa equipe está de prontidão para atender você com um suporte rápido e eficiente garantindo a melhor qualidade e segurança.</p>
                        </li>

                        <li>
                            <span class="icon"><img src="{{ asset('/galerias/pages/icons/world.webp') }}" class="img-fluid" alt="Pronto! Seja um sucesso no Mundo Digital"></span>
                            <h2>Pronto! <br> Seja um sucesso <br> no Mundo Digital</h2>
                            <p class="side-text-right-text">Sua loja virtual tem todas as funções necessárias para você ter sucesso nas vendas online e alavancar seu negócio.</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>



<section class="padding-40-0">
    <div class="container">
        <div class="row justify-content-center mr-tp-50">
            <div class="col-8 text-center">
                <h1 class="side-text-right-title text-center">É fantástico. <br> Vamos fazer tudo isso com você!</h1>
                <p class="side-text-right-text">
                    Um de nosso maiores objetivos é te ajudar a crescer. Isso significa que temos um time que está trabalhando exclusivamente para trazer a você informações relevantes, dicas e atualidades do mundo do e-commerce.
                </p>

                <p class=""><a href="https://wa.me/5516992747526?text=Ol%C3%A1+%F0%9F%91%8B+tenho+interesse+em+ter+o+meu+site" target="_Blank" class="btn btn-color-whatsapp"><i class="fa fa-whatsapp"></i> Fale com um especialista</a></p>
            </div>
        </div>
    </div>
</section>

<section class="section-wth-amwaj">
    <div class="bg_overlay_section-amwaj">
        <img src="{{ asset('/frontend/img/bg/b_bg_02.jpg') }}" class="img-fluid" alt="img-bg">
    </div>

    <div class="container-fullwidth">
        <div class="row justify-content-between mr-tp-50">

            <div class="col-md-7 side-text-right-container">
                <h2 class="side-text-right-title">Sua Loja fica incrível e preparada para qualquer dispositivo móvel</h2>
                <div class="groups-icons fullwidth mt-5">
                    <ul class="">
                        <li class="text-left d-flex flex-column flex-sm-column flex-md-row flex-lg-row">
                            <span class="icon mt-2 mr-3"><img src="{{ asset('/galerias/pages/icons/ecommerce.webp') }}" class="img-fluid" alt="SISTEMA ROBUSTO E PROFISSIONAL"></span>
                            <div class="content">
                                <h2>SISTEMA ROBUSTO E PROFISSIONAL</h2>
                                <p class="side-text-right-text">Tema exclusivo e pronto para computadores e para todos os tipos de smartphone ou tablet. Utilizando o SEO otimizado sua loja será facilmente encontrada na internet.</p>
                            </div>
                        </li>
                        <li class="text-left d-flex flex-column flex-sm-column flex-md-row flex-lg-row">
                            <span class="icon mt-2 mr-3"><img src="{{ asset('/galerias/pages/icons/dominio.webp') }}" class="img-fluid" alt="DOMÍNIO PRÓPRIO E E-MAILS"></span>
                            <div class="content">
                                <h2>DOMÍNIO PRÓPRIO E E-MAILS</h2>
                                <p class="side-text-right-text">Construa uma identidade única, passe uma boa impressão aos seus clientes com um domínio próprio e emails personalizados.</p>
                            </div>
                        </li>
                        <li class="text-left d-flex flex-column flex-sm-column flex-md-row flex-lg-row">
                            <span class="icon mt-0 mr-3"><img src="{{ asset('/galerias/pages/icons/ssl-site-seguro.webp') }}" class="img-fluid" alt="CERTIFICADO DE SEGURANÇA DIGITAL (SSL)"></span>
                            <div class="content">
                                <h2>CERTIFICADO DE SEGURANÇA DIGITAL (SSL)</h2>
                                <p class="side-text-right-text">Garantindo total segurança aos seus clientes, utilizamos o Certificado SSL que criptografa todas as informações através do protocolo HTTPS que é exibido no navegador.</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-md-5">
                <img src="{{ asset('/galerias/pages/icons/responsive_layout.webp') }}" class="img-fluid mt-5" alt="Layout Responsivo">
            </div>


        </div><!-- end row -->
    </div><!-- end container -->
</section>

<section class="padding-70-0">
    <div class="container">
        <h1 class="side-text-right-title text-center">Os Principais Recursos</h1>
        <div class="row justify-content-between mr-tp-50 mr-bt-100">
            <div class="col-md-7 side-text-right-container">
                <p class="side-text-right-text"><b>MEIOS DE PAGAMENTOS</b></p>
                <p class="side-text-right-text">
                    Aceite o pagamento de seus clientes através dos principais meios de pagamentos, PagSeguro, PayPal, Mercado Pago, PIX ou Boleto Bancário.
                </p>
            </div>
            <div class="col-xs-12 col-md-12 col-lg-5 side-text-right-image text-center d-flex mx-auto flex-column">
                <img src="{{ asset('/galerias/pages/icons/meios_pagamentos.webp?1') }}" class="img-fluid" alt="Meios de Pagamentos">
            </div>
        </div><!-- row -->

        <div class="row justify-content-between mr-tp-50 mr-bt-100">
            <div class="col-xs-12 col-md-12 col-lg-5 side-text-right-image text-center d-flex mx-auto flex-column order-last order-md-first">
                <img src="{{ asset('/galerias/pages/icons/meios_envios.webp?1') }}" class="img-fluid" alt="Meios de Envio">
            </div>

            <div class="col-md-7 side-text-right-container order-first order-md-last">
                <p class="side-text-right-text"><b>ENTREGA E CÁLCULO DE FRETE</b></p>
                <p class="side-text-right-text">
                    Agora seu cliente consegue cálcular o custo de frete automaticamente de forma rápida e prática. Envie seus pedidos por Correios ou Transportadoras.
                </p>
            </div>
        </div><!-- row -->

        <div class="row justify-content-between mr-tp-50">
            <div class="col-md-7 side-text-right-container">
                <p class="side-text-right-text"><b>INTEGRAÇÕES ERP’s</b></p>
                <p class="side-text-right-text">
                    Emita nota fiscal de forma rápida e automatizada, leve seus produtos para os Marketplaces e diversos outros recursos disponíveis nas empresas TinyERP ou BlingERP.
                </p>
            </div>
            <div class="col-xs-12 col-md-12 col-lg-5 side-text-right-image text-center d-flex mx-auto flex-column">
                <img src="{{ asset('/galerias/pages/icons/integracao-erp.webp?1') }}" class="img-fluid" alt="Integração ERP">
            </div>
        </div><!-- row -->

    </div><!-- container -->
</section>


<section class="padding-70-0">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-md-12 side-text-right-container">
                <h1 class="side-text-right-title text-center">Venda muito mais com <br> os Marketplaces</h1>
            </div>
            <div class="col-xs-9 col-md-9 col-lg-9 side-text-right-image text-center d-flex mx-auto flex-column">
                <p class="side-text-right-text my-4">Funcionam como um shopping online, onde as empresas podem anunciar seus produtos em lojas conhecidas e abrir novos canais de venda com maior visibilidade.</p>
                <img src="{{ asset('/galerias/pages/icons/marketplaces.webp?1') }}" alt="Marketplaces" class="img-fluid mt-5">
            </div>
        </div><!-- row -->

    </div>
</section>

<section class="padding-40-0">
    <div class="container">
        <div class="row justify-content-center mr-tp-50">
            <div class="col-8 text-center">
                <p class=""><a href="https://wa.me/5516992747526?text=Ol%C3%A1+%F0%9F%91%8B+tenho+interesse+em+ter+o+meu+site" target="_Blank" class="btn btn-color-whatsapp"><i class="fa fa-whatsapp"></i> Fale com um especialista</a></p>
            </div>
        </div>
    </div>
</section>
@endsection


@section('jsPage')
<script>
    // hosting plans with chekout price yearly and monthly //
    $('#monthly-yearly-chenge a').on('click', function() {
        $(this).addClass('active').siblings().removeClass('active');

        if (jQuery('.yearli-price').hasClass('active')) {
            $('.second-pricing-table-price').addClass('yearly');
            $('.second-pricing-table-price').removeClass('monthly');

            $('.price-period-monthly').addClass('d-none');
            $('.price-period-yearly').removeClass('d-none');
        }

        if (jQuery('.monthly-price').hasClass('active')) {
            $('.second-pricing-table-price').addClass('monthly');
            $('.second-pricing-table-price').removeClass('yearly');

            $('.price-period-yearly').addClass('d-none');
            $('.price-period-monthly').removeClass('d-none');
        }
    });
</script>
@endsection
@section('cssPage')
@endsection