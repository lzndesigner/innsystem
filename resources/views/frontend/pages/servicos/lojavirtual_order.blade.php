@extends('frontend.base')
@section('title', 'Resumo do Pedido - Loja Virtual')

@section('content')

    <section class="padding-40-0">
        <div class="container">
            <div class="row justify-content-start second-pricing-table-container mr-tp-30">
                <div class="col-md-4">
                    <div class="second-pricing-table style-2 ">
                        <h5 class="second-pricing-table-title"><span>Plano Escolhido</span> {{ $getService->name }}</h5>
                        <ul class="second-pricing-table-body">
                            {!! $getService->description !!}
                        </ul>
                        <div id="monthly-yearly-chenge" class="my-2 pb-3 custom-change text-center">
                            <a class="{{ $period == 'yearly' ? '' : 'active' }} monthly-price f-size12"> <span
                                    class="change-box-text">Mensal</span> <span class="change-box"></span></a>
                            <a class="{{ $period == 'yearly' ? 'active' : '' }} yearli-price f-size12"> <span
                                    class="change-box-text">Anual (-15%)</span></a>
                        </div>

                        <div class="second-pricing-table-title mb-2 text-center">
                            <span>valor do plano</span>
                        </div>
                        <div id="price_plan"
                            class="second-pricing-table-price my-2 text-center {{ $period == 'yearly' ? 'yearly' : 'monthly' }}">
                            <i class="yearly">R$
                                <span
                                    data-value="{{ $getService->price_anual }}">{{ number_format($getService->price_anual, 2) }}</span><small>/ano</small></i>
                            <i class="monthly">R$
                                <span
                                    data-value="{{ $getService->price }}">{{ number_format($getService->price, 2) }}</span><small>/mês</small></i>
                        </div>



                        <img src="/galerias/payment/tarja_pagamento.webp" alt="Formas de Pagamentos" class="img-fluid mt-5">
                        <img src="/galerias/payment/ssl-site-seguro.webp" alt="Certificado de Segurança Digital SSL"
                            class="col-8 mt-4">

                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card">
                        <div class="row justify-content-center">
                            <div class="col-sm-12 text-center p-0 mt-3 mb-2">
                                <h2 id="heading">Finalizar Pedido</h2>
                                <p>Escolha sua customizações <br> e preencha os campos para finalizar.</p>
                                <form id="request_order" method="POST" class="m-2">
                                    <input type="hidden" name="price_total" id="price_total"
                                        value="{{ $period == 'yearly' ? $getService->price_anual : $getService->price }}" />
                                    <input type="hidden" name="service_id" id="service_id"
                                        value="{{ $getService->id }}" />
                                    <input type="hidden" name="service_name" id="service_name"
                                        value="{{ $getService->name }}" />
                                    <input type="hidden" name="period" id="period"
                                        value="{{ $period == 'yearly' ? 'anual' : 'mensal' }}" />

                                    <input type="hidden" name="customer_id" id="customer_id"
                                        value="{{ isset($getCustomer->id) ? $getCustomer->id : '' }}" />

                                    <div class="row mb-5">
                                        <div class="col-sm-12">
                                            <!-- progressbar -->
                                            <ul id="progressbar">
                                                <li class="active" id="register"><strong>Faça seu Cadastro</strong></li>
                                                <li id="order"><strong>Finalize seu Pedido</strong></li>
                                            </ul>
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-striped progress-bar-animated"
                                                    role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <fieldset id="form-account">
                                        <div class="form-card">
                                            <div class="row">
                                                <div class="col-sm-7">
                                                    <h2 class="fs-title">Faça seu Cadastro:</h2>
                                                </div>
                                                <div class="col-sm-5">
                                                    <h2 class="steps">Etapa 1 - 2</h2>
                                                </div>
                                            </div>

                                            <div class="card contact-page-form-send mt-2 mb-3">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="field input-field">
                                                            <input class="form-contain-home-input" type="email" id="email"
                                                                name="email" placeholder="Seu e-mail válido"
                                                                value="{{ isset($getCustomer->email) ? $getCustomer->email : '' }}"
                                                                required="">

                                                            <i class="far fa-envelope" aria-hidden="true"></i>
                                                        </div>
                                                    </div><!-- col -->
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-7">
                                                        <div class="field input-field">
                                                            <input class="form-contain-home-input" type="text" id="name"
                                                                name="name" placeholder="Nome e sobrenome"
                                                                value="{{ isset($getCustomer->name) ? $getCustomer->name : '' }}"
                                                                required="">

                                                            <i class="far fa-user" aria-hidden="true"></i>
                                                        </div>
                                                    </div><!-- col -->
                                                    <div class="col-sm-5">
                                                        <div class="field input-field">
                                                            <input class="form-contain-home-input formatPhone" type="text"
                                                                id="phone" name="phone"
                                                                value="{{ isset($getCustomer->phone) ? $getCustomer->phone : '' }}"
                                                                placeholder="Telefone Celular" required="">

                                                            <i class="fa fa-whatsapp" aria-hidden="true"></i>
                                                        </div>
                                                    </div><!-- col -->
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-7">
                                                        <div class="field input-field">
                                                            <input class="form-contain-home-input" type="text" id="company"
                                                                name="company" placeholder="Nome da Empresa"
                                                                value="{{ isset($getCustomer->company) ? $getCustomer->company : '' }}"
                                                                required="">

                                                            <i class="fa fa-home" aria-hidden="true"></i>
                                                        </div>
                                                    </div><!-- col -->
                                                    <div class="col-sm-5">
                                                        <div class="field input-field">
                                                            <input class="form-contain-home-input" type="text" id="document"
                                                                name="document" placeholder="CPF/CNPJ"
                                                                value="{{ isset($getCustomer->document) ? $getCustomer->document : '' }}"
                                                                required="">

                                                            <i class="fa fa-book" aria-hidden="true"></i>
                                                        </div>
                                                    </div><!-- col -->
                                                </div>

                                                @if (!Auth::guard('customer')->check())
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="field input-field">
                                                                <input class="form-contain-home-input" type="password"
                                                                    id="password" name="password" placeholder="***********"
                                                                    required="">

                                                                <i class="fa fa-key" aria-hidden="true"></i>
                                                            </div>
                                                        </div><!-- col -->
                                                        <div class="col-sm-6">
                                                            <div class="field input-field">
                                                                <input class="form-contain-home-input" type="password"
                                                                    id="password_confirmation" name="password_confirmation"
                                                                    placeholder="***********" required="">

                                                                <i class="fa fa-check" aria-hidden="true"></i>
                                                            </div>
                                                        </div><!-- col -->
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <input type="button" name="next" class="next action-button" value="Continuar" />
                                    </fieldset><!-- created user -->

                                    <fieldset id="form-order">
                                        <div class="form-card">
                                            <div class="row">
                                                <div class="col-7">
                                                    <h2 class="fs-title">Finalize seu Pedido:</h2>
                                                </div>
                                                <div class="col-5">
                                                    <h2 class="steps">Etapa 2 - 2</h2>
                                                </div>
                                            </div>
                                            <div class="card contact-page-form-send mt-2 mb-3">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <span class="mb-4"><b>Informações da Loja:</b></span>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <div class="field input-field">
                                                            <input class="form-contain-home-input" type="text" id="dominio"
                                                                name="dominio" placeholder="Domínio registrado (www)"
                                                                required="">

                                                            <i class="fa fa-chrome" aria-hidden="true"></i>
                                                        </div>
                                                    </div><!-- col -->
                                                </div>

                                                <hr>

                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <span class="mb-4"><b>Formas de Pagamento:</b></span>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group mb-1 ml-2">
                                                            <label for="payment_pix">
                                                                <input type="radio" name="payment_method" id="payment_pix"
                                                                    value="pix">
                                                                <b>PIX - <small>Disponível 24 horas por dia</small> <span
                                                                        class="badge badge-success">Aprovação
                                                                        Imediata</span></b>
                                                            </label>
                                                        </div>
                                                        <div class="form-group mb-1 ml-2">
                                                            <label for="payment_boleto">
                                                                <input type="radio" name="payment_method"
                                                                    id="payment_boleto" value="boleto">
                                                                <b>Boleto Bancário <span class="badge badge-primary">1 dia
                                                                        útil</span></b>
                                                            </label>
                                                        </div>
                                                        <div class="form-group mb-1 ml-2">
                                                            <label for="payment_cartao">
                                                                <input type="radio" name="payment_method"
                                                                    id="payment_cartao" value="mercadopago">
                                                                <b>Cartão de Crédito <span
                                                                        class="badge badge-warning">Imediata/Aprovação</span></b>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card mt-2 mb-3">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <span class="mb-4"><b>Termos de Aceite & Contrato:</b></span>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="terms_description"
                                                            style="height:250px; padding:20px 15px; overflow-y:scroll;">
                                                            @include('frontend.includes.termos_lojavirtual')
                                                        </div>
                                                        <span class="p-3 f-size13 help-block">Você pode baixar o contrato em PDF <a href="{{ url('/contrato/loja_virtual.pdf')}}" download>clicando aqui</a>.</span>
                                                        <div class="form-group mt-3 mb-1 ml-2">
                                                            <label for="terms_confirmation">
                                                                <input type="checkbox" name="terms_confirmation"
                                                                    id="terms_confirmation" value="aceito">
                                                                <b>Eu declaro que li e aceito os <b>Termos de Aceite &
                                                                        Contrato</b> no ato dessa contratação.</b>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <button type="button" name="submit" class="submit action-button"
                                            value="">Aceito e Confirmar!</button>
                                        <input type="button" name="previous" class="previous action-button-previous"
                                            value="Voltar" />
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('cssPage')
    <link rel="stylesheet" href="{{ asset('/general/plugins/sweetalert/sweetalert2.min.css') }}">
@endsection

@section('jsPage')
    <script src="{{ asset('/general/plugins/sweetalert/sweetalert2.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            var current_fs, next_fs, previous_fs; //fieldsets
            var opacity;
            var current = 1;
            var steps = $("fieldset").length;

            setProgressBar(current);

            @if (!Auth::guard('customer')->check())
                $('#password_confirmation').blur(function() {
                if ($('#password').val() != $('#password_confirmation').val()) {
                Swal.fire({
                text: 'As senhas são diferentes!',
                icon: 'error',
                showClass: {
                popup: 'animate_animated animate_wobble'
                }
                });
                $('#password, #password_confirmation').parent().addClass("danger");
                } else {
                $('#password, #password_confirmation').parent().removeClass("danger");
                }
                });
            @else
                $(window).on('load', function() {
                $('input.next').trigger('click');
                });
            
            @endif

            $(".next").click(function() {

                var isFormValid = true;

                $("#form-account input").each(function(index, value) {
                    if ($.trim($(value).val()).length == 0) {
                        $(value).parent().addClass("danger");
                        Swal.fire({
                            text: 'Preencha os campos obrigatórios!',
                            icon: 'error',
                            showClass: {
                                popup: 'animate_animated animate_wobble'
                            }
                        });
                        isFormValid = false;
                    } else {
                        $(value).parent().removeClass("danger");
                    }
                });

                if (isFormValid) {

                    current_fs = $(this).parent();
                    next_fs = $(this).parent().next();

                    //Add Class Active
                    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

                    //show the next fieldset
                    next_fs.show();
                    //hide the current fieldset with style
                    current_fs.animate({
                        opacity: 0
                    }, {
                        step: function(now) {
                            // for making fielset appear animation
                            opacity = 1 - now;

                            current_fs.css({
                                'display': 'none',
                                'position': 'relative'
                            });
                            next_fs.css({
                                'opacity': opacity
                            });
                        },
                        duration: 500
                    });
                    setProgressBar(++current);
                }
            });

            $(".previous").click(function() {

                current_fs = $(this).parent();
                previous_fs = $(this).parent().prev();

                //Remove class active
                $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

                //show the previous fieldset
                previous_fs.show();

                //hide the current fieldset with style
                current_fs.animate({
                    opacity: 0
                }, {
                    step: function(now) {
                        // for making fielset appear animation
                        opacity = 1 - now;

                        current_fs.css({
                            'display': 'none',
                            'position': 'relative'
                        });
                        previous_fs.css({
                            'opacity': opacity
                        });
                    },
                    duration: 500
                });
                setProgressBar(--current);
            });

            function setProgressBar(curStep) {
                var percent = parseFloat(100 / steps) * curStep;
                percent = percent.toFixed();
                $(".progress-bar")
                    .css("width", percent + "%")
            }


            $(document).on('click', '.submit', function(e) {
                e.preventDefault();

                //disable the submit button
                $("form button.submit").attr("disabled", true);
                $('form button.submit').append('<i class="fa fa-spinner fa-spin ml-3"></i>');
                setTimeout(function() {
                    $('form button.submit').prop("disabled", false);
                    $('form button.submit').find('.fa-spinner').addClass('d-none');
                }, 8000);

                var validInputs = true;
                var input_payment = $("#form-order input[name=payment_method]");
                var input_terms = $("#form-order input[name=terms_confirmation]");

                if (!input_payment.is(':checked')) {
                    Swal.fire({
                        text: 'Selecione um meio de pagamento!',
                        icon: 'error',
                        showClass: {
                            popup: 'animate_animated animate_wobble'
                        }
                    });
                }

                if (!input_terms.is(':checked')) {
                    Swal.fire({
                        text: 'Aceite os Termos e Contrato!',
                        icon: 'error',
                        showClass: {
                            popup: 'animate_animated animate_wobble'
                        }
                    });
                }

                if (input_payment.is(':checked') && input_terms.is(':checked')) {

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': "{{ csrf_token() }}"
                        }
                    });

                    var url = "{{ route('frontend.lojavirtual.store') }}";
                    var method = 'POST';

                    var data = $('#request_order').serialize();
                    $.ajax({
                        url: url,
                        data: data,
                        method: method,
                        success: function(data) {
                            Swal.fire({
                                text: data,
                                icon: 'success',
                                showClass: {
                                    popup: 'animate_animated animate_backInUp'
                                },
                                onClose: () => {
                                    // Loading page listagem
                                    $('#request_order')[0].reset();
                                    location.href =
                                        "{{ url('/loja-virtual/contratar/sucesso') }}";
                                }
                            });
                        },
                        error: function(xhr) {
                            if (xhr.status === 422) {
                                Swal.fire({
                                    text: 'Validação: ' + xhr.responseJSON,
                                    icon: 'warning',
                                    showClass: {
                                        popup: 'animate_animated animate_wobble'
                                    }
                                });
                            } else {
                                Swal.fire({
                                    text: 'Erro interno, informe ao suporte: ' + xhr
                                        .responseJSON,
                                    icon: 'error',
                                    showClass: {
                                        popup: 'animate_animated animate_wobble'
                                    }
                                });
                            }
                        }
                    });

                }

            })

        });
    </script>

    <script>
        // Begin :: Format Phone
        formatedPhone();

        function upMaskPhone(event) {
            var $element = $('#' + this.id);
            $(this).off('blur');
            $element.unmask();
            if (this.value.replace(/\D/g, '').length > 10) {
                $element.mask("(00) 00000-0000");
            } else {
                $element.mask("(00) 0000-00009");
            }
            $(this).on('blur', upMaskPhone);
        }

        function formatedPhone() {
            $('.formatPhone').each(function(i, el) {
                $('.formatPhone').mask("(00) 00000-0000");
            });
            $('.formatPhone').on('blur', upMaskPhone);
        }
        // End :: Format Phone


        // hosting plans with chekout price yearly and monthly //
        $('#monthly-yearly-chenge a').on('click', function() {
            $(this).addClass('active').siblings().removeClass('active');

            if (jQuery('.monthly-price').hasClass('active')) {
                $('.second-pricing-table-price').addClass('monthly');
                $('.second-pricing-table-price').removeClass('yearly');
                var price = $('#price_plan').find('i.monthly span').attr('data-value');
                $('#price_total').val(price);
                $('#period').val('mensal');

            }

            if (jQuery('.yearli-price').hasClass('active')) {
                $('.second-pricing-table-price').addClass('yearly');
                $('.second-pricing-table-price').removeClass('monthly');
                var price = $('#price_plan').find('i.yearly span').attr('data-value');
                $('#price_total').val(price);
                $('#period').val('anual');

            }
        });
    </script>
@endsection
