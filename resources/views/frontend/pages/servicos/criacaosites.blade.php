@extends('frontend.base')
@section('title', 'Criação de Site, Loja Virtual e Design Gráfico')

@section('content')

<section class="padding-70-0-0">
    <div class="container">
        <div class="row justify-content-between mr-tp-50">
            <div class="col-md-6 side-text-right-container">
                <h2 class="side-text-right-title">Criação de Sites</h2>
                <p class="side-text-right-text">
                    Hoje em dia é essencial a presença das empresas na <b>internet</b>, facilitando que clientes
                    <u>encontrem</u> sua empresa com uma simples busca no <i>Google</i>. A criação de um site é a
                    <b>identidade digital</b> da empresa na internet, levando os clientes a consumir seus
                    <u>produtos</u> e <u>serviços</u> com mais facilidade e deixa uma experiência incrível.
                    <br>
                    <br>
                    Desenvolvemos <b>projetos exclusivos</b> oferecendo a melhor experiência em usabilidade e a
                    apresentação da sua <b>empresa</b>, com os recursos essenciais e <u>layout responsivo</u> para os
                    dispositivos <i>celulares, tablets, notebooks e computadores</i>.
                </p>
            </div>
            <div class="col-md-6 side-text-right-image text-center d-flex mx-auto flex-column">
                <div class="mb-auto"></div>
                <img src="{{ asset('/galerias/pages/criacao-sites.webp') }}" alt="Criação de Sites">
                <div class="mt-auto"></div>
            </div>
        </div>
    </div>
</section>

<section class="padding-40-0">
    <div class="container">
        <?php /*
            <div class="row justify-content-center side-text-plan-hosting mr-tp-20">
                <h2 class="side-text-right-title f-size20 mr-0">Nossos Clientes</h2>
                <div class="owl-carousel portfolio-carousel owl-theme" data-dots="true">

                    <div class="item">
                        <a href="https://lavanderiastylo.com.br" target="_Blank">
                            <img src="{{ asset('galerias/portfolio/lavanderiastylo.webp?1') }}" alt="Lavanderia Stylo"
                                class="img-fluid" />
                            <div class="description">
                                <h2>Site - Lavanderia Stylo</h2>
                                <p>Lavagem de Estofados</p>
                            </div>
                        </a>
                    </div><!-- item -->

                    <div class="item">
                        <a href="https://jpwtechdigital.com.br" target="_Blank">
                            <img src="{{ asset('galerias/portfolio/jpwtechdigital.webp?1') }}" alt="JPW Tech Digital"
                                class="img-fluid" />
                            <div class="description">
                                <h2>Site - JPW Tech Digital</h2>
                                <p>Agência especializada em Marketing</p>
                            </div>
                        </a>
                    </div><!-- item -->

                </div>
            </div>
            
    */ ?>

        <div class="row justify-content-center mr-tp-50">
            <div class="col-8 text-center">
                <p class="side-text-right-text"><b>Vamos começar seu Projeto?</b></p>
                <a class="plan-dedicated-order-button btn-lg" href="https://wa.me/5516992747526"
                    target="_Blank">Entre em Contato</a>
            </div>
        </div>
    </div>
</section>

<section class="section-wth-amwaj">
    <div class="bg_overlay_section-amwaj">
        <img src="{{ asset('/frontend/img/bg/b_bg_02.jpg') }}" alt="img-bg">
    </div>
    <div class="container">
        <div class="box-features-new row justify-content-start">
            <div class="col-md-4 box-features-item">
                <i class="fa fa-desktop"></i>
                <h5>Painel Administrativo <br> & Otimização</h5>
                <p>Você pode gerenciar todo o conteúdo do seu site através de um painel elegante e prático. Utilizamos
                    as melhores tecnologias do mercado para um melhor desempenho e usabilidade.</p>
            </div>
            <div class="col-md-4 box-features-item">
                <i class="fa fa-mobile"></i>
                <h5>Visual Personalizado <br> & Responsivo</h5>
                <p>Elaboração de visuais únicos e personalizados com o segmento de atuação da empresa ou negócio. Layout
                    do site se adapta a tela dos diversos dispositivos, smartphones, tablets, notebooks e desktops.</p>
            </div>
            <div class="col-md-4 box-features-item">
                <i class="fa fa-chart-line"></i>
                <h5>Otimização S.E.O</h5>
                <p>Sites otimizados que potencializa o posicionamento nos mecanismos de busca, como o Google, Bing,
                    Yahoo... Com recursos robustos essenciais para o melhor aproveitamento em seu projeto.</p>
            </div>
        </div>
    </div>
</section>
@endsection


@section('jsPage')
@endsection
@section('cssPage')
@endsection