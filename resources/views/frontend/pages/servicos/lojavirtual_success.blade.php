@extends('frontend.base')
@section('title', 'Pedido Conclído - Loja Virtual')

@section('content')

    <section class="padding-40-0">
        <div class="container">

            <div class="row justify-content-center">
                <div class="col-2 col-sm-1">
                    <img src="https://i.imgur.com/GwStPmg.png" class="img-fluid">
                </div>
            </div>
            <h2 class="purple-text text-center"><strong>Pedido Concluído !</strong></h2>
            <div class="row justify-content-center mb-5">
                <div class="col-12 col-sm-7 text-center">
                    <h5 class="purple-text text-center">
                        O pagamento se encontra como pendente. <br>
                        Você pode realizar por um dos meios abaixo.
                    </h5>

                    <div class="mb-3">
                        Entre em Contato com nosso Suporte
                    </div>
                    <div>
                        <a href="https://wa.me/5516992747526" target="_Blank" class="btn-whatsapp"><i
                                class="fa fa-whatsapp"></i> (16) 9.9274-7526</a>
                    </div>
                </div>
            </div>

            <div class="row justify-content-center">
                <ul class="list_payments text-center">
                    <li><img src="/galerias/payment/pix.webp" alt="PIX" class="img-fluid col-2"><img
                            src="/galerias/payment/qrcode_nubank.webp" alt="QR Code" class="img-fluid col-2"> <br> Chave
                        E-mail: contato@innsystem.com.br <br> Nu Pagamentos - Leonardo Augusto</li>
                    <li><img src="/galerias/payment/boleto.webp" alt="Boleto Bancário" class="img-fluid col-2"> <br> Boleto
                        Bancário <br> (Você receberá por e-mail o boleto)</li>
                    <li><img src="/galerias/payment/tarja_pagamento.webp" alt="Boleto Bancário" class="img-fluid col-4"> <br>
                        Cartão de Crédito <br> (Mercado Pago, PagSeguro ou PayPal)</li>
                </ul>
            </div>
        </div>
    </section>
@endsection

@section('cssPage')
@endsection

@section('jsPage')

@endsection
