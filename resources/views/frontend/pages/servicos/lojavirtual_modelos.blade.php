@extends('frontend.base')
@section('title', 'Modelos de Loja Virtual')

@section('content')

<section class="padding-70-0-0">
    <div class="container">
        <div class="row justify-content-between mr-tp-50">
            <div class="col-md-12 side-text-right-container">
                <h2 class="side-text-right-title">Modelos Visuais <small>(Loja Virtual)</small></h2>
            </div>
        </div>
    </div>
</section>

<section class="padding-0">
    <div class="container">
        <div class="row justify-content-center side-text-plan-hosting mr-tp-20">
            <div class="owl-carousel portfolio-carousel owl-theme" data-dots="true">
                <div class="item">
                    <a href="https://lojas.innsystem.com.br/base/" target="_Blank">
                        <img src="{{ asset('galerias/modelos_lojavirtual/base.webp?1') }}" alt="Modelo #1 - Base" class="img-fluid" />
                        <div class="description">
                            <h2>Modelo #1 - Base</h2>
                        </div>
                    </a>
                </div><!-- item -->

            </div>
        </div>

        <div class="row justify-content-center mr-tp-50">
            <div class="col-8 text-center">
                <p class="side-text-right-text"><span class="f-size25">É fantástico!</span> <br> Com isso, você receberá
                    cada vez <b>mais visitas</b> e clientes que podem comprar <br> a qualquer hora do dia. Fica
                    <b>online 24 horas</b> por dia.
                </p>
            </div>
        </div>
    </div>
</section>

<section class="padding-70-0">
    <div class="container">
        <div class="row justify-content-center first-pricing-table-container">
            <div class="col-md-5">
                <p class="side-text-right-text f-size25 text-center"><b>Vamos começar sua Loja Virtual?</b></p>

                <div id="monthly-yearly-chenge" class="mr-tp-20 custom-change text-center">
                    <a class="monthly-price f-size12"> <span class="change-box-text">Plano Mensal</span> <span class="change-box"></span></a>
                    <a class="active yearli-price f-size12"> <span class="change-box-text">Plano Anual (-15%)</span></a>
                </div>
            </div>
        </div>
        <div class="row justify-content-center first-pricing-table-container mr-tp-10">
            <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                <div class="first-pricing-table">
                    <div class="content">
                        <i class="fas fa-cart-plus first-pricing-table-icon"></i>
                        <h5 class="first-pricing-table-title">Starting <span>para você que está <br> começando no
                                <b>mundo digital</b>!</span></h5>

                        <ul class="first-pricing-table-body">
                            <li><b>Ativação Imediata</b></li>
                            <li>Visitas, Produtos & Pedidos <br> <b>SEM LIMITE</b></li>
                            <li>Treinamento em Vídeo <small>(<a href="https://www.youtube.com/playlist?list=PLCHp7y2B24qmh4YqHbDPr7NKNvd--f7pl" target="Blank">disponível no Youtube</a>)</small></a></li>
                            <li>Suporte Prioritario no WhatsApp</li>
                            <li>Modelos de Visuais (5) <br> + Personalização * <small>(<a href="{{ url('/loja-virtual/modelos') }}" target="_Blank">conheça os modelos</a>)</small></li>
                            <li>Pacote de 5 artes digitais <b>BRINDE</b></li>
                        </ul>

                        <span style="text-align:center; margin-top:20px;" class="plan-price second-pricing-table-price yearly">
                            <i class="monthly">R$ 100 <span style="opacity: 0.5;font-size: 14px;">/mês</span></i>
                            <i class="yearly">R$ 1020 <span style="opacity: 0.5;font-size: 14px;">/ano</span> <br> <span style="opacity: 0.5;font-size: 14px;">Economize R$ 180,00</span></i>
                        </span>


                    </div>
                    <a class="first-pricing-table-order price-period-monthly d-none" href="{{ url('/loja-virtual/contratar/1/monthly/') }}"><i class="fa fa-whatsapp"></i>
                        CONTRATAR AGORA</a>
                    <a class="first-pricing-table-order price-period-yearly" href="{{ url('/loja-virtual/contratar/1/yearly/') }}"><i class="fa fa-whatsapp"></i>
                        CONTRATAR AGORA</a>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                <div class="first-pricing-table best-plan">
                    <span class="first-pricing-table-most">EXCLUSIVE</span>
                    <div class="content">
                        <i class="fas fa-rocket first-pricing-table-icon"></i>
                        <h5 class="first-pricing-table-title">Rocket <span>ideal para entrar no mundo digital <br>e nos
                                principais <b>marketplaces</b>!</span></h5>

                        <ul class="first-pricing-table-body">
                            <li class="active"><b><i class="fa fa-angle-left"></i> Tudo do plano Starting</b></li>
                            <li>Marketplaces (TinyERP)</li>
                            <li>Atacado & Varejo</li>
                            <li>Programa de Afiliados</li>
                            <li>Modelos de Visuais (5) <br> + Personalização * <small>(<a href="{{ url('/loja-virtual/modelos') }}" target="_Blank">conheça os modelos</a>)</small></li>
                            <li>Pacote de 10 artes digitais <b>BRINDE</b> </li>
                        </ul>

                        <span style="text-align:center; margin-top:20px;" class="plan-price second-pricing-table-price yearly">
                            <i class="monthly">R$ 200 <span style="opacity: 0.5;font-size: 14px;">/mês</span></i>
                            <i class="yearly">R$ 2040 <span style="opacity: 0.5;font-size: 14px;">/ano</span> <br> <span style="opacity: 0.5;font-size: 14px;">Economize R$ 360,00</span></i>
                        </span>
                    </div>
                    <a class="first-pricing-table-order price-period-monthly d-none" href="{{ url('/loja-virtual/contratar/4/monthly/') }}"><i class="fa fa-whatsapp"></i>
                        CONTRATAR AGORA</a>
                    <a class="first-pricing-table-order price-period-yearly" href="{{ url('/loja-virtual/contratar/4/yearly/') }}"><i class="fa fa-whatsapp"></i>
                        CONTRATAR AGORA</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="padding-40-0">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="table-responsive">
                    <table id="prices" class="table table-striped">
                        <thead id="theadHide">
                            <tr class="price-dsc" id="toDisapear">
                                <th class="">

                                </th>
                                <th class="checklist left">
                                    <p>
                                        recursos do plano
                                        <span class="soft-type oceanlight"><i class="fa fa-cart-plus"></i>
                                            Starting</span>
                                    </p>
                                </th>
                                <th class="checklist left">
                                    <p>
                                        recursos do plano
                                        <span class="soft-type oceandark"><i class="fa fa-rocket"></i> Rocket</span>
                                    </p>
                                </th>
                            </tr>
                        </thead>
                        <tbody id="features-body">
                            <tr class="feature active">
                                <td class="feature" colspan="3"><i class="fa fa-heart"></i> Recursos Exclusivo do Plano
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Visitas na Loja</span>
                                </td>
                                <td>
                                    <span>Ilimitadas</span>
                                </td>
                                <td>
                                    <span>Ilimitadas</span>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Produtos Cadastrados</span>
                                </td>
                                <td>
                                    <span>Ilimitadas</span>
                                </td>
                                <td>
                                    <span>Ilimitadas</span>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Pedidos Realizados</span>
                                </td>
                                <td>
                                    <span>Ilimitadas</span>
                                </td>
                                <td>
                                    <span>Ilimitadas</span>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Marketplaces (TinyERP)</span>
                                </td>
                                <td>
                                    <i class="fa fa-remove text-danger"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Atacado & Varejo</span>
                                </td>
                                <td>
                                    <i class="fa fa-remove text-danger"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Programa de Afiliados</span>
                                </td>
                                <td>
                                    <i class="fa fa-remove text-danger"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Atendimento & Suporte</span>
                                </td>
                                <td>
                                    <span>E-mail + WhatsApp</span>
                                </td>
                                <td>
                                    <span>E-mail + WhatsApp</span>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Modelos de Visuais</span>
                                </td>
                                <td>
                                    <span>5 Temas + Personalização</span>
                                </td>
                                <td>
                                    <span>5 Temas + Personalização</span>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Pacote de Artes Digitais</span>
                                </td>
                                <td>
                                    <span>5 artes digitais <br> (feed, story, box, slider ou faixa)</span>
                                </td>
                                <td>
                                    <span>10 artes digitais <br> (feed, story, box, slider ou faixa)</span>
                                </td>
                            </tr>


                            <tr class="feature active">
                                <td class="feature" colspan="3"><i class="fa fa-credit-card"></i> Formas de Pagamento
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Depósito e Transferência</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">PayPal (Cartões)</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">PagSeguro (Cartões e Boleto)</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Pagamento na Entrega/Local</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>

                            <tr class="feature active">
                                <td class="feature" colspan="3"><i class="fa fa-truck"></i> Formas de Envios</td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Frete Grátis com Valor Mínimo</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Melhor Envio (Correios e Transportadoras)</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Frenet (Correios e Transportadoras)</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Retirar na Loja</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>

                            <tr class="feature active">
                                <td class="feature" colspan="3"><i class="fa fa-star"></i> Recursos Padrões</td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Funcionamento 24 horas por dia</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Instalação & Configuração</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Treinamento em Vídeo (passo-a-passo)</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Certificado de Segurança Digital (SSL)</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Hospedagem para seu Domínio</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">5 Contas de E-mail @seudominio</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Layout Responsivo e Customizavel</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Integração com o Google Analytics</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Otimização de Site S.E.O</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Backup & Manutenção</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Simulação de Frete com os Correios</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Produtos com Opções de Variações</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Produtos com Marcas e Comentários</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Imagens de Produtos com Zoom</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Pedidos com Valor Mínimo</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Cupons de Descontos para Clientes</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Informativo aos Clientes (por e-mail)</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Páginas de Informações e Institucionais</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Relatórios de Vendas, Produtos e Clientes</span>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                                <td>
                                    <i class="fa fa-check text-success"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Contrato de Fidelidade</span>
                                </td>
                                <td>
                                    <i class="fa fa-remove text-danger"></i>
                                </td>
                                <td>
                                    <i class="fa fa-remove text-danger"></i>
                                </td>
                            </tr>
                            <tr class="feature">
                                <td class="feature">
                                    <span class="name">Sem taxas % de comissão</span>
                                </td>
                                <td>
                                    <i class="fa fa-remove text-danger"></i>
                                </td>
                                <td>
                                    <i class="fa fa-remove text-danger"></i>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- row -->
    </div><!-- container -->
</section>

@endsection


@section('jsPage')
<script>
    // hosting plans with chekout price yearly and monthly //
    $('#monthly-yearly-chenge a').on('click', function() {
        $(this).addClass('active').siblings().removeClass('active');

        if (jQuery('.monthly-price').hasClass('active')) {
            $('.second-pricing-table-price').addClass('monthly');
            $('.second-pricing-table-price').removeClass('yearly');

            $('.price-period-yearly').addClass('d-none');
            $('.price-period-monthly').removeClass('d-none');
        }

        if (jQuery('.yearli-price').hasClass('active')) {
            $('.second-pricing-table-price').addClass('yearly');
            $('.second-pricing-table-price').removeClass('monthly');

            $('.price-period-monthly').addClass('d-none');
            $('.price-period-yearly').removeClass('d-none');
        }
    });
</script>
@endsection
@section('cssPage')
@endsection