@extends('frontend.base')
@section('title', 'Política de Privacidade')

@section('content')

<section class="padding-70-0 position-relative how-it-work-section">
    <div class="container">
        <div class="row justify-content-center mr-tp-0 how-it-work-section-row">
            <div class="col-md-8 text-center">
                <h2 class="side-text-right-title">@yield('title')</h2>
                <p><b>1. Introdução</b></p>
                <p>Bem-vindo ao InnSystem! A sua privacidade é importante para nós. Esta Política de Privacidade explica como coletamos, usamos e protegemos suas informações pessoais quando você visita nosso site e utiliza nossos serviços.</p>

                <p><b>2. Coleta de Informações</b></p>
                <p>Coletamos informações pessoais que você nos fornece diretamente, como nome, endereço de e-mail, telefone e qualquer outra informação que você decida compartilhar conosco ao preencher formulários, criar uma conta ou entrar em contato conosco.</p>
                <p>Também coletamos informações automaticamente sobre o uso do nosso site, como seu endereço IP, tipo de navegador, páginas visitadas e o tempo gasto em cada página. Usamos tecnologia do Google Analytics para ajudar a coletar essas informações.</p>

                <p><b>3. Uso das Informações</b></p>
                <p>Utilizamos suas informações pessoais para:</p>
                <ul>
                    <li>Fornecer e melhorar nossos serviços.</li>
                    <li>Processar transações e gerenciar sua conta.</li>
                    <li>Enviar atualizações e informações sobre nossos serviços.</li>
                    <li>Responder a perguntas e fornecer suporte.</li>
                    <li>Personalizar sua experiência em nosso site.</li>
                </ul>

                <p><b>4. Compartilhamento de Informações</b></p>
                <p>Não compartilhamos suas informações pessoais com terceiros, exceto nas seguintes circunstâncias:</p>
                <ul>
                    <li>Com prestadores de serviços que ajudam na operação do nosso site e na prestação dos nossos serviços.</li>
                    <li>Quando exigido por lei ou em resposta a processos legais.</li>
                    <li>Para proteger os direitos, propriedade ou segurança do InnSystem, nossos usuários ou outros.</li>
                </ul>

                <p><b>5. Segurança</b></p>
                <p>Adotamos medidas de segurança adequadas para proteger suas informações pessoais contra acesso não autorizado, alteração, divulgação ou destruição. No entanto, nenhum sistema é completamente seguro e não podemos garantir a segurança absoluta.</p>

                <p><b>6. Seus Direitos</b></p>
                <p>Você tem o direito de acessar, corrigir ou excluir suas informações pessoais. Caso queira exercer esses direitos ou tenha dúvidas sobre como suas informações são tratadas, entre em contato conosco através dos meios fornecidos abaixo.</p>

                <p><b>7. Alterações na Política de Privacidade</b></p>
                <p>Podemos atualizar esta Política de Privacidade periodicamente para refletir mudanças em nossas práticas ou requisitos legais. Recomendamos que você revise esta política regularmente para se manter informado sobre como estamos protegendo suas informações.</p>

                <p><b>8. Contato</b></p>
                <p>Se você tiver dúvidas ou preocupações sobre nossa Política de Privacidade, entre em contato conosco pelo e-mail: <a href="mailto:seuemail@innsystem.com.br">seuemail@innsystem.com.br</a>.</p>

            </div>
        </div>
    </div>
</section>

@endsection


@section('jsPage')
@endsection
@section('cssPage')
@endsection