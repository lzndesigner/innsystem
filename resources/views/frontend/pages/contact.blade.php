@extends('frontend.base')
@section('title', 'Estamos ansiosos para atender de segunda à segunda')

@section('main_home')
    <main class="container mb-auto">
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-5 text-center">
                        <h3 class="mt-3 main-header-text-title mr-tp-60"><span>Você possui alguma dúvida?</span> vamos
                            conversar</h3>
                        <h5 class="immediate-help-center-title text-white">Entre em Contato direto</h5>
                        <a class="immediate-help-center-link text-white"
                            href="https://wa.me/5516992747526" target="_Blank"><i
                                class="fab fa-whatsapp"></i> +55 (16) 9.9274-7526</a>
                        <a class="immediate-help-center-link text-white" href="mailto:contato@innsystem.com.br"><i
                                class="fa fa-envelope"></i> contato@innsystem.com.br</a>

                        <p class="immediate-help-center-text text-white">siga nas redes sociais</p>
                        <div class="contact-us-social-icons mr-tp-30">
                            <a class="fb" href="https://facebook.com/innsystem" target="_Blank"><i
                                    class="fab fa-facebook-f"></i></a>
                            <a class="tg" href="https://instagram.com/innsystem" target="_Blank"><i
                                    class="fab fa-instagram"></i></a>
                            <a class="wa" href="https://wa.me/5516992747526" target="_Blank"><i
                                    class="fa fa-whatsapp"></i></a>
                            <a class="yb" href="https://www.youtube.com/channel/UC4bXTS73cCs_qzY29M6J2lw" target="_Blank"><i
                                    class="fab fa-youtube"></i></a>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <form class="row form-contain-home contact-page-form-send" id="form-request" method="post"
                            action="{{ route('frontend.contact.sendmail') }}">
                            @csrf
                            <h5>Entre em Contato <span>Gostaríamos de oferecer nossas soluções para seu projeto.</span></h5>
                            <div id="form-messages"></div>

                            <div class="col-md-12">

                                <div class="field input-field">
                                    <input class="form-contain-home-input" type="text" id="name" name="name"
                                        placeholder="Seu Nome" required="">

                                    <i class="fas fa-user"></i>
                                </div>
                            </div>

                            <div class="col-md-12">

                                <div class="field input-field">
                                    <input class="form-contain-home-input" type="email" id="email" name="email"
                                        placeholder="Seu E-mail" required="">

                                    <i class="far fa-envelope"></i>
                                </div>
                            </div>

                            <div class="col-md-12">

                                <div class="field input-field">
                                    <textarea class="form-contain-home-input" id="message" name="message"
                                        placeholder="Descreva como podemos ajudar-lo" required=""></textarea>
                                </div>
                            </div>

                            <div class="btn-holder-contect">
                                <button type="submit" id="btn-sendmail">Enviar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <div class="mt-auto"></div>
@endsection

@section('content')
    <section class="padding-70-0">
        <div class="container">
            <h3 class="map-comment-area-title">Conheça nossa Central de Ajuda <small>(HelpDesk)</small></h3>
            <p class="map-comment-area-text">Disponibilizamos uma variedades de dicas úteis para seu negócio, esclarecemos
                as principais dúvidas e preparamos diversos tutoriais e treinamento em vídeos para nossos principais
                serviços e necessidades do dia-a-dia.</p>
            <div class="text-center">
                <a class="plan-dedicated-order-button f-size15" href="{{ url('/helpdesk') }}">conheça mais</a>
            </div>
        </div>
    </section>
@endsection

@section('cssPage')
    <link rel="stylesheet" href="{{ asset('/general/plugins/sweetalert/sweetalert2.min.css') }}">
@endsection

@section('jsPage')
    <script src="{{ asset('/general/plugins/sweetalert/sweetalert2.min.js') }}"></script>
    <script>
        $(document).on('click', '#btn-sendmail', function(e) {
            e.preventDefault();


            //disable the submit button
            $("form #btn-sendmail").attr("disabled", true);
            $('form #btn-sendmail').append('<i class="fa fa-spinner fa-spin ml-3"></i>');
            setTimeout(function() {
                $('form #btn-sendmail').prop("disabled", false);
                $('form #btn-sendmail').find('.fa-spinner').addClass('d-none');
            }, 8000);


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });

            $(this).attr('disabled', true);

            var url = "{{ route('frontend.contact.sendmail') }}";
            var method = 'POST';

            var data = $('#form-request').serialize();
            $.ajax({
                url: url,
                data: data,
                method: method,
                success: function(data) {
                    Swal.fire({
                        text: data,
                        icon: 'success',
                        showClass: {
                            popup: 'animate_animated animate_backInUp'
                        },
                        onClose: () => {
                            // Loading page listagem
                            $('#form-request')[0].reset();
                            $('#btn-sendmail').attr('disabled', false);
                        }
                    });
                },
                error: function(xhr) {
                    if (xhr.status === 422) {
                        Swal.fire({
                            text: 'Validação: ' + xhr.responseJSON,
                            icon: 'warning',
                            showClass: {
                                popup: 'animate_animated animate_wobble'
                            }
                        });
                    } else {
                        Swal.fire({
                            text: 'Erro interno, informe ao suporte: ' + xhr.responseJSON,
                            icon: 'error',
                            showClass: {
                                popup: 'animate_animated animate_wobble'
                            }
                        });
                    }
                }
            });
        });
    </script>
@endsection
