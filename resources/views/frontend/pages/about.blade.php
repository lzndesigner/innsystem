@extends('frontend.base')
@section('title', 'Criação de Site, Loja Virtual e Design Gráfico')

@section('content')

<section class="padding-70-0 position-relative how-it-work-section">
    <div class="container">


        <div class="row justify-content-center mr-tp-0 how-it-work-section-row">
            <div class="col-md-8 text-center">
                <h2 class="side-text-right-title">Um pouco sobre a empresa</h2>
                <p class="side-text-right-text">
                    Somos uma empresa que reside em <u>Ribeirão Preto / SP</u>, atuamos com o desenvolvimento de <b>sites, lojas virtuais e design gráfico</b> desde 2010, unindo <i>inovação e usabilidade</i> para potencializar seu <b>negócio na internet</b> com as melhores <i>tecnologias</i> do mercado.
                    <br>
                    <br>
                    Temos prazer de atender nossos clientes com total <b>atenção</b> e <b>gentileza</b> buscando sempre a melhor <u>solução</u> para atingir os objetivos. Nosso ambiente de trabalho nos proporciona uma perfeita harmonia para oferecer o <b>melhor suporte</b>, qualidade em termos de design e tecnologias.
                </p>
            </div>
        </div>
        <div class="row justify-content-center mr-tp-20 how-it-work-section-row">
            <div class="col-md-4">
                <div class="how-it-works-box">
                    <i class="h-flaticon-019-uptime"></i>
                    <h5>Loja Virtual <small>(E-commerce)</small></h5>
                    <p>Venda pela internet proporciona uma experiência incrível para seus clientes.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="how-it-works-box">
                    <i class="h-flaticon-021-browser"></i>
                    <h5>Design Gráfico</h5>
                    <p>Criação de logos, identidade visual, interfaces de aplicativos web e mobile, entre outros...</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="how-it-works-box">
                    <i class="h-flaticon-009-devices"></i>
                    <h5>Criação de Sites</h5>
                    <p>Desenvolvimento de sites com recursos essenciais para alavancar sua empresa no mundo digital.</p>
                </div>
            </div>
        </div>

        <div class="row justify-content-start mr-tp-50">
            <div class="col-12 mb-5">
                <h5 class="title-default-coodiv-two">Acompanhe nas redes sociais</h5>
            </div>
            <div class="col-md-4">
                <div class="host-pack-features">
                    <a href="https://www.facebook.com/innsystem" target="_Blank"><i class="fa fa-facebook"></i></a>
                    <h5>Facebook</h5>
                    <p><a href="https://www.facebook.com/innsystem" target="_Blank">@innsystem</a></p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="host-pack-features">
                    <a href="https://www.instagram.com/innsystem/" target="_Blank"><i class="fa fa-instagram"></i></a>
                    <h5>Instagram</h5>
                    <p><a href="https://www.instagram.com/innsystem/" target="_Blank">@innsystem</a></p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="host-pack-features">
                    <a href="https://www.youtube.com/channel/UC4bXTS73cCs_qzY29M6J2lw" target="_Blank"><i class="fa fa-youtube"></i></a>
                    <h5>Youtube</h5>
                    <p><a href="https://www.youtube.com/channel/UC4bXTS73cCs_qzY29M6J2lw" target="_Blank">Central de Ajuda</a></p>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection


@section('jsPage')
@endsection
@section('cssPage')
@endsection