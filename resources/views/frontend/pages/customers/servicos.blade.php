@extends('frontend.pages.customers.base')
@section('title', 'Meus Serviços')

@section('content-account')

    <div class="row">
        <div class="col-sm-12">
            <section class="position-relative">
                <div class="row justify-content-left server-tabls-head">
                    <div class="col-md-5">Serviço</div>
                    <div class="col-md-2">Início</div>
                    <div class="col-md-2">Vencimento</div>
                    <div class="col-md-2">Valor</div>
                    <div class="col-md-1"></div>
                </div>
                <div class="server-tabls-body">

                    @foreach ($myServices as $myService)
                        <div class="row justify-content-left align-items-center server-tabls-row">
                            <div class="col-md-1">
                                @if ($myService->status == 'ativo')
                                    <span class="badge badge-success"><i class="fa fa-check"></i>
                                        {{ $myService->status }}</span>
                                @elseif($myService->status == 'pendente')
                                    <span class="badge badge-info"><i class="fa fa-minus"></i>
                                        {{ $myService->status }}</span>
                                @else
                                    <span class="badge badge-secondary"><i class="fa fa-remove"></i>
                                        {{ $myService->status }}</span>
                                @endif
                            </div>
                            <div class="col-md-4"><b>{{ $myService->nameService }}</b>
                                <br><i>{{ $myService->dominio }}</i></div>
                            <div class="col-md-2">
                                <b>{{ \Carbon\Carbon::parse($myService->date_start)->format('d/m/Y') }}</b></div>
                            <div class="col-md-2">
                                <b>{{ \Carbon\Carbon::parse($myService->date_end)->format('d/m/Y') }}</b></div>
                            <div class="col-md-2"><b>R$ {{ $myService->price }}</b> <br> ({{ $myService->period }})
                            </div>
                            <div class="col-md-1"><button class="btn btn-xs btn-primary"><i class="fa fa-bars"></i></button>
                            </div>
                        </div>
                    @endforeach

                </div>
            </section>
        </div>
    </div>

@endsection

@section('cssPage')
@endsection

@section('jsPage')

@endsection
