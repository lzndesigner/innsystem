@extends('frontend.base')
@section('title', 'Minha Conta')

@section('content')

    <section class="padding-40-0">
        <div class="container">

            <div class="row justify-content-center">

                <div class="col-sm-3">
                    <div class="card menu-accounts">
                        @include('frontend.pages.customers.includes.menu-account')
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="card contact-page-form-send mt-2 mb-3">
                        <h2 class="purple-text text-left mb-4">@yield('title')</h2>

                        @yield('content-account')
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('cssPage')
@endsection

@section('jsPage')

@endsection
