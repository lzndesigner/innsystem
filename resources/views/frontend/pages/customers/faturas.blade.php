@extends('frontend.pages.customers.base')
@section('title', 'Minhas Faturas')

@section('content-account')

    <div class="row">
        <div class="col-sm-12">
            <section class="position-relative">
                <div class="row justify-content-left server-tabls-head">
                    <div class="col-md-5">Descrição</div>
                    <div class="col-md-2">Gerada em</div>
                    <div class="col-md-2">Vence em</div>
                    <div class="col-md-2">Total</div>
                    <div class="col-md-1"></div>
                </div>
                <div class="server-tabls-body">
                    @if ($myInvoices)
                        @foreach ($myInvoices as $myInvoice)
                            <div class="row justify-content-left align-items-center server-tabls-row">
                                <div class="col-md-4">
                                    @if ($myInvoice->status == 'pago')
                                        <span class="badge badge-success">Pago</span>
                                    @elseif($myInvoice->status == 'nao_pago')
                                        <span class="badge badge-danger">Não Pago</span>
                                    @else
                                        <span class="badge badge-secondary">Cancelado</span>
                                    @endif
                                    <b>Fatura: #{{ $myInvoice->id }}</b>
                                </div>
                                <div class="col-md-2">
                                    <b>{{ \Carbon\Carbon::parse($myInvoice->date_invoice)->format('d/m/Y') }}</b>
                                </div>
                                <div class="col-md-2">
                                    <b>{{ \Carbon\Carbon::parse($myInvoice->date_end)->format('d/m/Y') }}</b>
                                    {{ $myInvoice->date_payment ? \Carbon\Carbon::parse($myInvoice->date_payment)->format('d/m/Y') : '-' }}
                                </div>
                                <div class="col-md-3"><b>R$ {{ $myInvoice->price }}</b> <br>
                                    <small>({{ $myInvoice->payment_method }})</small>
                                </div>
                                <div class="col-md-1"><a href="{{ url('/minha-conta/fatura/'.$myInvoice->id) }}" target="_Blank" class="btn btn-xs p-1 f-size12 btn-primary">
                                    <i class="fa fa-eye"
                                            aria-hidden="true"></i>
                                </a>
                                </div>
                                <div class="col-md-12">
                                    <blockquote>
                                        <ul>
                                            @foreach (json_decode($myInvoice->description) as $description)
                                                <li>{{ $description }}</li>
                                            @endforeach
                                        </ul>
                                    </blockquote>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </section>
        </div>
    </div>

@endsection

@section('cssPage')
@endsection

@section('jsPage')

@endsection
