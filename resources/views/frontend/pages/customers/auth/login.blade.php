@extends('frontend.base')
@section('title', 'Central do Cliente')

@section('content')

    <section class="padding-40-0">
        <div class="container">

            <div class="row justify-content-center">
                <div class="col-xs-12 col-md-6">
                    <div class="card contact-page-form-send mt-2 mb-3">
                        <h2 class="purple-text text-center mb-4"><strong>Central do Cliente</strong></h2>
                        <form class="form-horizontal" method="POST" action="{{ route('customer.auth.loginCustomer') }}">
                            {{ csrf_field() }}
                            @include('elements.messages')

                            <div class="d-flex justify-content-center my-3 d-none">
                                <a href="{{route('customer.auth.login.google')}}" class="btn btn-sm btn-primary"><i class="fa fa-google"></i></a>
                            </div>

                            <div class="card">
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <div class="field input-field">
                                        <input type="email" class="form-contain-home-input" id="email" name="email"
                                            value="{{ old('email') }}" placeholder="Digite seu e-mail de acesso" required
                                            autofocus>
                                        <i class="fa fa-user" aria-hidden="true"></i>
                                    </div>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <div class="field input-field">
                                        <input id="password" type="password" class="form-contain-home-input" name="password"
                                            required>
    
                                        <i class="fa fa-key" aria-hidden="true"></i>
                                    </div>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <button type="submit" class="btn btn-primary btn-icon-blue">
                                   <i class="fa fa-sign-in"></i> Acessar Minha Conta
                                </button>
                            </div>
                            <div class="form-group d-none">
                                <div class="col-md-12">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                            Lembrar-me
                                        </label>
                                    </div>
                                </div>
                            </div>



                            <div class="form-group mt-4">
                                <div class="col-md-12"> 
                                    <p class="mt-4 f-size13">Ainda não é cadastrado? Crie agora uma <a
                                            href="{{ route('customer.register') }}" class="">nova conta.</a></p>
                                    <p class="mt-4 f-size13">Esqueceu sua senha? Solicite uma nova senha <a
                                            href="{{ route('forget.password.get') }}" class="">clicando aqui.</a></p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('cssPage')
@endsection

@section('jsPage')

@endsection
