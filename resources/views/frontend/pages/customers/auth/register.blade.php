@extends('frontend.base')
@section('title', 'Faça seu Cadastro')

@section('content')

    <section class="padding-40-0">
        <div class="container">

            <div class="row justify-content-center">
                <div class="col-xs-12 col-md-7">
                    <div class="card contact-page-form-send mt-2 mb-3">
                        <h2 class="purple-text text-center mb-4"><strong>@yield('title')</strong></h2>
                        <form id="form-request" class="form-horizontal" method="POST">
                            {{ csrf_field() }}
                            @include('elements.messages')

                            <div class="card">
                                <p><small><b>Campos obrigatórios (<span class="text-danger">*</span>)</b></small></p>

                                <fieldset>
                                    <h6>Dados Pessoais</h6>
                                    <div class="form-row align-items-center">
                                        <div class="col-xs-6 col-md-7">
                                            <div class="form-group">
                                                <div class="field input-field">
                                                    <label for="name" class="col-form-label">Cliente <small>(<span
                                                                class="text-danger">*</span>)</small>:</label>
                                                    <input type="text" id="name" name="name" class="form-contain-home-input"
                                                        placeholder="Cliente"
                                                        value="{{ isset($result->name) ? $result->name : '' }}" required>
                                                </div><!-- form-group -->
                                            </div><!-- form-group -->
                                        </div><!-- col -->

                                        <div class="col-xs-6 col-md-5">
                                            <div class="form-group">
                                                <div class="field input-field">
                                                    <label for="company" class="col-form-label">Empresa (<span
                                                            class="text-danger">*</span>):</label>
                                                    <input type="text" id="company" name="company"
                                                        class="form-contain-home-input" placeholder="Empresa"
                                                        value="{{ isset($result->company) ? $result->company : '' }}"
                                                        required>
                                                </div><!-- form-group -->
                                            </div><!-- form-group -->
                                        </div><!-- col -->

                                    </div>
                                    <div class="form-row align-items-center">
                                        <div class="col-xs-6 col-md-6">
                                            <div class="form-group">
                                                <div class="field input-field">
                                                    <label for="document" class="col-form-label">Documento CPF/CNPJ (<span
                                                            class="text-danger">*</span>):</label>
                                                    <input type="text" id="document" name="document"
                                                        class="form-contain-home-input" placeholder="Documento"
                                                        value="{{ isset($result->document) ? $result->document : '' }}"
                                                        required>
                                                </div><!-- form-group -->
                                            </div><!-- form-group -->
                                        </div><!-- col -->
                                        <div class="col-xs-6 col-md-6">
                                            <div class="form-group">
                                                <div class="field input-field">
                                                    <label for="phone" class="col-form-label">Telefone (<span
                                                            class="text-danger">*</span>):</label>
                                                    <input type="text" id="phone" name="phone"
                                                        class="form-contain-home-input formatPhone" placeholder="Telefone"
                                                        value="{{ isset($result->phone) ? $result->phone : '' }}"
                                                        required>
                                                </div><!-- form-group -->
                                            </div><!-- form-group -->
                                        </div><!-- col -->
                                    </div><!-- form-row -->
                                </fieldset>


                                <fieldset>
                                    <h6>Dados de Acesso</h6>
                                    <div class="form-row">
                                        <div class="col-xs-12 col-md-12">
                                            <div class="form-group">
                                                <div class="field input-field">
                                                    <label for="email" class="col-form-label">E-mail de Acesso (<span
                                                            class="text-danger">*</span>):</label>
                                                    <input type="email" id="email" name="email"
                                                        class="form-contain-home-input" placeholder="E-mail de Acesso"
                                                        value="{{ isset($result->email) ? $result->email : '' }}"
                                                        required>
                                                </div><!-- form-group -->
                                            </div><!-- form-group -->
                                        </div><!-- col -->
                                    </div><!-- form-row -->

                                    <div class="form-row">
                                        <div class="col-xs-6 col-md-6">
                                            <div class="form-group">
                                                <div class="field input-field">
                                                    <label for="password" class="col-form-label">Senha (<span
                                                            class="text-danger">*</span>):</label>
                                                    <input type="password" id="password" name="password"
                                                        class="form-contain-home-input" placeholder="Senha">
                                                    <span class="help-block">Mínimo de 6 caracteres</span>
                                                </div><!-- form-group -->
                                            </div><!-- form-group -->
                                        </div><!-- col -->

                                        <div class="col-xs-6 col-md-6">
                                            <div class="form-group">
                                                <div class="field input-field">
                                                    <label for="password_confirmation" class="col-form-label">Confirmar
                                                        Senha
                                                        (<span class="text-danger">*</span>):</label>
                                                    <input type="password" id="password_confirmation"
                                                        name="password_confirmation" class="form-contain-home-input"
                                                        placeholder="Confirmar Senha">
                                                </div><!-- form-group -->
                                            </div><!-- form-group -->
                                        </div><!-- col -->
                                    </div><!-- form-row -->
                                </fieldset>

                                <fieldset>

                                    <h6>Dados de Endereço</h6>
                                    <div class="form-row">
                                        <div class="col-xs-3 col-md-3">
                                            <div class="form-group">
                                                <div class="field input-field">
                                                    <label for="cep" class="col-form-label">CEP (<span
                                                            class="text-danger">*</span>):</label>
                                                    <input type="text" id="cep" name="cep" class="form-contain-home-input"
                                                        placeholder="CEP"
                                                        value="{{ isset($result->cep) ? $result->cep : '' }}" required>
                                                </div><!-- form-group -->
                                            </div><!-- form-group -->
                                        </div><!-- col -->

                                        <div class="col-xs-6 col-md-6">
                                            <div class="form-group">
                                                <div class="field input-field">
                                                    <label for="address" class="col-form-label">Endereço (<span
                                                            class="text-danger">*</span>):</label>
                                                    <input type="text" id="address" name="address"
                                                        class="form-contain-home-input" placeholder="Endereço"
                                                        value="{{ isset($result->address) ? $result->address : '' }}"
                                                        required>
                                                </div><!-- form-group -->
                                            </div><!-- form-group -->
                                        </div><!-- col -->

                                        <div class="col-xs-3 col-md-3">
                                            <div class="form-group">
                                                <div class="field input-field">
                                                    <label for="number" class="col-form-label">Número (<span
                                                            class="text-danger">*</span>):</label>
                                                    <input type="text" id="number" name="number"
                                                        class="form-contain-home-input" placeholder="Número"
                                                        value="{{ isset($result->number) ? $result->number : '' }}"
                                                        required>
                                                </div><!-- form-group -->
                                            </div><!-- form-group -->
                                        </div><!-- col -->
                                    </div><!-- form-row -->

                                    <div class="form-row">
                                        <div class="col-xs-12 col-md-12">
                                            <div class="form-group">
                                                <div class="field input-field">
                                                    <label for="complement" class="col-form-label">Complemento:</label>
                                                    <input type="text" id="complement" name="complement"
                                                        class="form-contain-home-input" placeholder="Complemento"
                                                        value="{{ isset($result->complement) ? $result->complement : '' }}">
                                                </div><!-- form-group -->
                                            </div><!-- form-group -->
                                        </div><!-- col -->
                                    </div><!-- form-row -->

                                    <div class="form-row">
                                        <div class="col-xs-4 col-md-4">
                                            <div class="form-group">
                                                <div class="field input-field">
                                                    <label for="city" class="col-form-label">Cidade (<span
                                                            class="text-danger">*</span>):</label>
                                                    <input type="text" id="city" name="city" class="form-contain-home-input"
                                                        placeholder="Cidade"
                                                        value="{{ isset($result->city) ? $result->city : '' }}" required>
                                                </div><!-- form-group -->
                                            </div><!-- form-group -->
                                        </div><!-- col -->

                                        <div class="col-xs-4 col-md-4">
                                            <div class="form-group">
                                                <div class="field input-field">
                                                    <label for="state" class="col-form-label">Estado (<span
                                                            class="text-danger">*</span>):</label>
                                                    <input type="text" id="state" name="state"
                                                        class="form-contain-home-input" placeholder="Estado"
                                                        value="{{ isset($result->state) ? $result->state : '' }}"
                                                        required>
                                                </div><!-- form-group -->
                                            </div><!-- form-group -->
                                        </div><!-- col -->
                                    </div><!-- form-row -->
                                </fieldset>


                                <button type="submit" id="btn-salvar" class="btn btn-primary btn-icon-blue">
                                    <i class="fa fa-user-plus"></i> Cadastrar minha conta
                                </button>
                            </div>
                            <div class="form-group d-none">
                                <div class="col-md-12">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="remember"
                                                {{ old('remember') ? 'checked' : '' }}>
                                            Lembrar-me
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group mt-4">
                                <div class="col-md-12">
                                    <p class="mt-4 f-size13">Você já tem uma conta? <a
                                            href="{{ route('customer.auth.login') }}" class="">Acesse agora.</a></p>
                                    <p class="mt-4 f-size13">Esqueceu sua senha? Solicite uma nova senha <a
                                            href="{{ route('forget.password.get') }}" class="">clicando aqui.</a></p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('cssPage')
    <link rel="stylesheet" href="{{ asset('/general/plugins/sweetalert/sweetalert2.min.css') }}">
@endsection

@section('jsPage')
    <script src="{{ asset('/general/plugins/sweetalert/sweetalert2.min.js') }}"></script>
    <script>
        // Button Save Forms - Create and Edit
        $(document).on('click', '#btn-salvar', function(e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });


            var url = "{{ url('/minha-conta/register') }}";
            var method = 'POST';

            var data = $('#form-request').serialize();

            $.ajax({
                url: url,
                data: data,
                method: method,
                success: function(data) {
                    Swal.fire({
                        text: data,
                        icon: 'success',
                        showClass: {
                            popup: 'animate_animated animate_backInUp'
                        },
                        onClose: () => {
                            // Loading page listagem
                            location.href = "{{ url('/minha-conta') }}";
                        }
                    });
                },
                error: function(xhr) {
                    if (xhr.status === 422) {
                        Swal.fire({
                            text: 'Validação: ' + xhr.responseJSON,
                            icon: 'warning',
                            showClass: {
                                popup: 'animate_animated animate_wobble'
                            }
                        });
                    } else {
                        Swal.fire({
                            text: 'Erro interno, informe ao suporte: ' + xhr.responseJSON,
                            icon: 'error',
                            showClass: {
                                popup: 'animate_animated animate_wobble'
                            }
                        });
                    }
                }
            });
        });
    </script>
    <script>
        // Begin :: Format Phone
        formatedPhone();

        function upMaskPhone(event) {
            var $element = $('#' + this.id);
            $(this).off('blur');
            $element.unmask();
            if (this.value.replace(/\D/g, '').length > 10) {
                $element.mask("(00) 00000-0000");
            } else {
                $element.mask("(00) 0000-00009");
            }
            $(this).on('blur', upMaskPhone);
        }

        function formatedPhone() {
            $('.formatPhone').each(function(i, el) {
                $('.formatPhone').mask("(00) 00000-0000");
            });
            $('.formatPhone').on('blur', upMaskPhone);
        }
        // End :: Format Phone
    </script>

    <script>
        // Ao clicar no button desabilita botao e coloca icon loading depois de 3s retira
        $("form").submit(function(e) {
            //disable the submit button
            $("form button").attr("disabled", true);
            $('form button').append('<i class="fa fa-spinner fa-spin ml-3"></i>');
            setTimeout(function() {
                $('form button').prop("disabled", false);
                $('form button').find('.fa-spinner').addClass('d-none');
            }, 8000);
        });
    </script>
@endsection
