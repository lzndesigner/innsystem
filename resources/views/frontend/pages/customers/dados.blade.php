@extends('frontend.pages.customers.base')
@section('title', 'Meus Dados')

@section('content-account')

    <div class="row">
        <div class="col-sm-12">
            <div class="contact-page-form-send m-0 p-3">
                <form id="form-request" class="form-horizontal" method="POST">
                    {{ csrf_field() }}

                    <section class="">
                        <div class="form-group">
                            <div class="field input-field">
                                <input type="text" class="form-contain-home-input" id="email" name="email"
                                    value="{{ isset($result->email) ? $result->email : '' }}"
                                    placeholder="Digite seu e-mail" readonly>
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                            </div>
                        </div><!-- form-group -->

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <div class="field input-field">
                                        <input type="text" class="form-contain-home-input" id="name" name="name"
                                            value="{{ isset($result->name) ? $result->name : '' }}"
                                            placeholder="Digite seu Nome e Sobrenome" required autofocus>
                                        <i class="fa fa-user" aria-hidden="true"></i>
                                    </div>
                                </div><!-- form-group -->
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <div class="field input-field">
                                        <input type="text" class="form-contain-home-input" id="document" name="document"
                                            value="{{ isset($result->document) ? $result->document : '' }}"
                                            placeholder="Seu CPF/CNPJ" required>
                                        <i class="fa fa-book" aria-hidden="true"></i>
                                    </div>
                                </div><!-- form-group -->
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="field input-field">
                                        <input type="text" class="form-contain-home-input" id="company" name="company"
                                            value="{{ isset($result->company) ? $result->company : '' }}"
                                            placeholder="Nome da Empresa" required>
                                        <i class="fa fa-home" aria-hidden="true"></i>
                                    </div>
                                </div><!-- form-group -->
                            </div><!-- col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="field input-field">
                                        <input type="text" class="form-contain-home-input" id="phone" name="phone"
                                            value="{{ isset($result->phone) ? $result->phone : '' }}"
                                            placeholder="Telefone" required>
                                        <i class="fa fa-home" aria-hidden="true"></i>
                                    </div>
                                </div><!-- form-group -->
                            </div><!-- col -->
                        </div><!-- row -->
                    </section>

                    <section class="padding-20-0">
                        <h4>Meu endereço</h4>
                        @if ($result->cep == null)
                            <div class="alert alert-danger">Preencha seu Endereço!</div>
                        @endif
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="field input-field">
                                        <input type="text" class="form-contain-home-input" id="cep" name="cep"
                                            value="{{ isset($result->cep) ? $result->cep : '' }}" placeholder="Seu CEP">
                                    </div>
                                </div><!-- form-group -->
                            </div><!-- col-sm-3 -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="field input-field">
                                        <input type="text" class="form-contain-home-input" id="address" name="address"
                                            value="{{ isset($result->address) ? $result->address : '' }}"
                                            placeholder="Seu endereço">
                                    </div>
                                </div><!-- form-group -->
                            </div><!-- col-sm-3 -->
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="field input-field">
                                        <input type="text" class="form-contain-home-input" id="number" name="number"
                                            value="{{ isset($result->number) ? $result->number : '' }}"
                                            placeholder="Número">
                                    </div>
                                </div><!-- form-group -->
                            </div><!-- col-sm-3 -->
                        </div><!-- row -->

                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <div class="field input-field">
                                        <input type="text" class="form-contain-home-input" id="complement" name="complement"
                                            value="{{ isset($result->complement) ? $result->complement : '' }}"
                                            placeholder="Complemento">
                                    </div>
                                </div><!-- form-group -->
                            </div><!-- col-sm-3 -->
                            <div class="col-sm-5">
                                <div class="form-group">
                                    <div class="field input-field">
                                        <input type="text" class="form-contain-home-input" id="city" name="city"
                                            value="{{ isset($result->city) ? $result->city : '' }}" placeholder="Cidade">
                                    </div>
                                </div><!-- form-group -->
                            </div><!-- col-sm-3 -->
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="field input-field">
                                        <input type="text" class="form-contain-home-input" id="state" name="state"
                                            value="{{ isset($result->state) ? $result->state : '' }}"
                                            placeholder="Estado">
                                    </div>
                                </div><!-- form-group -->
                            </div><!-- col-sm-3 -->
                        </div>
                    </section>

                    <section class="padding-20-0">
                        <h4>Segurança da Conta</h4>
                        <p>Deixe a senha em branco para não alterá-la.</p>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="field input-field">
                                        <input type="password" id="password" name="password" class="form-control"
                                            placeholder="Senha">
                                    </div>
                                    <span class="help-block">Mínimo de 6 caracteres</span>
                                </div><!-- form-group -->
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="field input-field">
                                        <input type="password" id="password_confirmation" name="password_confirmation"
                                            class="form-control" placeholder="Confirmar Senha">
                                    </div>
                                </div><!-- form-group -->
                            </div>
                        </div>
                    </section>

                    <div class="form-group">
                        <button type="button" id="btn-salvar" data-id="{{ isset($result->id) ? $result->id : '' }}"
                            class="btn btn-primary">Salvar</button>
                    </div>
                </form>
            </div>

            <!-- FALTA COLOCAR o document, method payment e concluir a alteração via ajax  -->
        </div>
    </div>

@endsection

@section('cssPage')
    <link rel="stylesheet" href="{{ asset('/general/plugins/sweetalert/sweetalert2.min.css') }}">
@endsection

@section('jsPage')
    <script src="{{ asset('/general/plugins/sweetalert/sweetalert2.min.js') }}"></script>
    <script>
        // Button Save Forms - Create and Edit
        $(document).on('click', '#btn-salvar', function(e) {
            e.preventDefault();

            //disable the submit button
            $("form #btn-salvar").attr("disabled", true);
            $('form #btn-salvar').append('<i class="fa fa-spinner fa-spin ml-3"></i>');
            setTimeout(function() {
                $('form #btn-salvar').prop("disabled", false);
                $('form #btn-salvar').find('.fa-spinner').addClass('d-none');
            }, 8000);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });

            let id = $(this).data('id');

            if (id) {
                var url = `{{ url('/minha-conta/meus-dados/update/${id}') }}`;
                var method = 'PUT';
            }

            var data = $('#form-request').serialize();

            $.ajax({
                url: url,
                data: data,
                method: method,
                success: function(data) {
                    Swal.fire({
                        text: data,
                        icon: 'success',
                        showClass: {
                            popup: 'animate_animated animate_backInUp'
                        },
                        onClose: () => {
                            // Loading page listagem
                            location.href = "{{ url('/minha-conta/meus-dados') }}";
                        }
                    });
                },
                error: function(xhr) {
                    if (xhr.status === 422) {
                        Swal.fire({
                            text: 'Validação: ' + xhr.responseJSON,
                            icon: 'warning',
                            showClass: {
                                popup: 'animate_animated animate_wobble'
                            }
                        });
                    } else {
                        Swal.fire({
                            text: 'Erro interno, informe ao suporte: ' + xhr.responseJSON,
                            icon: 'error',
                            showClass: {
                                popup: 'animate_animated animate_wobble'
                            }
                        });
                    }
                }
            });
        });
    </script>
@endsection
