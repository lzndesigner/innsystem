@extends('frontend.base')
@section('title', 'Recuperação de Senha')

@section('content')

    <section class="padding-40-0">
        <div class="container">

            <div class="row justify-content-center">
                <div class="col-6">
                    <div class="card contact-page-form-send mt-2 mb-3">
                        <h2 class="purple-text text-center mb-4"><strong>@yield('title')</strong></h2>
                        @include('elements.messages')
                        @if (Session::has('message'))
                            <div class="alert alert-success" role="alert">
                                {{ Session::get('message') }}
                            </div>
                        @endif
                        <form class="form-horizontal" method="POST" action="{{ route('forget.password.post') }}">
                            {{ csrf_field() }}
                            <div class="d-flex">
                                <div class="flex-fill mx-5">
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <input id="email" type="email" class="form-control" name="email"
                                            value="{{ old('email') }}" required placeholder="E-mail de acesso">
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div><!-- form-group -->
                                </div>
                            </div>

                            <div class="d-flex justify-content-center mb-2">
                                <div>
                                    <button type="submit" class="btn btn-primary btn-icon-blue">
                                        <i class="fa fa-check"></i> Recuperação de Senha
                                    </button>
                                </div>
                            </div>
                            <div class="text-center">
                                <p><small><b>Você já possui uma conta? <a href="/minha-conta/login">Faça seu
                                                login</a></b></small></p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('jsPage')
    <script>
        // Ao clicar no button desabilita botao e coloca icon loading depois de 3s retira
        $("form").submit(function(e) {
            //disable the submit button
            $("form button").attr("disabled", true);
            $('form').append('<i class="fa fa-spinner fa-spin ml-3"></i>');
            // setTimeout(function() {
            //   $('form button').prop("disabled", false);
            //   $('form').parent().find('svg.fa-spinner').html('');
            // }, 8000);
        });
    </script>
@endsection
