@extends('frontend.base')
@section('title', 'Definir Nova Senha')

@section('content')

    <section class="padding-40-0">
        <div class="container">

            <div class="row justify-content-center">
                <div class="col-6">
                    <div class="card contact-page-form-send mt-2 mb-3">
                        <h2 class="purple-text text-center mb-4"><strong>@yield('title')</strong></h2>
                        @include('elements.messages')
                        <form class="form-horizontal" method="POST" action="{{ route('reset.password.post') }}">

                            {{ csrf_field() }}

                            <input type="hidden" name="token" value="{{ $token }}">
                            <div class="d-flex">
                                <div class="flex-fill mx-5">
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="email" class="control-label">Seu E-mail</label>
                                        <input id="email" type="email" class="form-control" name="email" value=""
                                            autocomplete="off" required placeholder="E-mail de acesso">
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div><!-- form-group -->
                                </div>
                            </div>

                            <div class="d-flex">
                                <div class="flex-fill mx-5">
                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="password" class="control-label">Nova Senha</label>
                                        <input id="password" type="password" class="form-control" name="password" required>
                                        <span class="help-block"><small>- Mínimo de 6 caracteres.</small></span>
                                        <br>
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div><!-- form-group -->
                                </div><!-- col -->
                            </div>
                            <div class="d-flex">
                                <div class="flex-fill mx-5">
                                    <div class="form-group">
                                        <label for="password-confirm" class="control-label">Confirma senha</label>
                                        <input id="password-confirm" type="password" class="form-control"
                                            name="password_confirmation" required>
                                        @if ($errors->has('password_confirmation'))
                                            <span class="text-danger">{{ $errors->first('password_confirmation') }}</span>
                                        @endif
                                    </div><!-- form-group -->
                                </div><!-- col -->
                            </div><!-- -->

                            <div class="d-flex justify-content-center mb-2">
                              <div>
                                  <button type="submit" class="btn btn-primary btn-icon-blue">
                                      <i class="fa fa-check"></i> Salvar Senha
                                  </button>
                              </div>
                          </div>
                          <div class="text-center">
                              <p><small><b>Você já possui uma conta? <a href="/minha-conta/login">Faça seu
                                              login</a></b></small></p>
                          </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('jsPage')
    <script>
        // Ao clicar no button desabilita botao e coloca icon loading depois de 3s retira
        $("form").submit(function(e) {
            //disable the submit button
            $("form button").attr("disabled", true);
            $('form').append('<i class="fa fa-spinner fa-spin ml-3"></i>');
            setTimeout(function() {
                $('form button').prop("disabled", false);
                $('form').parent().find('svg.fa-spinner').html('');
            }, 8000);
        });
    </script>
@endsection
