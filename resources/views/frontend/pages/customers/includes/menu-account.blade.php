<ul>
<li><a href="{{ url('/minha-conta') }}">Inicio</a></li>
<li><a href="{{ url('/minha-conta/meus-dados') }}">Meus Dados</a></li>
<li><a href="{{ url('/minha-conta/meus-servicos') }}">Meus Serviços</a></li>
<li><a href="{{ url('/minha-conta/minhas-faturas') }}">Minhas Faturas</a></li>
<li><a href="{{ url('/minha-conta/logout') }}">Sair da Conta</a></li>
</ul>