@extends('frontend.base')
@section('title', 'Criação de Site, Loja Virtual e Design Gráfico')

@section('breadcrumb')
<section class="padding-20-0">
    <div class="container">
        <div class="the_breadcrumb_conatiner_page">
            <div class="the_breadcrumb">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="{{ url('/') }}"><i class="fas fa-home"></i> Início</a></li>
                        <li><a href="{{ url('/helpdesk') }}">Central de Ajuda</a></li>
                        @if(isset($result->slugCategoria))
                        <li><a href="{{ url('/helpdesk/'.$result->slugCategoria) }}">{{ $result->nameCategory }}</a></li>
                        @endif
                        <li class="current"><a>{{ $result->title }}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('content')

<section class="padding-40-0 position-relative">
    <div class="container">
        <div class="row justify-content-start">
            <div class="col-md-8 privacy-content">
            @if($result->idCategory != '2')
                @if(isset($result->image))
                <div class="parallax-window" style="background: url('/storage/blog/{{ $result->idCategory }}/{{ $result->image }}') no-repeat center;"></div>
                @endif
            @endif
                <div class="help-center-header">
                    <h5 class="help-center-title">
                        {{ $result->title }}
                    </h5>
                </div>

                @if(isset($result->description))
                <div class="privacy-description">
                    {!! $result->description !!}
                </div>
                @endif
                <div class="privacy-footer">
                    <p><small>Atualizado: {{ \Carbon\Carbon::parse($result->updated_at)->format('d/m/Y') }} às {{ \Carbon\Carbon::parse($result->updated_at)->format('H:i') }}</small></p>
                    <p><small>
                        @if(isset($result->slugCategoria))
                        <a href="{{ url('/helpdesk/'.$result->slugCategoria) }}">{{ $result->nameCategory }}</a> |
                        @endif
                        Visualizações {{ $result->views }}</small></p>
                </div>
            </div>

            @include('frontend.pages.helpdesk.menuleft')
        </div>
    </div>
</section>

@endsection


@section('jsPage')
@endsection
@section('cssPage')
@endsection