<div class="col-md-4">
    <div class="widget">
        <h5 class="immediate-help-center-title">Menu de Navegação</h5>
        <div class="accordion" id="frequently-questions">
            @if(isset($blogcategories))
            @foreach($blogcategories as $blogCategory)
            <div class="questions-box">
                <div id="heading{{ $blogCategory->id }}">
                    <button class="btn questions-title @if(collect(request()->segments())->offsetGet('1') != $blogCategory->slug) collapsed @endif" type="button" data-toggle="collapse" data-target="#question{{ $blogCategory->id }}" aria-expanded="true" aria-controls="questionone">
                        {{ $blogCategory->name }}
                    </button>
                </div>
                <div id="question{{ $blogCategory->id }}" class="collapse @if(collect(request()->segments())->offsetGet('1') == $blogCategory->slug) show @endif questions-reponse" aria-labelledby="heading{{ $blogCategory->id }}" data-parent="#frequently-questions">
                    <ul>
                        @foreach($blogposts as $blogPost)
                        @if($blogCategory->id == $blogPost->category_id)
                        <li class="@if(collect(request()->segments())->last() == $blogPost->slug) active @endif"><a href="{{ url('/helpdesk/'.$blogCategory->slug.'/'.$blogPost->slug) }}">{{ $blogPost->title }}</a></li>
                        @endif
                        @endforeach
                    </ul>
                    <p><small><a href="{{ url('/helpdesk/'.$blogCategory->slug) }}">Ver todos</a></small></p>
                </div>
            </div>
            @endforeach
            @endif

        </div>
    </div>
    <div class="widget">
    <p class="immediate-help-center-text ">siga nas redes sociais</p>
                    <div class="contact-us-social-icons mr-tp-10">
                        <a class="fb" href="https://facebook.com/innsystem" target="_Blank"><i class="fab fa-facebook-f"></i></a>
                        <a class="tg" href="https://instagram.com/innsystem" target="_Blank"><i class="fab fa-instagram"></i></a>
                        <a class="yb" href="https://www.youtube.com/channel/UC4bXTS73cCs_qzY29M6J2lw" target="_Blank"><i class="fab fa-youtube"></i></a>
                    </div>
    </div>
</div>