@extends('frontend.base')
@section('title', 'Bem-vindo a Central de Ajuda')

@section('breadcrumb')
<section class="padding-20-0">
    <div class="container">
        <div class="the_breadcrumb_conatiner_page">
            <div class="the_breadcrumb">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="{{ url('/') }}"><i class="fas fa-home"></i> Início</a></li>
                        <li class="current"><a href="{{ url('/helpdesk') }}">Central de Ajuda</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('content')

<section class="pad-bt-60 position-relative">
    <div class="container">
        <div class="row">
            <div class="col-md-9 help-center-header">
                <h5 class="help-center-title">@yield('title')</h5>
                <p class="help-center-text"> Reunimos e organizamos materiais para auxiliar no seu dia-a-dia, são dicas úteis para otimizar sua produtividade diária, alavancar sua empresa na internet e preparamos um guia de treinamento para iniciar sua Loja Virtual. Confira as categorias disponíveis abaixo.</p>
            </div>
        </div>
        <div class="row justify-content-center padding-40-0">
            <div class="col-md-8 text-center">
                <div class="row justify-content-center nav our-help-center-tabs-nav">
                    @foreach($getCategories as $category)
                    <div class="col-6 col-md-6 col-lg-4">
                        <a href="{{ url('/helpdesk/'.$category->slug) }}">
                            <div class="support-contact-us-box">
                                <i class="{{$category->icon}}"></i>
                                <h5>{{$category->name}}</h5>
                                <p>{{$category->description}}</p>
                            </div>
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('jsPage')
@endsection
@section('cssPage')
@endsection