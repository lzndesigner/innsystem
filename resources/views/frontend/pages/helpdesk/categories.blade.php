@extends('frontend.base')
@section('title', $infoCategory->name)

@section('breadcrumb')
<section class="padding-20-0">
    <div class="container">
        <div class="the_breadcrumb_conatiner_page">
            <div class="the_breadcrumb">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="{{ url('/') }}"><i class="fas fa-home"></i> Início</a></li>
                        <li><a href="{{ url('/helpdesk') }}">Central de Ajuda</a></li>
                        <li class="current"><a href="{{ url('/helpdesk/'.$infoCategory->slugCategory) }}">{{ $infoCategory->name }}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('content')

<section class="padding-40-0 position-relative">
    <div class="container">
        <div class="row justify-content-start">
            <div class="col-md-8 privacy-content">
                <h2 class="side-text-right-title">@yield('title')</h2>
                <div class="row justify-content-start blog-items-home">
                    @foreach($results as $result)
                    <div class="col-md-6">
                        <div class="home-blog-te">
                            @if(isset($result->image))
                            <a href="{{ url('/helpdesk/'.$infoCategory->slugCategory.'/'.$result->slugPost) }}">
                                <div class="post-thumbn parallax-window" style="background: url('/storage/blog/{{ $infoCategory->id }}/{{ $result->image }}') no-repeat left;"></div>
                            </a>
                            @endif
                            <div class="post-bodyn">
                                <h5><a href="{{ url('/helpdesk/'.$infoCategory->slugCategory.'/'.$result->slugPost) }}">{{ $result->title }}</a></h5>
                                <p><i class="fa fa-eye"></i> Views {{ $result->views }} | <i class="far fa-calendar"></i> {{ \Carbon\Carbon::parse($result->created_at)->format('d/m/Y') }}</p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>

            @include('frontend.pages.helpdesk.menuleft')
        </div>
    </div>
</section>

@endsection


@section('jsPage')
@endsection
@section('cssPage')
@endsection