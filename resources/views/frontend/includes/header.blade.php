@if (trim($__env->yieldContent('main_home')))
<div id="coodiv-header" class="d-flex mx-auto flex-column moon-edition">
    @else
    <div id="coodiv-header" class="subpages-header-min moon-edition">
        @endif
        <!-- start header -->
        <div class="bg_overlay_header">
            <div id="particles-bg"></div>
            <img src="{{ asset('frontend/img/header/h_bg_02.svg') }}" alt="img-bg">
            @if (trim($__env->yieldContent('main_home')))
            <span class="header-shapes shape-01"></span>
            <span class="header-shapes shape-02"></span>
            <span class="header-shapes shape-03"></span>
            @endif
        </div>
        <!-- Fixed navbar -->
        <nav id="coodiv-navbar-header" class="navbar navbar-expand-md fixed-header-layout">
            <div class="container main-header-coodiv-s">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img class="w-logo" src="{{ asset('galerias/logo_azul_branco_60.webp') }}" alt="InnSystem Inovação em Sistemas" />
                    <img class="b-logo" src="{{ asset('galerias/logo_preto_60.webp') }}" alt="InnSystem Inovação em Sistemas" />
                </a>
                <button class="navbar-toggle offcanvas-toggle menu-btn-span-bar ml-auto js-offcanvas-has-events" data-toggle="offcanvas" data-target="#offcanvas-menu-home">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
                <div class="collapse navbar-collapse navbar-offcanvas d-none" id="offcanvas-menu-home">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item @if (collect(request()->segments())->last() == '') active @endif">
                            <a class="nav-link" href="/" aria-expanded="false">Início</a>
                        </li>

                        <li class="nav-item dropdown  @if (collect(request()->segments())->last() ==
                    'loja-virtual' || collect(request()->segments())->last() == 'criacao-sites' ||
                    collect(request()->segments())->last() == 'design-grafico' ||
                    collect(request()->segments())->last() == 'ilustracao') active @endif">
                            <a class="nav-link" href="#" role="button" id="services-megamenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Serviços <i class="fa fa-caret-down"></i><span class="megamenu-toggle"></span></a>
                            <div class="dropdown-menu coodiv-dropdown-header web-menu" aria-labelledby="webhosting-megamenu">
                                <ul class="web-hosting-menu-header">
                                    <li><a href="/loja-virtual"><i class="fas fa-shopping-cart"></i> Loja Virtual
                                            <small>(e-commerce)</small> <span>Venda seus produtos pela internet de forma rápida
                                                e segura</span></a></li>
                                    <li><a href="/criacao-sites"><i class="fas fa-desktop"></i> Criação de Sites <span>Tenha seu
                                                site na internet com visual próprio e recursos essenciais</span></a></li>
                                    <li><a href="/design-grafico"><i class="fas fa-magic"></i> Design Gráfico <span>Criação de
                                                logos, banners, cartões, interface web e mobile</span></a></li>
                                    <li><a href="/design-games"><i class="fa fa-dragon"></i> Design Games <span>Criação de personagens, logo e-sport e desenhos artísticos.</span></a></li>
                                </ul>
                            </div>
                        </li>

                        <li class="nav-item @if (collect(request()->segments())->last() == 'clientes') active @endif">
                            <a class="nav-link" href="/clientes">Clientes</a>
                        </li>

                        <li class="nav-item @if (collect(request()->segments())->last() == 'sobre-nos') active @endif">
                            <a class="nav-link" href="/sobre-nos">Sobre nós</a>
                        </li>

                        <li class="nav-item @if (collect(request()->segments())->last() == 'contato') active @endif">
                            <a class="nav-link" href="/contato">Contato</a>
                        </li>

                        <li class="d-none d-lg-block nav-item dropdown @if (collect(request()->segments())->first() ==
                    'helpdesk') active @endif">
                            <a class="nav-link" href="#" role="button" id="header-pages-drop-down" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Help Desk <i class="fa fa-caret-down"></i></a>
                            <ul class="dropdown-menu coodiv-dropdown-header" aria-labelledby="header-pages-drop-down">
                                <li><a href="{{ url('/helpdesk') }}" class="dropdown-item">Como funciona?</a></li>
                                <li class="divider"></li>
                                <li class="category"><span>Categorias</span></li>
                                @if (isset($blogcategories))
                                @foreach ($blogcategories as $blogCategory)
                                <li><a class="dropdown-item" href="{{ url('/helpdesk/' . $blogCategory->slug) }}">{{ $blogCategory->name }}</a>
                                </li>
                                @endforeach
                                @else
                                <li><a href="{{ url('/helpdesk') }}" class="dropdown-item">Ir pra Central de Ajuda</a>
                            </li>
                            @endif
                            <li><a href="{{ url('/grupos') }}" class="dropdown-item">Grupos do Facebook</a>
                            </ul>
                        </li>

                        @if (Auth::guard('customer')->check())
                        <li class="nav-item dropdown account">
                            <a class="nav-link" href="#" role="button" id="header-pages-account" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Minha Conta <i class="fa fa-caret-down"></i></a>
                            <ul class="dropdown-menu coodiv-dropdown-header right text-left" aria-labelledby="header-pages-account">
                                <li><a href="{{ url('/minha-conta') }}" class="dropdown-item">Início</a></li>
                                <li><a href="{{ url('/minha-conta/meus-dados') }}" class="dropdown-item">Meus Dados</a></li>
                                <li><a href="{{ url('/minha-conta/meus-servicos') }}" class="dropdown-item">Meus Serviços</a>
                                </li>
                                <li><a href="{{ url('/minha-conta/minhas-faturas') }}" class="dropdown-item">Minhas
                                        Faturas</a></li>
                                <li class="divider"></li>
                                <li><a href="{{ url('/minha-conta/logout') }}" class="dropdown-item">Sair da Conta</a></li>
                            </ul>
                        </li>
                        @else
                        <li class="dropdown">
                            <a href="{{ url('/minha-conta') }}" class="nav-link btnMyAccount">Minha Conta</a>
                        </li>
                        @endif


                    </ul>
                </div>
            </div>
        </nav>
        <div class="mt-auto header-top-height"></div>
        @yield('main_home')
    </div>
    <!-- end header -->