<p>Este documento lista os termos de contrato do sistema de loja virtual (e-commerce). Para fins de esclarecimento na prestação de serviço ao cliente.</p>
 
<p>Pelo presente instrumento particular as partes:</p>
 
<p><b>CLIENTE</b>: Pessoa Física ou Jurídica devidamente qualificada, que contrate produtos ou serviços através do endereço eletrônico www.innsystem.com.br, (o “CLIENTE”); </p>
<p><b>EMPRESA</b>: Leonardo Augusto Costa dos Santos, sociedade comercial, inscrita no C.N.P.J./MF sob o no. 21.635.247/0001-54, sediada na Avenida Antônio da Costa Lima, n° 526 Casa 1 – Quintino Facci 1 – Ribeirão Preto/SP, (a “EMPRESA”).  </p>

<p>Têm entre si ajustado o presente Contrato de Prestação de Serviços que se regerá pelas cláusulas e condições seguintes:</p>
 
<h5>Objetos de Contrato</h5>
  
<p><b>1ª CLÁUSULA: DAS CONDIÇÕES INICIAIS:</b></p>
 
<p>A prestação de serviço, ora contratado, substituem quaisquer acordos anteriormente vigentes, devendo, portanto, ser observadas e aceitas pelos interessados nesta tratativa. </p>
 
<p>O presente contrato tem eficácia plena e imediata a partir do momento em que o CLIENTE realizar a contratação do serviço. </p>

<p><b>2ª CLÁUSULA: DO OBJETO DO CONTRATO:</b></p>
 
<ul>
    <li>1.	O presente contrato tem por objeto a prestação de serviços de criação, hospedagem, manutenção e suporte de Loja Virtual, ressalvando-se à EMPRESA o direito de recusar desenvolver qualquer projeto que atende à legislação vigente ou que incentivem práticas discriminatórias, difamatórias, pornográficas ou outra qualquer que, a seu entendimento, entenda pertinente. </li>
    <li>2.	O envio de informações e materiais para o desenvolvimento do projeto, por parte do CLIENTE, se dê, de forma eletrônica, mediante o envio por e-mail, ferramentas de compartilhamento online (Google Drive e Dropbox) ou pessoalmente em reunião de pauta.</li>
    <li>3.	A utilização temporária de licença (s) através de um sistema proprietário de gerenciamento de pedidos (carrinho de compras, e-commerce) permitindo ao CLIENTE oferecer e vender seus produtos na internet. </li>
    <li>4.	São de responsabilidade do CLIENTE as tratativas e providências necessárias para a contratação e utilização dos meios de pagamentos e envios integrados ao sistema, não cabendo responsabilidade a EMPRESA, nas tratativas e nos pagamentos que serão efetuados por transações eletrônicas.</li>
    <li>5.	A EMPRESA poderá, a qualquer tempo, disponibilizar outros meios de pagamentos, envios e funcionalidades adicionais, bem como poderá cancelar os atualmente existentes, via comunicação antecipada ao CLIENTE.</li>
</ul>

<p><b>3ª CLÁUSULA: DAS OBRIGAÇÕES DO CLIENTE:</b></p>

<p>Para que a prestação do serviço se dê, de maneira ágil e eficiente, o CLIENTE compromete-se a:</p>
 
<ul>
    <li>1.	Fornecer todas as informações corretas e completas sobre si e seu negócio para contato e eventual entrega, incluindo sua denominação social, endereço completo, número de telefone e endereço de e-mail válido.</li>
    <li>2.	Fazer cópia de segurança (backup) dos arquivos recebidos e enviado pela EMPRESA.</li>
    <li>3.	Caso o CLIENTE não seja o titular do domínio do site registrado junto ao órgão de registro de domínios, declara ele sob as penas da lei civil e criminal manter relação jurídica válida e eficaz com o legítimo titular do domínio ou estar devidamente autorizado por este a hospedar em nome próprio o referido site.  </li>
</ul>

<p><b>4ª CLÁUSULA: DAS OBRIGAÇÕES DA EMPRESA:</b></p>

<ul>
    <li>1.	Desenvolver os serviços com técnica, qualidade e criatividade;</li>
    <li>2.	Desenvolver o material respeitando sempre o objetivo do cliente com os serviços contratados;</li>
    <li>3.	Entregar o material dentro do prazo acordados, salvo por motivo de força maior ou prazos acordados via e-mail;</li>
    <li>4.	Realizar o “backup de segurança” a cada 15 dias e manter os arquivos do “backup de segurança” por um período de 3 (três) meses;</li>
</ul>



<p><b>5ª CLÁUSULA: DA PROTEÇÃO DE DIREITOS INTELECTUAIS E DE PROPRIEDADE INDUSTRIAL:</b></p>

<ul>
    <li>1.	A EMPRESA se reserva o direito de expor ou não o projeto desenvolvido, a título de modelo, em seu portfólio. Os critérios de decisão são de inteira responsabilidade da EMPRESA. Fica expressamente proibido reproduzir as artes elaboradas neste contrato em outras aplicações ou outras empresas.</li>
</ul>


<p><b>6ª CLÁUSULA: DA EFICÁCIA:</b></p>

<ul>
    <li>1.	A EMPRESA se reserva o direito de fazer adições, modificações ou exclusões, no todo ou em partes, nas condições estabelecidas neste instrumento, a qualquer momento, sendo comunicado ao CLIENTE.</li>
</ul>


<p><b>7ª CLÁUSULA: DAS RESPONSABILIDADES E SUAS EXCLUDENTES:</b></p>

<p>O CLIENTE compromete-se a proteger, isentar e indenizar a EMPRESA, bem como seus sócios, administradores, prepostos, funcionários e agentes, por quaisquer danos materiais ou morais sofridos:</p>
<ul>
    <li>1.	Em decorrência do desenvolvimento do projeto.</li>
    <li>2.	Em decorrência de reclamação, reivindicação ou intervenção de terceiros, de natureza judicial ou extrajudicial.</li>
    <li>3.	Em decorrência de prejuízos à bens/serviços e suas respectivas marcas que eventualmente estiverem amparados por proteção legal relativa à propriedade intelectual, industrial, violação de imagem e de privacidade de pessoas, dados e/ou documentos relativos ao projeto.</li>
</ul>

<p>A EMPRESA ficará isenta de qualquer responsabilidade:</p>

<ul>
    <li>1.	Caso a demora na execução do serviço, decorrer de omissão de informação ou erro exclusivo causado pelo CLIENTE.</li>
    <li>2.	Por prejuízos diretos, indiretos e expectativas de benefícios comerciais eventualmente não alcançados após a veiculação do projeto.</li>
    <li>3.	Por projeto confiscado ou destruído por autoridade competente.</li>
    <li>4.	Em caso fortuito ou de força maior.</li>
</ul>


<p><b>8ª CLÁUSULA: DAS DISPONIBILIDADE DOS SERVIÇOS:</b></p>

<ul>
    <li>
        1.	Os serviços estarão disponíveis 24 horas por dia, 7 dias por semana, ressalvada a ocorrência de interrupções causadas por:
        <ul>
            <li>a.	Falta ou falha de fornecimento de energia elétrica para a EMPRESA.</li>
            <li>b.	Incompatibilidade dos sistemas do CLIENTE com os da EMPRESA.</li>
            <li>c.	Falhas de responsabilidade da empresa prestadora de serviços de telecomunicações.</li>
            <li>d.	Necessidade de reparos ou manutenção da rede externa ou interna que exijam o desligamento temporário do sistema.</li>
            <li>e.	Falhas nos sistemas de transmissão ou de roteamento de dados via Internet fornecidas pela empresa de telecomunicações.</li>
            <li>f.	Qualquer ação de terceiros que impeça a prestação dos serviços.</li>
            <li>g.	Em caso fortuito ou de força maior.</li>
        </ul>
    </li>
    
    <li>2.	A EMPRESA buscará comunicar previamente ao CLIENTE, sempre que possível as interrupções na prestação dos serviços pelos motivos relacionados acima, com a indicação do motivo para a interrupção e estarão registradas via e-mail do CLIENTE.</li>
    <li>3.	A EMPRESA não se responsabiliza por danos ou prejuízos decorrentes das interrupções da prestação do serviço pelos casos previstos nos itens 8.1, ou determinações legais ou judiciais.</li>
</ul>

<p><b>9ª CLÁUSULA: DOS DADOS DE ACESSO:</b></p>

<ul>
    <li>1.	Após a aprovação do pagamento, a EMPRESA informará ao CLIENTE os dados de acesso (link, usuário e senha) para acesso privativo, que constituirão a sua identificação para uso e administração dos serviços e recursos. </li>
    <li>2.	O CLIENTE terá apenas um código e uma senha privativa, pessoais e intransferíveis, não podendo ser objeto de alienação ou divulgação a terceiros. </li>
    <li>3.	O CLIENTE é inteiramente responsável por quaisquer prejuízos que venha a sofrer ou que cause a terceiros pela divulgação ou utilização indevida de seu código de assinante ou de sua senha privativa.</li>
    <li>4.	Se a EMPRESA verificar que senhas utilizadas nos serviços se encontram abaixo dos padrões mínimos de segurança, poderá solicitar ao CLIENTE sua imediata alteração ou bloquear o acesso evitando que os sites ou blogs hospedados venham a sofrer atuação de “hackers”, colocando em risco demais clientes e a operacionalidade do servidor. Em caso de bloqueio, a EMPRESA comunicará o CLIENTE da medida, persistindo o bloqueio até que o CLIENTE substitua as senhas de forma satisfatória.</li>
    <li>5.	A EMPRESA não se responsabiliza por falha do provedor de acesso, falha na confecção das páginas, furto ou destruição do conteúdo da página por algum acesso não autorizado (“hackers”, ou piratas eletrônicos), uso indevido da senha por terceiros autorizados ou não, alterações ou uso de gravações das páginas ou de informações sigilosas enviadas por e-mail. </li>
</ul>


<p><b>10ª CLÁUSULA: DO SERVIÇO COMPARTILHADO:</b></p>

<ul>
    <li>1.	Considerando que os serviços de Web Hosting (Loja) são compartilhados, o CLIENTE está ciente de que existem limitações advindas da hospedagem nos servidores da EMPRESA, devendo observar as restrições de utilização visando à preservação desses servidores. A infração a tais restrições autoriza a EMPRESA a suspender a prestação dos serviços contratados independentemente de aviso ou notificação, como forma de proteção dos servidores compartilhados e dos demais usuários que os utilizam. </li>
    <li>2.	O CLIENTE não pode utilizar os ambientes compartilhados para atividades como: backup online, disco virtual, repositório de quaisquer arquivos de mídia que exijam direitos autorais, repositório de arquivos em geral como “servidor de FTP”, streaming ou compartilhamento de arquivos com público em geral, ou qualquer outra atividade da estrutura de hospedagem que venha a prejudicar o desempenho dos serviços contratados. </li>
    <li>3.	O CLIENTE pode fazer a alteração de plano mediante a uma solicitação por e-mail” da EMPRESA.</li>
    <li>4.	A EXCLUSÃO DE SERVIÇOS ADICIONAIS posteriormente à celebração do Contrato, pode ser feita pelo CLIENTE mediante solicitação por e-mail para a exclusão. Em qualquer dos casos, todos os dados relativos aos serviços adicionados serão imediatamente apagados (deletados), sem qualquer possibilidade de recuperação. </li>
</ul>

<p><b>11ª CLÁUSULA: POLÍTICA DO ILIMITADO, ESPAÇO EM DISCO E TRANSFERÊNCIA:</b></p>

<ul>
    <li>1.	“Espaço em disco”, ou “espaço”, é a quantidade de dados gravados em disco, representados por arquivos de código, imagens, vídeos etc. e “transferência” é a quantidade de dados trafegados entre os servidores no qual seu site está hospedado e os visitantes legítimos de seu site.</li>
    <li>2.	Por "ilimitado" entende-se que não existem restrições sobre quantidade de espaço consumido por acessos de visitantes/usuários ao seu site ou pelo tráfego gerado. </li>
    <li>3.	O CLIENTE compreende, todavia, que ter espaço e transferência ilimitados não significa poder utilizar sua hospedagem de forma a prejudicar o funcionamento do servidor, ou para fins a qualquer título ilícitos. Caso isto venha a acontecer, a EMPRESA tomará as providências cabíveis do Contrato. </li>
    <li>4.	Na HOSPEDAGEM COMPARTILHADA OU DEDICADA não são permitidos:
        <ul>
            <li>a.	A utilização da hospedagem como um disco virtual, isto é, como espaço para armazenamento ou backup de arquivos que não sejam elementos de seu site. Para essa finalidade, outros produtos podem ser utilizados. </li>
            <li>b.	A hospedagem de filmes, séries de TV, música, aplicativos ou qualquer tipo de material com direitos autorais, caracterizados como “pirataria”, armazenadas em quaisquer formatos ou extensão de arquivos. </li>
            <li>c.	A hospedagem de conteúdo do tipo streaming ou webcasts sob quaisquer formatos ou extensões. </li>
            <li>d.	A utilização da hospedagem como “espelho” de outros sites, nem para servir como servidor de uploads no estilo de sites como “RapidShare” ou “MegaUpload”, ou a utilização como distribuidor em massa de multimídia de grande porte. </li>
            <li>e.	A disponibilização dos seus dados de login e senha para o Painel de Administração de Loja, SSH, FTP ao público - por exemplo, em fóruns, listas de discussão, canais de chat, outros sites -, com o intuito de compartilhar arquivos com o público em geral. </li>
            <li>f.	Para bases de dados MySQL/MariaDB, PostgreSQL e SQL Server, os acessos considerados legítimos são aqueles efetuados a partir de seu site, hospedado na EMPRESA. Acessos externos, feitos diretamente à base de dados, não são permitidos. </li>
            <li>g.	Em se tratando de hospedagem COMPARTILHADA OU DEDICADA, as limitações previstas não interferem no funcionamento na loja do cliente, eis que são compatíveis com as premissas de uma hospedagem COMPARTILHADA OU DEDICADA.   </li>
        </ul>
    </li>
</ul>

<p><b>12ª CLÁUSULA: FORMA DE PAGAMENTO E REAJUSTE:</b></p>

<ul>
    <li>1.	O CLIENTE deve realizar os pagamentos devidos em razão do Contrato antecipadamente e por meio de DEPÓSITO OU TRANSFERÊNCIA BANCÁRIA, BOLETO BANCÁRIO, PIX ou de CARTÃO DE CRÉDITO. Qualquer outra forma de pagamento não serve como prova de quitação e permite a rescisão do contrato pela EMPRESA por inadimplemento. </li>
    <li>2.	O CLIENTE deve fazer o pagamento do preço da assinatura mensalmente, a 2ª mensalidade após os 30 dias da data de cadastro e as demais a cada 30 dias ou de acordo com o período escolhido (mensal, trimestral ou anual), na mesma data de vencimento da fatura do mês seguinte ao da utilização, incluindo tributos e demais encargos conforme a legislação em vigor. </li>
    <li>3.	A não autorização do débito no cartão de crédito ou a não quitação da cobrança bancária, autoriza a EMPRESA, independentemente de notificação judicial ou extrajudicial, a suspender a prestação dos serviços após 7 dias de atraso. </li>
    <li>4.	O preço contratado também poderá ser revisto, a qualquer momento com aviso prévio de 1 mês, para o resgate do equilíbrio econômico-financeiro do contrato em caso de elevação imprevista dos insumos necessários à prestação dos serviços ou no caso de modificações significativas do regime tributário. </li>
    <li>5.	O atraso no pagamento acarretará a incidência de correção monetária e de multa de 10% do valor em atraso, sem prejuízo da suspensão automática dos serviços, com a possibilidade de imediata rescisão do contrato pela EMPRESA, independentemente de aviso prévio. </li>
    <li>6.	Caso o atraso persista por mais de 30 dias a EMPRESA, informará o CLIENTE o cancelamento do serviço com aviso prévio por rescisão do contrato.</li>
    <li>7.	Os serviços prestados em cada mês, para efeito de cobrança, se constituem em um todo indivisível, de modo que pagamentos parciais não são admitidos. </li>
    <li>8.	Caso o CLIENTE opte pelo pagamento trimestral ou anual ele fará jus a um desconto correspondente a 10% e 15% respectivamente, calculados sobre o preço correspondente ao serviço mensal anualizado.</li>
    <li>9.	Ocorrendo a rescisão antecipada do Contrato, o CLIENTE não terá a devolução do pagamento daquele ano vigente. Caso o CLIENTE desejar reativar o serviço dentro do ano vigente será feito um novo Contrato e Pagamento sobre um novo Plano. </li>
</ul>

<p><b>13ª CLÁUSULA: DA DESISTÊNCIA OU CANCELAMENTO DO SERVIÇO:</b></p>

<ul>
    <li>1.	O CLIENTE poderá solicitar o cancelamento do serviço contratado, desde que expressamente formalizado por e-mail no endereço contato@innsystem.com.br. Ressaltamos que o CLIENTE está ciente de que não haverá a devolução do valor pago.</li>
    <li>2.	Não trabalhamos com devoluções, pois trabalhamos com serviços Digitais, por isso, informamos todos os detalhes do serviço, compatibilidade e garantimos o funcionamento do mesmo. Em caso de problemas, entre em contato para que seja analisado e resolvido.</li>
    <li>3.	Apenas serão considerados válidos e eficazes, os pedidos de desistência ou cancelamento enviados exclusivamente para o e-mail contato@innsystem.com.br.</li>
    <li>4.	Fica expressamente vedado às partes, sob as penas da lei, o reaproveitamento, o repasse, a adaptação ou a reprodução do projeto dado por cancelado pelo CLIENTE. </li>
</ul>

<p><b>14ª CLÁUSULA: FORMA DE UTILIZAÇÃO DO SERVIÇO:</b></p>

<ul>
    <li>1.	O CLIENTE está ciente de que a cobrança pelos serviços contempla a disponibilização de infraestrutura para o uso da capacidade EMPRESA. Assim, caso não ocupe todo o espaço que lhe for disponibilizado, ou não utilize todos os serviços contratados (tais como tráfego mensal), ou não faça uso de todo o limite que lhe é disponibilizado, isto não confere descontos, compensações, acúmulos, inovações, ou transferências para os próximos meses, tampouco sendo permitido transferir os limites não utilizados para terceiros ou para outros contratos que tenha com a EMPRESA.</li>
    <li>2.	A EMPRESA não se responsabiliza pela interrupção da prestação dos serviços por determinação dos órgãos competentes para o registro de domínios por falta de envio de documentação e pagamento da anuidade aos mesmos órgãos, quando se fizer necessário, sejam eles o REGISTRO.BR, o Registrador Internacional ou outro autorizado.</li>
    <li>3.	É VEDADO ao CLIENTE instalar ou tentar instalar qualquer tipo de programa no servidor. </li>
    <li>4.	A utilização de programas ou conteúdo que por qualquer razão prejudiquem ou possam vir a prejudicar o funcionamento do servidor permite à EMPRESA promover a imediata suspensão da prestação dos serviços independentemente de aviso ou notificação. </li>
    <li>5.	É também VEDADO ao CLIENTE:
        <ul>
            <li>a.	Divulgar informações falsas, incorretas, incompletas ou sigilosas; </li>
            <li>b.	Ameaçar, ofender, abalar a imagem ou invadir a privacidade de outros assinantes ou de terceiros;</li>
            <li>c.	Buscar acesso a senhas e dados privativos, modificando arquivos ou assumindo a identidade de outro assinante;</li>
            <li>d.	Desrespeitar leis de direito autoral e de propriedade intelectual; </li>
            <li>e.	Prejudicar usuários da Internet ou da EMPRESA, através do uso de programas, acesso não autorizado a computadores, alterações de arquivos, programas e dados existentes na rede; </li>
            <li>f.	Divulgar ou disponibilizar qualquer material cujo conteúdo fira a legislação em vigor, em especial qualquer material se utilizando de menores de idade em cena de sexo ou pornográfica;</li>
        </ul>
    </li>
    
    <li>2.	Se a EMPRESA considerar impróprio ou ilegal qualquer material divulgado pelo CLIENTE, comunicará a respeito, por e-mail, estabelecendo prazo para que o material seja retirado ou alterado de forma a regularizar a sua utilização, vindo o qual a própria EMPRESA suspenderá a divulgação do material. </li>
    <li>3.	As informações pessoais do CLIENTE utilizadas no cadastramento do serviço são sigilosas e de uso exclusivo da EMPRESA.</li>
    <li>4.	Cabe exclusivamente ao CLIENTE a aquisição e manutenção dos programas (software/hardware), equipamentos, terminais e suas interfaces com as redes de telecomunicação, necessários à utilização dos serviços.</li>
    <li>5.	O backup por parte da EMPRESA visa apenas à restauração da infraestrutura em caso de falhas, não sendo seu objetivo a disponibilização para o CLIENTE. </li>
    <li>6.	Em caso de pedido feito pelo CLIENTE de alteração de plataforma, sistema de hospedagem ou empresa (migração), o “backup” armazenado será mantido por 90 dias corrido e será APAGADO, SEM POSSIBILIDADE DE RECUPERAÇÃO.</li>
    <li>7.	O CLIENTE compreende e aceita que em relação às eventuais caixas postais (e-mail) disponibilizadas em conjunto com o plano de hospedagem em servidor compartilhado escolhido, o “backup” abrange apenas as configurações e não as mensagens armazenadas, sendo o CLIENTE, portanto, responsável pelo “backup” das mensagens. </li>
    <li>8.	O CLIENTE autoriza a EMPRESA a acessar os arquivos existentes na área de hospedagem sempre que esse acesso for necessário e/ou conveniente para a prestação do suporte técnico de responsabilidade da EMPRESA.  </li>
</ul>

<p><b>15ª CLÁUSULA: PROTEÇÃO DO SERVIÇO COMPARTILHADO:</b></p>

<ul>
    <li>1.	Para garantir o bom funcionamento do servidor impedindo que problemas advindos de outros “sites” instalados no mesmo servidor prejudiquem o CLIENTE e os demais usuários, a EMPRESA pode, além de outras medidas previstas no Contrato: </li><li>a.	Alterar a configuração do servidor quando necessário ao seu bom funcionamento; 
        <ul>
            <li>b.	Habilitar ou desabilitar comandos que comprometam o bom funcionamento do servidor. </li>
            <li>c.	Remanejar internamente as lojas para outro servidor, independentemente de aviso ou notificação prévia ao CLIENTE, com a consequente alteração do IP (Internet Protocol) do servidor que hospeda o “site”. </li>
            <li>d.	Alterar o IP (Internet Protocol) de qualquer servidor, independentemente de aviso ou notificação ao CLIENTE. </li>
        </ul>
    </li>
</ul>


<p><b>16ª CLÁUSULA: RISCOS NA UTILIZAÇÃO DA INTERNET:</b></p>

<ul>
    <li>1.	O CLIENTE declara ter ciência de que A UTILIZAÇÃO DA INTERNET ENVOLVE DIVERSOS RISCOS, que podem ocasionar significativos prejuízos, sobretudo financeiros, dentre os quais se destacam: 
        <ul>
            <li>a.	Perda total ou parcial de informações, arquivos ou de programas por contaminação com vírus; </li>
            <li>b.	Clonagem ou cópia do número do cartão de crédito, ou de contas bancárias e respectivas senhas do usuário visando ao seu uso fraudulento; </li>
            <li>c.	Fraude na compra de produtos ou serviços pela INTERNET (incluindo a não entrega de produto e a não prestação de serviços). </li>
        </ul>
    </li>
    <li>2.	É de exclusiva responsabilidade do CLIENTE a adoção de medidas de prevenção contra os riscos mencionados acima e contra outros advindos da utilização da INTERNET. </li>
    <li>3.	O CLIENTE declara-se ciente de que a INTERNET contém material impróprio para menores, pelo que assume a responsabilidade pela supervisão e controle de visitas e acessos a conteúdos impróprios por menores de idade. </li>
    <li>4.	O CLIENTE assume integralmente qualquer responsabilidade por danos ou prejuízos diretos ou indiretos que a EMPRESA venha a sofrer ou cause a terceiros, como consequência da utilização da INTERNET, ou em razão de omissão no cumprimento dos deveres assumidos no Contrato.  </li>
</ul>

<p><b>17ª CLÁUSULA: DISPONIBILIDADE DO SERVIÇO E SLA:</b></p>

<ul>
    <li>1.	Considerando que o funcionamento dos equipamentos e demais limitações tanto de software quanto de hardware impedem, na área de tecnologia, a garantia de 100% de operação e, portanto, da integralidade dos serviços, as PARTES estabelecem o “acordo de nível mínimo de desempenho” ou SLA (Service Level Agreement) para o nível de desempenho técnico do serviço proposto. Com a observância das obrigações do CLIENTE previstas no Contrato, a EMPRESA tem condição técnica de oferecer e se propõe a manter um SLA de manutenção no ar dos serviços, por 99,5% do tempo, em cada mês civil, ressalvadas as seguintes hipóteses: 
        <ul>
            <li>a.	Falhas na conexão (“LINK”) fornecida pela empresa de telecomunicações encarregada da prestação do serviço, sem culpa da EMPRESA.</li>
            <li>b.	Interrupções necessárias para ajustes técnicos ou manutenção, nos termos das cláusulas, que serão informadas com antecedência e se realizarão, preferencialmente, em horários noturnos, de baixo movimento, com o limite máximo de uma manutenção por mês em dias e horários previamente acordados com o CLIENTE; </li>
            <li>c.	Intervenções emergenciais decorrentes da necessidade de preservar a segurança do site, destinadas a evitar ou fazer cessar a atuação de “hackers” ou destinadas a implementar correções de segurança (patches). A EMPRESA informará tais intervenções previamente ao CLIENTE, com a justificativa da necessidade da intervenção emergencial; </li>
            <li>d.	Interrupções diárias necessárias para ajustes técnicos ou manutenção, com duração de até 15 minutos, feitas entre as 02:00 e 6:00 da manhã, independente de prévia notificação. </li>
            <li>e.	Suspensão da prestação dos serviços por determinação de autoridades competentes, ou por descumprimento de obrigações do Contrato; </li>
            <li>f.	Superação pelo CLIENTE do limite de 95% da capacidade máxima de utilização do HD (disco rígido) do servidor; </li>
            <li>g.	Adoção pelo CLIENTE de plano ou pacotes de serviços cujas características sejam inadequadas ou insuficientes para suportar a demanda exigida, seja de modo permanente ou ocasional, pelas atividades desenvolvidas; </li>
            <li>h.	Falta ou falha no fornecimento de energia elétrica para o datacenter;</li>
            <li>i.	Incompatibilidade entre os sistemas do CLIENTE e os da EMPRESA; </li>
            <li>j.	Necessidade de reparos ou manutenção da rede externa ou interna que exijam o desligamento temporário do sistema;</li>
            <li>k.	Falha no equipamento, em softwares ou no sistema operacional; </li>
        </ul>
    </li>
    <li>2.	A EMPRESA sempre que possível informará ao CLIENTE as interrupções na prestação dos serviços pelos motivos relacionados acima, com a indicação do motivo para a interrupção; </li>
    <li>3.	Se o servidor ficar fora do ar por mais de 5% do tempo em algum mês civil, o CLIENTE pode opcionalmente rescindir o Contrato, sem aviso prévio; </li>
    <li>4.	Havendo diversos serviços sujeitos a SLA contratados pelo mesmo instrumento e possuindo esses serviços SLAs diferentes, prevalecerá o SLA específico de cada serviço; </li>
</ul>


<p><b>18ª CLÁUSULA: PRAZO DE EXTINÇÃO DO CONTRATO:</b></p>

<ul>
    <li>1.	O Contrato é celebrado pelo prazo de 30 dias e é renovado por iguais períodos, sucessivamente, desde que não haja denúncia por qualquer das partes, nos termos das demais condições. </li>
    <li>2.	Uma PARTE pode rescindir o Contrato sem qualquer aviso prévio, sempre que a outra PARTE violar qualquer das cláusulas do Contrato. </li>
    <li>3.	Após a conclusão do cancelamento todo conteúdo do CLIENTE armazenado nos servidores da EMPRESA, o site, e-mail e/ou bancos de dados será automaticamente apagado sem possibilidade de recuperação. </li>
    <li>4.	Na hipótese de extinção do Contrato, SERÃO AUTOMATICAMENTE APAGADOS também as mensagens eletrônicas ("e-mails") que não tiverem sido "baixadas" e todo o conteúdo do site (hospedagem), banco de dados, disponibilizado pelo CLIENTE. </li>
    <li>5.	Na extinção do contrato por pedido expresso por e-mail, todos os dados do “site” do CLIENTE e de todos os serviços adicionais eventualmente contratados serão automaticamente apagados (deletados), de forma definitiva e irreversível, sem possibilidade de recuperação. </li>
    <li>6.	Na extinção do Contrato por iniciativa da EMPRESA em razão de inadimplemento do CLIENTE, a EMPRESA, independentemente de haver retirado o site hospedado do ar, manterá em armazenagem, sem custo adicional para o CLIENTE, a última versão dos dados componentes do site, exclusivamente para os planos de hospedagem COMPARTILHADA OU DEDICADA, pelo período máximo de 30 dias a contar da data do inadimplemento do CLIENTE. </li>
    <li>7.	Independentemente de qualquer aviso ou notificação, findo o prazo de 30 (trinta) dias ora estabelecido, o apagamento dos dados ocorrerá de forma irreversível.  </li>
</ul>


<p><b>19ª CLÁUSULA: DAS CONDIÇÕES FINAIS:</b></p>

<p>No presente momento, as partes: </p>
<ul>
    <li>1.	Declaram e concordam que o pedido de contratação implica na concordância incondicional das cláusulas contidas no presente contrato, pois o mesmo reflete a vontade dos interessados; </li>
    <li>2.	Declaram e concordam que, em regra, todas as comunicações entre as partes dar-se-ão por qualquer meio de comunicação e serão consideradas recebidas quando trocadas entre os endereços da EMPRESA e do CLIENTE as quais têm força de prova digital;</li>
    <li>3.	Declaram e compromete-se que, se houver qualquer alteração do endereço eletrônico para comunicações e correspondências, esta atualização deve ser informada imediatamente uma à outra;</li>
    <li>4.	Declaram e concordam que para se utilizar da excludente por caso fortuito ou força maior, as partes deverão informar à outra, em até 48 horas contados da ocorrência do evento, as medidas e o prazo necessários para restabelecimento e normalização pertinentes; </li>
    <li>5.	Declaram, sob as penas da lei, que as informações cadastrais fornecidas são verdadeiras e corretas, bem como que as mesmas deverão ser mantidas de forma atualizada; </li>
    <li>6.	Declaram que o presente contrato foi pactuado dentro dos princípios da boa-fé; </li>
    <li>7.	Declaram-se cientes de que é expressamente vedada a cessão ou transferência deste contrato a terceiros, exceto de comum acordo entre as partes; </li>
    <li>8.	O cliente reconhece e declara que leu e que está ciente e de pleno acordo com todos os termos e condições deste contrato e que a ele se vincula no ato do seu cadastramento.  </li>
</ul>
<p>O presente contrato tem eficácia plena e imediata a partir do momento em que o CLIENTE realizar a contratação do serviço através do endereço eletrônico “www.innsystem.com.br”</p>
