<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/************************/
// Routes Frontend
Route::get('/', 'Frontend\HomeController@index')->name('frontend.home');
Route::get('/sobre-nos', 'Frontend\HomeController@about')->name('frontend.about');
Route::get('/criacao-sites', 'Frontend\ServicesController@sites')->name('frontend.criacaosites');
Route::get('/loja-virtual', 'Frontend\ServicesController@lojavirtual')->name('frontend.lojavirtual');
Route::get('/loja-virtual/modelos', 'Frontend\ServicesController@lojavirtual_modelos')->name('frontend.lojavirtual_modelos');
Route::get('/loja-virtual/contratar/{id}/{period}', 'Frontend\ServicesController@lojavirtualOrder')->name('frontend.lojavirtual.order');
Route::post('/loja-virtual/contratar/finalizar', 'Frontend\ServicesController@lojavirtualStore')->name('frontend.lojavirtual.store');
Route::get('/loja-virtual/contratar/sucesso', 'Frontend\ServicesController@lojavirtualSuccess')->name('frontend.lojavirtual.success');

Route::get('/design-grafico', 'Frontend\ServicesController@designgrafico')->name('frontend.designgrafico');
Route::get('/design-games', 'Frontend\ServicesController@designgames')->name('frontend.designgames');
Route::get('/ilustracao', function () {
  return redirect('/design-games');
});
Route::get('/mu-online', function () {
  return redirect('/design-games');
});
Route::get('/whatsapp', function () {
  return redirect('https://wa.me/5516992747526');
});
Route::get('/clientes', 'Frontend\HomeController@clientes')->name('frontend.clientes');
Route::get('/contato', 'Frontend\HomeController@contact')->name('frontend.contact');
Route::post('/contato-sendmail', 'Frontend\HomeController@contactSendMail')->name('frontend.contact.sendmail');

Route::get('/helpdesk', 'Frontend\BlogController@index')->name('frontend.blog.index');
Route::get('/helpdesk/{slugCategory}', 'Frontend\BlogController@showCategory')->name('frontend.blog.showCategory');
Route::get('/helpdesk/{slugCategory}/{slugPost}', 'Frontend\BlogController@showPost')->name('frontend.blog.showPost');

Route::get('/grupos', 'Frontend\HomeController@grupos')->name('frontend.grupos');

Route::get('/politica-privacidade', 'Frontend\HomeController@politicaPrivacidade')->name('frontend.pages.politica_privacidade');


Auth::routes(['register' => false]);


/************************/
// Routes Minha Conta
// Minha Conta
Route::prefix('minha-conta')->group(function () {

  // Login - Register
  Route::get('login', 'Customer\LoginController@login')->name('customer.auth.login');
  Route::post('login', 'Customer\LoginController@loginCustomer')->name('customer.auth.loginCustomer');
  Route::get('logout', 'Customer\LoginController@logout')->name('customer.auth.logout');
  // Registro
  Route::get('register', 'Customer\LoginController@create')->name('customer.register');
  Route::post('register', 'Customer\LoginController@store')->name('customer.register.store');

  Route::get('login/google', 'Customer\LoginController@loginGoogle')->name('customer.auth.login.google');
  Route::get('login/google/callback', 'Customer\LoginController@loginGoogleCallback')->name('customer.auth.login.google.callback');


  Route::get('forget-password', 'Customer\ResetPasswordController@showForgetPasswordForm')->name('forget.password.get');
  Route::post('forget-password', 'Customer\ResetPasswordController@submitForgetPasswordForm')->name('forget.password.post');
  Route::get('reset-password/{token}', 'Customer\ResetPasswordController@showResetPasswordForm')->name('reset.password.get');
  Route::post('reset-password', 'Customer\ResetPasswordController@submitResetPasswordForm')->name('reset.password.post');



  // Dashboard
  Route::get('/', 'Customer\HomeController@index')->name('customer.dashboard');
  Route::get('/meus-dados', 'Customer\HomeController@editarDados')->name('customer.editardados');
  Route::put('/meus-dados/update/{id}', 'Customer\HomeController@editarDadosUpdate')->name('customer.editardados.update');

  Route::get('/meus-servicos', 'Customer\HomeController@myServices')->name('customer.services');
  Route::get('/minhas-faturas', 'Customer\HomeController@myInvoices')->name('customer.invoices');
  Route::get('/fatura/{id}', 'Customer\HomeController@viewInvoice')->name('frontend.invoice.show');
});




/************************/
// Routes Backend
Route::prefix('admin')->middleware('auth')->group(function () {
  Route::get('/', 'Backend\HomeController@index')->name('backend.home');

  //Customers
  Route::get('/customers', 'Backend\CustomerController@index')->name('backend.customers');
  Route::get('/customers-create', 'Backend\CustomerController@create')->name('backend.customers.create');
  Route::post('/customers-store', 'Backend\CustomerController@store')->name('backend.customers.store');
  Route::get('/customers-edit/{id}', 'Backend\CustomerController@edit')->name('backend.customers.edit');
  Route::put('/customers-update/{id}', 'Backend\CustomerController@update')->name('backend.customers.update');
  Route::get('/customers-details/{id}', 'Backend\CustomerController@show')->name('backend.customers.show');
  Route::get('/customers-services/{id}', 'Backend\CustomerController@showServices')->name('backend.customers.showServices');
  Route::get('/customers-invoices/{id}', 'Backend\CustomerController@showInvoices')->name('backend.customers.showInvoices');
  Route::get('/customers-activities/{id}', 'Backend\CustomerController@showActivities')->name('backend.customers.showActivities');
  Route::delete('/customers-delete', 'Backend\CustomerController@destroy')->name('backend.customers.delete');

  //Customer Services
  Route::get('/customerservices-create/{customer_id}', 'Backend\CustomerServicesController@create')->name('backend.customerservices.create');
  Route::post('/customerservices-store', 'Backend\CustomerServicesController@store')->name('backend.customerservices.store');
  Route::get('/customerservices-edit/{customer_id}/{id}', 'Backend\CustomerServicesController@edit')->name('backend.customerservices.edit');
  Route::put('/customerservices-update/{id}', 'Backend\CustomerServicesController@update')->name('backend.customerservices.update');
  Route::get('/customerservices-details/{id}', 'Backend\CustomerServicesController@show')->name('backend.customerservices.show');
  Route::delete('/customerservices-delete', 'Backend\CustomerServicesController@destroy')->name('backend.customerservices.delete');

  //Customer Invoices
  Route::get('/invoices', 'Backend\InvoicesController@index')->name('backend.invoices');
  Route::get('/invoices-create/{customer_id}', 'Backend\InvoicesController@create')->name('backend.invoices.create');
  Route::post('/invoices-store', 'Backend\InvoicesController@store')->name('backend.invoices.store');
  Route::get('/invoices-edit/{customer_id}/{id}', 'Backend\InvoicesController@edit')->name('backend.invoices.edit');
  Route::put('/invoices-update/{id}', 'Backend\InvoicesController@update')->name('backend.invoices.update');
  Route::put('/invoices-confirm/{id}', 'Backend\InvoicesController@confirm')->name('backend.invoices.confirm');
  Route::get('/invoices-details/{id}', 'Backend\InvoicesController@show')->name('backend.invoices.show');
  Route::delete('/invoices-delete', 'Backend\InvoicesController@destroy')->name('backend.invoices.delete');

  //Services Actives
  Route::get('/servicescustomers', 'Backend\ServicesActivesController@index')->name('backend.services.customers');

  //Services
  Route::get('/services', 'Backend\ServiceController@index')->name('backend.services');
  Route::get('/services-create', 'Backend\ServiceController@create')->name('backend.services.create');
  Route::post('/services-store', 'Backend\ServiceController@store')->name('backend.services.store');
  Route::get('/services-edit/{id}', 'Backend\ServiceController@edit')->name('backend.services.edit');
  Route::put('/services-update/{id}', 'Backend\ServiceController@update')->name('backend.services.update');
  Route::get('/services-details/{id}', 'Backend\ServiceController@show')->name('backend.services.show');
  Route::delete('/services-delete', 'Backend\ServiceController@destroy')->name('backend.services.delete');


  //Relatorios
  Route::get('/reports', 'Backend\ReportsController@index')->name('backend.reports');

  // Financeiro
  Route::get('/financeiro', 'Backend\FinancialController@index')->name('backend.financial');
  Route::get('/financeiro/conta-bancaria/create', 'Backend\FinancialController@accountBankCreate')->name('backend.financial.account_bank.create');
  Route::post('/financeiro/conta-bancaria/store', 'Backend\FinancialController@accountBankStore')->name('backend.financial.account_bank.store');
  Route::get('/financeiro/conta-bancaria/edit/{id}', 'Backend\FinancialController@accountBankEdit')->name('backend.financial.account_bank.edit');
  Route::post('/financeiro/conta-bancaria/update/{id}', 'Backend\FinancialController@accountBankUpdate')->name('backend.financial.account_bank.update');

  Route::get('/financeiro/despesas/create', 'Backend\FinancialController@expensesCreate')->name('backend.financial.expenses.create');
  Route::post('/financeiro/despesas/store', 'Backend\FinancialController@expensesStore')->name('backend.financial.expenses.store');
  Route::get('/financeiro/despesas/edit/{id}', 'Backend\FinancialController@expensesEdit')->name('backend.financial.expenses.edit');
  Route::post('/financeiro/despesas/update/{id}', 'Backend\FinancialController@expensesUpdate')->name('backend.financial.expenses.update');
  Route::post('/financeiro/despesas/delete/{id}', 'Backend\FinancialController@expensesDelete')->name('backend.financial.expenses.delete');

  Route::get('/financeiro/lancamentos/load', 'Backend\FinancialController@releasesLoad')->name('backend.financial.releases.load');
  Route::get('/financeiro/lancamentos/create', 'Backend\FinancialController@releasesCreate')->name('backend.financial.releases.create');
  Route::post('/financeiro/lancamentos/store', 'Backend\FinancialController@releasesStore')->name('backend.financial.releases.store');
  Route::get('/financeiro/lancamentos/edit/{id}', 'Backend\FinancialController@releasesEdit')->name('backend.financial.releases.edit');
  Route::post('/financeiro/lancamentos/update/{id}', 'Backend\FinancialController@releasesUpdate')->name('backend.financial.releases.update');



  //Blog - Categories
  Route::get('/blog/categories', 'Backend\BlogCategoriesController@index')->name('backend.blog.categories');
  Route::get('/blog/categories-create', 'Backend\BlogCategoriesController@create')->name('backend.blog.categories.create');
  Route::post('/blog/categories-store', 'Backend\BlogCategoriesController@store')->name('backend.blog.categories.store');
  Route::get('/blog/categories-edit/{id}', 'Backend\BlogCategoriesController@edit')->name('backend.blog.categories.edit');
  Route::put('/blog/categories-update/{id}', 'Backend\BlogCategoriesController@update')->name('backend.blog.categories.update');
  Route::get('/blog/categories-details/{id}', 'Backend\BlogCategoriesController@show')->name('backend.blog.categories.show');
  Route::delete('/blog/categories-delete', 'Backend\BlogCategoriesController@destroy')->name('backend.blog.categories.delete');
  //Blog - Posts
  Route::get('/blog/posts', 'Backend\BlogPostsController@index')->name('backend.blog.posts');
  Route::get('/blog/posts-create', 'Backend\BlogPostsController@create')->name('backend.blog.posts.create');
  Route::post('/blog/posts-store', 'Backend\BlogPostsController@store')->name('backend.blog.posts.store');
  Route::get('/blog/posts-edit/{id}', 'Backend\BlogPostsController@edit')->name('backend.blog.posts.edit');
  Route::post('/blog/posts-update/{id}', 'Backend\BlogPostsController@update')->name('backend.blog.posts.update');
  Route::get('/blog/posts/{slugCategory}/{slugPost}', 'Backend\BlogPostsController@show')->name('backend.blog.posts.show');
  Route::delete('/blog/posts-delete', 'Backend\BlogPostsController@destroy')->name('backend.blog.posts.delete');


  //Configuração Geral
  Route::get('/settings', 'Backend\HomeController@settings')->name('backend.settings');

  // Desativado function copy/duplique
  // Route::post('/portfolios/copy', 'PortfolioController@copy');
});
